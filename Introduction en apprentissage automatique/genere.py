import numpy as np
import matplotlib.pyplot as pl
from scipy.signal import hilbert

pl.rcParams.update({'font.size': 22})
#====================================================================
# Master 1 SIA       Generation des signaux
#
#                                       jf Trouilhet - Septembre 2022
#====================================================================
#

NbrPts = 2048
Fe = 44100

t = np.arange(NbrPts) / Fe

# PtsFreq1 = int(5500 * NbrPts / Fe - 25)
# PtsFreq2 = int(5500 * NbrPts / Fe + 25)
f = np.arange(NbrPts) * Fe / NbrPts
# f = np.arange(-f[f.shape[0] - 1] / 2, f[f.shape[0] - 1] / 2)
# f = f[PtsFreq1:PtsFreq2]

NbrFamille = np.array([150, 200, 300, 250])   # 4 familles pour 900 signaux
# NbrFamille = np.array([2, 2, 2, 2])  
Visu = 1

y = np.zeros((np.sum(NbrFamille[0:4]), NbrPts),'float')

#
### Famille n°1
#
f1 = 5500 - 1500 * (np.random.rand(NbrFamille[0]) - 0.5)
a1 = 1 - 0.2 * (np.random.rand(NbrFamille[0]) - 0.5)
AmpB = 0.05

for i in range(NbrFamille[0]):
    y[i] = np.multiply(a1[i], np.sin(2 * np.pi * f1[i] * t)) + AmpB * np.random.randn(NbrPts)
    if i == Visu:
        print("Famille 1")

        pl.figure(3 * i + 1)
        pl.plot(t, y[i], '-')
        pl.xlabel("t (s)")
        pl.grid(True)
        pl.title("Famille 1")

        SpectreM_y = abs(np.fft.fft(y[i]));
        SpectreP_y = np.unwrap(np.angle(np.fft.fft(y[i])))
        # SpectreM_y=SpectreM_y[PtsFreq1:PtsFreq2]
        # SpectreP_y=SpectreP_y[PtsFreq1:PtsFreq2]
        pl.figure(3 * i + 2)
        pl.plot(f, 20 * np.log10(SpectreM_y), '-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Module spectre famille 1")

        pl.figure(3 * i + 3)
        pl.plot(f, 20 * SpectreP_y,'-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Phase spectre famille 1")

        pl.figure(3 * i + 4)
        enveloppe = np.abs(hilbert(y[i]))
        pl.plot(t, enveloppe)
        pl.grid(True)
        pl.xlabel("t (s)")
        pl.title("Enveloppe famille 1")

        print(f"Energie = {np.sum(np.abs(y[i])**2)}")
        print(f"Nb de env < 0.2 = {np.count_nonzero(np.round_(enveloppe, decimals=1) < 0.2)}")
        seuil_freq = np.max((20 * np.log10(SpectreM_y))) - (np.max(20 * np.log10(SpectreM_y)) * 5 / 100)
        print(f"seuil = {seuil_freq}")
        freq_peaks = np.where((20 * np.log10(SpectreM_y)) > seuil_freq)
        print(f"max freq = {freq_peaks}")
pl.show()

#
### Famille n°2
#
f1 = 5500 - 1500 * (np.random.rand(NbrFamille[1]) - 0.5)
Delta = 200 * (np.random.rand(NbrFamille[1]) - 0.5)
f2 = f1 + 100 * np.sign(Delta) + Delta
a1 = 1 - 0.25 * (np.random.rand(NbrFamille[1]) - 0.5)
AmpB = 0.05

for i in range(NbrFamille[1]):
    y[np.sum(NbrFamille[0:1]) + i] = 0.5 * np.multiply(a1[i], (np.sin(2 * np.pi * f1[i] * t) + np.cos(2 * np.pi * f2[i] * t))) + AmpB * np.random.randn(NbrPts)
    if i == Visu:
        print("Famille 2")

        pl.figure(3 * i + 1)
        pl.plot(t, y[np.sum(NbrFamille[0:1]) + i], '-')
        pl.xlabel("t (s)")
        pl.grid(True)
        pl.title("Famille 2")

        SpectreM_y = abs(np.fft.fft(y[np.sum(NbrFamille[0:1]) + i]));
        SpectreP_y = np.unwrap(np.angle(np.fft.fft(y[np.sum(NbrFamille[0:1]) + i])))
        # SpectreM_y = SpectreM_y[PtsFreq1:PtsFreq2]
        # SpectreP_y = SpectreP_y[PtsFreq1:PtsFreq2]
        pl.figure(3 * i + 2)
        pl.plot(f, 20 * np.log10(SpectreM_y), '-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Module spectre famille 2")
        
        pl.figure(3 * i + 3)
        pl.plot(f, 20 * SpectreP_y, '-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Phase spectre famille 2")

        pl.figure(3 * i + 4)
        enveloppe = np.abs(hilbert(y[np.sum(NbrFamille[0:1]) + i]))
        pl.plot(t, enveloppe)
        pl.grid(True)
        pl.xlabel("t (s)")
        pl.title("Enveloppe famille 2")

        print(f"Energie = {np.sum(np.abs(y[np.sum(NbrFamille[0:1]) + i])**2)}")
        print(f"Nb de env < 0.2 = {np.count_nonzero(np.round_(enveloppe, decimals=1) < 0.2)}")
        seuil_freq = np.max((20 * np.log10(SpectreM_y))) - (np.max(20 * np.log10(SpectreM_y)) * 5 / 100)
        print(f"seuil = {seuil_freq}")
        freq_peaks = np.where((20 * np.log10(SpectreM_y)) > seuil_freq)
        print(f"max freq = {freq_peaks}")
pl.show()

#
### Famille n°3
#
f1 = 5500 - 1500 * (np.random.rand(NbrFamille[2]) - 0.5)
Delta = np.asarray(np.round(800 - 500 * (np.random.rand(NbrFamille[2]) - 0.5)), dtype="int")
a1 = 1 - 0.25 * (np.random.rand(NbrFamille[2]) - 0.5)
AmpB = 0.05

for i in range(NbrFamille[2]):
    # print(Delta[i])
    y[np.sum(NbrFamille[0:2]) + i] = np.multiply(a1[i], np.multiply(np.sin(2 * np.pi * f1[i] * t), np.concatenate((np.hanning(Delta[i]), np.zeros(NbrPts - Delta[i])), axis=0))) + AmpB * np.random.randn(NbrPts)

    if i == Visu:
        print("Famille 3")

        pl.figure(3 * i + 1)
        pl.plot(t, y[np.sum(NbrFamille[0:2]) + i], '-')
        pl.xlabel("t (s)")
        pl.grid(True)
        pl.title("Famille 3")

        SpectreM_y = abs(np.fft.fft(y[np.sum(NbrFamille[0:2]) + i]))
        SpectreP_y = np.unwrap(np.angle(np.fft.fft(y[np.sum(NbrFamille[0:2]) + i])))
        # SpectreM_y=SpectreM_y[PtsFreq1:PtsFreq2]
        # SpectreP_y=SpectreP_y[PtsFreq1:PtsFreq2]
        pl.figure(3 * i + 2)
        pl.plot(f, 20 * np.log10(SpectreM_y), '-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Module spectre famille 3")

        pl.figure(3 * i + 3)
        pl.plot(f, 20 * SpectreP_y, '-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Phase spectre famille 3")

        pl.figure(3 * i + 4)
        enveloppe = np.abs(hilbert(y[np.sum(NbrFamille[0:2]) + i]))
        pl.plot(t, enveloppe)
        pl.grid(True)
        pl.xlabel("t (s)")
        pl.title("Enveloppe famille 3")

        print(f"Energie = {np.sum(np.abs(y[np.sum(NbrFamille[0:2]) + i])**2)}")
        print(f"Nb de env < 0.2 = {np.count_nonzero(np.round_(enveloppe, decimals=1) < 0.2)}")
        seuil_freq = np.max((20 * np.log10(SpectreM_y))) - (np.max(20 * np.log10(SpectreM_y)) * 5 / 100)
        print(f"seuil = {seuil_freq}")
        freq_peaks = np.where((20 * np.log10(SpectreM_y)) > seuil_freq);
        print(f"max freq = {freq_peaks}")
pl.show()

#
### Famille n°4
#
f1 = 5500 - 800 * (np.random.rand(NbrFamille[3]) - 0.5)
f2 = 7500 - 1200 * (np.random.rand(NbrFamille[3]) - 0.5)
Delta1 = np.asarray(np.round(600 - 200 * (np.random.rand(NbrFamille[3]) - 0.5)), dtype="int")
Delta2 = np.asarray(np.round(1000 - 600 * (np.random.rand(NbrFamille[3]) - 0.5)), dtype="int")
a1 = 1 - 0.25 * (np.random.rand(NbrFamille[3]) - 0.5)
AmpB = 0.05

for i in range(NbrFamille[3]):
    # print(NbrPts, Delta1[i], Delta2[i], NbrPts - Delta1[i] - Delta2[i])
    y[np.sum(NbrFamille[0:3]) + i] = np.multiply(a1[i], np.multiply(np.sin(2 * np.pi * f1[i] * t), np.concatenate((np.hanning(Delta1[i]), np.zeros(NbrPts - Delta1[i])), axis=0)) + np.multiply(np.sin(2 * np.pi * f2[i] * t), np.concatenate((np.zeros(Delta1[i]), np.hanning(Delta2[i]), np.zeros(NbrPts - Delta1[i] - Delta2[i])), axis=0))) + AmpB * np.random.randn(NbrPts)

    if i == Visu:
        print("Famille 4")

        pl.figure(3 * i + 1)
        pl.plot(t, y[np.sum(NbrFamille[0:3]) + i], '-')
        pl.xlabel("t (s)")
        pl.grid(True)
        pl.title("Famille 4")

        SpectreM_y = abs(np.fft.fft(y[np.sum(NbrFamille[0:3]) + i]))
        SpectreP_y = np.unwrap(np.angle(np.fft.fft(y[np.sum(NbrFamille[0:3]) + i])))
        # SpectreM_y=SpectreM_y[PtsFreq1:PtsFreq2]
        # SpectreP_y=SpectreP_y[PtsFreq1:PtsFreq2]
        pl.figure(3 * i + 2)
        pl.plot(f, 20 * np.log10(SpectreM_y), '-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Module spectre famille 4")

        pl.figure(3 * i + 3)
        pl.plot(f, 20 * SpectreP_y, '-')
        pl.xlabel("f (Hz)")
        pl.grid(True)
        pl.title("Phase spectre famille 4")

        pl.figure(3 * i + 4)
        enveloppe = np.abs(hilbert(y[np.sum(NbrFamille[0:3]) + i]))
        pl.plot(t, enveloppe)
        pl.grid(True)
        pl.xlabel("t (s)")
        pl.title("Enveloppe famille 4")

        print(f"Energie = {np.sum(np.abs(y[np.sum(NbrFamille[0:3]) + i])**2)}")
        print(f"Nb de env < 0.2 = {np.count_nonzero(np.round_(enveloppe, decimals=1) < 0.2)}")
        seuil_freq = np.max((20 * np.log10(SpectreM_y))) - (np.max(20 * np.log10(SpectreM_y)) * 5 / 100)
        print(f"seuil = {seuil_freq}")
        freq_peaks = np.where((20 * np.log10(SpectreM_y)) > seuil_freq);
        print(f"max freq = {freq_peaks}")
pl.show()

#
### Sauvegarde
#
FichierDestination = open("Signaux.txt", "w")
FichierDestination.write("#--------------------------------------------------------\n")
FichierDestination.write("# Fichier généré par genere.py / jft le 30 Aout 2022     \n")
FichierDestination.write("#--------------------------------------------------------\n")

FichierDestination = open("Signaux.txt", "a+")

for i in range(np.sum(NbrFamille[0:4])):
    noClasse = 1
    for j in range(np.size(NbrFamille)):
        if i >= np.sum(NbrFamille[0:j]):
            noClasse = j + 1
    # print(noClasse)

    ligne = np.concatenate(([noClasse], y[i]), axis=0)
    ligne.tofile(FichierDestination, sep=',', format="%10.4f")
    FichierDestination.write("\n")

FichierDestination.close()
