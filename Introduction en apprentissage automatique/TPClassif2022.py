import numpy as np
import matplotlib.pyplot as plt
import TPClassif as tpc
from math import sqrt
from scipy.signal import hilbert
from sklearn.cluster import KMeans
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, plot_confusion_matrix


#====================================================================
# Master 1 SIA       Classif signaux
#====================================================================
#
#                 Lecture du fichier de donnees
#
#====================================================================
#
# NbrIndividus   : nbr de lignes dans le fichier
# NbrVariables   : nbr de colonnes dans la ligne
# MatriceDonnees : Matrice NbrIndividus x NbrParametres
# NoClasse       : Numero de classe de l'indivinu
# 
def lecture_fichier(nom_fichier):
    MatriceDonnees = np.ndarray([], dtype=float)
    NoClasse = np.array([], dtype=int)

    source = open(nom_fichier, "r")

    NbrIndividus = 0
    FinFichier = False
    while not FinFichier:
        ligne = source.readline().rstrip('\n\r')
        FinFichier = len(ligne) == 0
        # print(ligne)

        if (not FinFichier) and (ligne[0:1] != "#"):
            # Extraction des donnees de la ligne separees par une virgule
            donnees = ligne.rstrip('\n\r').split(",")
            NbrVariables = len(donnees) - 1
            NbrIndividus += 1

            Data = np.array([], dtype=float)
            # print('/',donnees[0].strip(),'/')
            NoClasse = np.append(NoClasse, [int(float(donnees[0].strip()))], axis=0)
            Data = np.array(donnees[1:], dtype=float)
            if NbrIndividus > 2:
                MatriceDonnees = np.append(MatriceDonnees, [Data], axis=0)
            else:
                if NbrIndividus == 2:
                    MatriceDonnees = np.append([MatriceDonnees], [Data], axis=0)
                else:
                    MatriceDonnees = Data

    ### Fermerture du fichier source
    source.close()
    #----------------------------------------------------------------------------
    print("Nombre d'individus : %d"  % NbrIndividus)
    print("Nombre de variables : %d\n" % NbrVariables)

    return MatriceDonnees, NoClasse


def extraire_donnees(x):
#---------------------------------------------------------------------------------
# function [donnees] = extraire_donnees(individu);
#
# Calcul des trois parametres representatifs des signaux.
#
#---------------------------------------------------------------------------------
# Entree : Individu / Signal
#
# Sortie : donnees           [1 x 3]
#                            Notes: donnees[0] -> energie
#                                   donnees[1] -> nb de points = 0 de l'enveloppe
#                                   donnees[2] -> delda_freq = pic_freq_max - pic_freq_min
#---------------------------------------------------------------------------------
    spectreM = 20 * np.log10(abs(np.fft.fft(x)))
    enveloppe = np.abs(hilbert(x))
    energie = np.sum(np.abs(x)**2)
    seuil_freq = np.max(spectreM) - (np.max(spectreM) / 10)
    freq_pics = np.where(spectreM > seuil_freq)[0]
    delta_pics = freq_pics[freq_pics.shape[0] - 1] - freq_pics[int(freq_pics.shape[0] / 2)]
    return [energie, np.count_nonzero(np.round_(enveloppe, decimals=1) < 0.2), delta_pics]


### Analyse Factorielle Discriminante
def AFD(data, NoClasses):
#---------------------------------------------------------------------------------
# function [proj] = AFD(donnees, NoClasses);
#
# Projection des individus par rapport au nouvel axe cree par AFD
#
#---------------------------------------------------------------------------------
# Entree : Donnees extraites
#
# Sortie : projections       [nb_individus x 3]
#---------------------------------------------------------------------------------
    centres_gravite = tpc.CalculerCentresGravite(data, NoClasses)
    cov_tot, cov_intra, cov_inter = tpc.CalculerMatricesCovariance(data, NoClasses, centres_gravite)
    val_propres, vec_propres = np.linalg.eig(np.linalg.inv(cov_intra).dot(cov_inter))
    i = np.argsort(val_propres)[::-1]
    val_propres = val_propres[i]
    vec_propres = vec_propres[:, i]
    return np.dot(data, vec_propres.T)


### Analyse en Composantes Principales
def ACP(data, NoClasses):
#---------------------------------------------------------------------------------
# function [proj] = ACP(donnees, NoClasses);
#
# Projection des individus par rapport au nouvel axe cree par ACP
#
#---------------------------------------------------------------------------------
# Entree : Donnees extraites
#
# Sortie : projections       [nb_individus x 3]
#---------------------------------------------------------------------------------
    centres_gravite = tpc.CalculerCentresGravite(data, NoClasses)
    cov_tot, cov_intra, cov_inter = tpc.CalculerMatricesCovariance(data, NoClasses, centres_gravite)
    val_propres, vec_propres = np.linalg.eig(cov_tot)
    i = np.argsort(val_propres)[::-1]
    val_propres = val_propres[i]
    vec_propres = vec_propres[:, i]
    return np.dot(data, vec_propres.T)

print("Jeu d'apprentissage")
MatriceDonneesTrain, NoClasse = lecture_fichier("train.txt") ## Lecture du fichier du jeu d'apprentissage
print("Jeu de test")
MatriceDonneesTest, NoClasse = lecture_fichier("test.txt") ## Lecture du fichier du jeu de test

MatriceDonneesTrainCR = tpc.CalculerIndividusCentresReduits(MatriceDonneesTrain, tpc.CalculerCentresGravite(MatriceDonneesTrain, NoClasse))
MatriceDonneesTestCR = tpc.CalculerIndividusCentresReduits(MatriceDonneesTest, tpc.CalculerCentresGravite(MatriceDonneesTest, NoClasse))

### Creation de la matrice des individus et de leurs caracteristiques
data_train = []
data_test = []
# data_train_cr = []
# data_test_cr = []
for train, test, train_cr, test_cr in zip(MatriceDonneesTrain, MatriceDonneesTest, MatriceDonneesTrainCR, MatriceDonneesTestCR):
    data_train.append(extraire_donnees(train))
    data_test.append(extraire_donnees(test))
    # data_train_cr.append(extraire_donnees(train_cr))
    # data_test_cr.append(extraire_donnees(test_cr))
data_train = np.array(data_train)
data_test = np.array(data_test)
# data_cr = np.array(data_train_cr)
# data_test_cr = np.array(data_test_cr)


### AFD
# proj_train_data = AFD(data_train[:, [1, 2]], NoClasse)
# proj_test_data = AFD(data_test[:, [1, 2]], NoClasse)
# proj_train_data_cr = AFD(data_train_cr[:, [1, 2]], NoClasse)
# proj_test_data_cr = AFD(data_test_cr[:, [1, 2]], NoClasse)


### ACP
proj_train_data = ACP(data_train[:, [0, 1]], NoClasse)
proj_test_data = ACP(data_test[:, [0, 1]], NoClasse)
# proj_train_data_cr = ACP(data_train_cr[:, [1, 2]], NoClasse)
# proj_test_data_cr = ACP(data_test_cr[:, [1, 2]], NoClasse)


def centre_mobiles(X_train, NoClasses, X_test):
    classifier = KMeans(n_clusters=4)
    classifier.fit(X_train, NoClasses)
    y_predicted = classifier.predict(X_test)
    return y_predicted


def perceptron_multicouche(X_train, NoClasses, X_test):
    classifier = MLPClassifier(activation="tanh", hidden_layer_sizes=(4,), max_iter=2000)
    classifier.fit(X_train, NoClasses)
    y_predicted = classifier.predict(X_test)
    return y_predicted


def SVM(X_train, NoClasses, X_test):
    classifier = SVC(C=0.01)
    classifier.fit(X_train, NoClasses)
    y_predicted = classifier.predict(X_test)
    return y_predicted


def foret_arbre_decisionnels(X_train, NoClasses, X_test):
    classifier = RandomForestClassifier(n_estimators=50, min_samples_split=10, criterion="entropy")
    classifier.fit(X_train, NoClasses)
    y_predicted = classifier.predict(X_test)
    return y_predicted


kmeans = centre_mobiles(proj_train_data, NoClasse, proj_test_data)
print(f"Nombre de signaux par groupe avec KMeans: [{np.count_nonzero(kmeans == 0)}, {np.count_nonzero(kmeans == 1)}, {np.count_nonzero(kmeans == 2)}, {np.count_nonzero(kmeans == 3)}]")
mlp = perceptron_multicouche(proj_train_data, NoClasse, proj_test_data)
print("Nombre de signaux bien classes avec MLPClassifier:", np.count_nonzero(NoClasse == mlp))
svm = SVM(proj_train_data, NoClasse, proj_test_data)
print("Nombre de signaux bien classes avec SVC:", np.count_nonzero(NoClasse == svm))
forest = foret_arbre_decisionnels(proj_train_data, NoClasse, proj_test_data)
print("Nombre de signaux bien classes avec RandomForestClassifier:", np.count_nonzero(NoClasse == forest))
