close all; clear all; clc;

load('signal.mat');

% Q1
N = length(signalModule);
t = (0:(N-1)) / fe;
f = (-(N-1)/2:(N-1)/2) * fe/N;

figure, plot(t(1:50000), signalModule(1:50000)), title("Quelques echantillons du signal module");

f_hor = 1 / (0.00289925 - 0.00279925);
Th = 1 / f_hor;

tf = fft(signalModule);
figure, plot(f, fftshift(abs(tf))), title("Module du signal module");
%figure, plot(f, fftshift(angle(tf))), title("Phase du signal module");

fp = 1000000;

% Q2
fc = 1000;
a = 2 * signalModule .* cos(2 * pi * fp * t);
fil = fir1(100, fc / (fe / 2));
ak = filter(fil, 1, a);
figure, plot(t(1:100000), ak(1:100000));

b = 2 * signalModule .* sin(2 * pi * fp * t);
bk = filter(fil, 1, b);
figure, plot(t(1:100000), bk(1:100000));

% Q3
P = round(fe / f_hor);
figure, scatter(ak(P/2:P:N), bk(P/2:P:N), 'filled'), title("Constellation");

bruit = (5 * rand(1, N));
signalModuleB = signalModule + bruit;
ab = 2 * signalModuleB .* cos(2 * pi * fp * t);
fil = fir1(100, fc / (fe / 2));
akb = filter(fil, 1, ab);

bb = 2 * signalModuleB .* sin(2 * pi * fp * t);
bkb = filter(fil, 1, bb);
figure, scatter(akb((P/2):P:N), bkb((P/2):P:N), 'filled'), title("Constellation bruitee");

% Q4
points = [akb(P/2:P:N); bkb(P/2:P:N)]';
dist = zeros(1, length(points));
const = [-2 -1.73; -2 1.73; 0 -1.73; 0 1.73; 2 -1.73; 2 1.73; -1 0; 1 0];
for i = 1:length(points)
    d = sqrt((points(i, 1) - const(:, 1)).^2 + (points(i, 2) - const(:, 2)).^2);
    [~, dist(i)] = min(d);
end

% Q5
codes = ["101"; "111"; "001"; "110"; "000"; "100"; "011"; "010"];
for i = 1:3:(length(dist)-3)
    bin = sprintf("%s%s%s", codes(dist(i), :), codes(dist(i+1), :), codes(dist(i+2), :));
    bin_msg(i, :) = str2num(bin(:));
    ascii = bin2dec(bin);
    message(i) = char(ascii);
end
bin_msg = logical(reshape(bin_msg, 1, []));

% Q6
points = [ak(P/2:P:N); bk(P/2:P:N)]';
dist = zeros(1, length(points));
for i = 1:length(points)
    d = sqrt((points(i, 1) - const(:, 1)).^2 + (points(i, 2) - const(:, 2)).^2);
    [~, dist(i)] = min(d);
end

for i = 1:3:(length(dist)-3)
    bin = sprintf("%s%s%s", codes(dist(i), :), codes(dist(i+1), :), codes(dist(i+2), :));
    vrai_bin_msg(i, :) = str2num(bin(:));
end
vrai_bin_msg = logical(reshape(vrai_bin_msg, 1, []));

nb_erreurs = sum(xor(bin_msg, vrai_bin_msg));
BER = nb_erreurs / length(vrai_bin_msg);

s = signalModuleB ./ cos(2 * pi * fp * t);
Eb = (abs(s).^2 * Th) ./ (2 * log2(8));
No = sum(bruit(1:Th+1)) / Th;

%figure, plot((Eb / No)(1:5000), BER), title("Taux d'erreur de bit pour un bruit de variance 5V");

