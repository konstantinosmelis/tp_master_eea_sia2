function [r c] = nonmaxsup(M, WindowSize, N)
%NONMAXSUP Non-maximal suppresion.
%   [R C] = NONMAXSUP(M, WindowSize, N) obtains the indexes for the rows R
%   and columns C of the N largest values of matrix M performing
%   non-maximal suppression around a neighbourhood of size "WindowSize"
%   (which must be an odd number).
%                                 OERP, Unversite de Toulouse III (UPS)

ws = (WindowSize-1)/2;                          % ws: half of the window size
for k=1:N 
    maxM = max(M(:));                           % Maximum value of M
    [rtmp ctmp] = find(M==maxM);                % Coordinates of the maximum values: r,c
    r(k)=rtmp(1); c(k)=ctmp(1);                 % Select the first maximum
    ci = c(k)-ws; cf = c(k)+ws; ri = r(k)-ws; rf = r(k)+ws; % Limits of the window centered on r,c
    
    % Ensure that the window is inside the image
    if c(k)-ws<1,         ci=1;         end     
    if c(k)+ws>size(M,2), cf=size(M,2); end
    if r(k)-ws<1,         ri=1;         end
    if r(k)+ws>size(M,1), rf=size(M,1); end
    
    % Set the elements of the window to the lowest possible value
    M(ri:rf,ci:cf) = -eps;
end

