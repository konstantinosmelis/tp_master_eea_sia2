function [r,c] = createGrid(r1,c1,r2,c2,r3,c3,r4,c4,NR,NC)
%CREATEGRID Creates a uniform grid
%   [R,C] = createGrid(r1,c1,r2,c2,r3,c3,r4,c4,Nrows,Ncols) generates a
%   uniform grid with coordinates R,C (R = rows and C = columns) centered
%   on a box with coordinates p1=(r1,c1), p2=(r2,c2), p3=(r3,c3), 
%   p4=(r4,c4) specified as:
%                p1     p2
%                p3     p4
%   The values of Nrows and Ncols constitute the number of rows and columns
%   that the grid will have, and the total number of elements in R and C
%                                OERP, Universite de Toulouse III (UPS)


% Center of the middle box
ro = (r1+r2+r3+r4)/4;
co = (c1+c2+c3+c4)/4;

% Lengths of the box in the row and column directions
dr = round((r3-r1 + r4-r2)/2); 
dc = round((c2-c1 + c4-c3)/2); 

% Half the length of the box
dr2 = round(dr/2);
dc2 = round(dc/2);

% Ajouter les points dans l'image
Nr = NR/2;           % Number of points in each row
Nc = NC/2;           % Number of points in each column
ru1 = []; cu1 = [];

% Generation of the 4 small subgrids
r0 = ro+dr2; Dr=dr;    c0 = co+dc2; Dc=dc;
for i=0:Nr-1      
    for j=0:Nc-1
        ru1 = [ru1 r0+i*Dr]; cu1 = [cu1 c0+j*Dc];
    end
end

r0 = ro+dr2; Dr=dr;   c0 = co-dc2; Dc=-dc;
for i=0:Nr-1      
    for j=0:Nc-1
        ru1 = [ru1 r0+i*Dr]; cu1 = [cu1 c0+j*Dc];
    end
end

r0 = ro-dr2; Dr=-dr;   c0 = co+dc2; Dc=dc;
for i=0:Nr-1      
    for j=0:Nc-1
        ru1 = [ru1 r0+i*Dr]; cu1 = [cu1 c0+j*Dc];
    end
end

r0 = ro-dr2; Dr=-dr;   c0 = co-dc2; Dc=-dc;
for i=0:Nr-1      
    for j=0:Nc-1
        ru1 = [ru1 r0+i*Dr]; cu1 = [cu1 c0+j*Dc];
    end
end

% Order the rows and columns
[r, c] = orderElements(ru1, cu1, NC);

