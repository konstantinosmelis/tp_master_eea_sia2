function [rows_out, cols_out] = orderElements(rows, cols, Ncol)
%ORDERELEMENTS Order top-down and left-right
%   [ROWS_OUT  COLS_OUT] = orderElements(ROWS, COLS, NC) orders the 
%   coordinates of several points in the image from top to down and left 
%   to right. The coordinates are given by the elements of ROWS and COLS 
%   (columns). The assumption is that all rows have the same fixed number 
%   of NC columns.
%                                OERP, Universite de Toulouse III (UPS)


if (nargin<3)
    error('Insufficient arguments: 3 elements expected')
end

%Temporal variables 
rows_temp = rows;   cols_temp = cols;

%Order the points from top to bottom using the rows information
[val index] = sort(rows);
rows = rows(index);   
cols = cols(index);

% For each row (or "approximation" of row), order the columns knowing that
% the number of columns is constant for all the rows
for i=1:(length(rows)/Ncol)
    cols_temp = cols( (i-1)*Ncol+1 : Ncol*i );
    rows_temp = rows( (i-1)*Ncol+1 : Ncol*i );
    [val index] = sort(cols_temp);
    rows_out((i-1)*Ncol+1:Ncol*i) = rows_temp(index);
    cols_out((i-1)*Ncol+1:Ncol*i) = cols_temp(index);
end
