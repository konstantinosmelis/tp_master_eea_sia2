function [r, c] = findCorners(im, N, WindowSize, doPlot)
%FINDCORNERS Find the corners in an image
%   [R C] = findCorners(IMAGE, N, WIN_SIZE, PLOT) finds N corners in IMAGE
%   using the Harris corner criterion, and returns the coordinates (R,C) of
%   the corners, where R and C refer to row and column. A non-maximal 
%   suppression of the initially found corners is applied on a window with 
%   size WIN_SIZE (it must be odd) so that only the point with the largest 
%   value is kept. If the value of PLOT is 1, a plot of the original image 
%   with crosses on the corners is shown. If PLOT is not specified (or is 
%   set to 0), no image is shown.
%
%   [R C] = findCorners(IMAGE, N) finds N corners in IMAGE using a window 
%   size of 11. No plot is shown. Note: the values of the borders within
%   10 pixels are discarded. 
%   
%                                     OERP, Unversite de Toulouse III (UPS)

% Check the minimum number of arguments
if(nargin<2), error('Unsufficient number of arguments!'); end

% Default value if only 2 arguments
if(nargin==2)
    WindowSize = 11; doPlot = 0;
end

% Default value if only 3 arguments
if(nargin==3), doPlot = 0; end

% Check the type of the image and convert it to gray, if necessary
if(size(im,3)>1), im = rgb2gray(im); end

% Compute the image derivatives Ix and Iy
maskdx = [1 0 -1; 2 0 -2;  1  0 -1];
maskdy = [1 2  1; 0 0  0; -1 -2 -1];
Ix = imfilter(double(im),maskdx);
Iy = imfilter(double(im),maskdy);

% Generate a Gaussian filter 'g' of size 9x9 and standard deviation sigma=2.
g = fspecial('gaussian',9,2);

% Smooth the squared image derivatives to obtain Ix2, Iy2 and Ixy
Ix2 = conv2((Ix.*Ix),g,'same');
Iy2 = conv2((Iy.*Iy),g,'same');
Ixy = conv2((Ix.*Iy),g,'same');

% ------- Computation of Matrix R -------
R = zeros(size(im));                            % Allocation of memory
k = 0.04;                                       % Typical value
for i=2:size(im,1)-1
    for j=2:size(im,2)-1
        M(1,1) = sum( sum( Ix2(i-1:i+1,j-1:j+1) ) );
        M(1,2) = sum( sum( Ixy(i-1:i+1,j-1:j+1) ) );
        M(2,1) = M(1,2);
        M(2,2) = sum( sum( Iy2(i-1:i+1,j-1:j+1) ) );
        R(i,j) = M(1,1)*M(2,2)-M(1,2)*M(2,1)-k*(M(1,1)+M(2,2))^2;
    end
end
% Discard the values of the borders
delta = 10;
R(1:delta,:) = -eps;         R(:,end-delta:end) = -eps;
R(end-delta:end,:) = -eps;   R(:,1:delta) = -eps;

% ------- Non-Maximal suppression for matrix R -------
[r c] = nonmaxsup(R, WindowSize, N);              % Non-Maximal suppression
for i=1:N, features(i).px_r=r(i); features(i).py_r=c(i); end

% ------- Plot the image with the corners -------
if (doPlot)
    imshow(im); hold on;            
    for i=1:size(features,2)
        % Show crosses on the image
        plot(features(i).py_r, features(i).px_r, 'r+');           
        % Show numbers on the image
        text(features(i).py_r+3, features(i).px_r+3,num2str(i), 'Color','b');  
    end
end
