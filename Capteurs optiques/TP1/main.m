% ========================================================================
%                    M1 SIA/ESET - TP1 MATLAB
%            Capteurs optiques et formation des images
%                    MELISSARATOS KONSTANTINOS
% ========================================================================

% Ajouter le chemin vers les fonctions auxiliaires
addpath('./utils')


%% =========== Initialisation =============================================

% Lire et afficher l'image distordue
imd = imread('grid-dist.png');
figure, imshow(imd) 
title('Image distordue: grid-dist.png'), drawnow

% Trouver les "coins"
N = 80;                  % Nombre désiré de coins 
windowSize = 21;         % Taille de la fenétre pour la suppression des points voisins
generate_plot = 0;       % Afficher l'image avec les 'coins': 0=non, 1=oui
Ncols = 10;              % Nombre de colonnes dans chaque ligne
[Yd1, Xd1] = findCorners(imd, N, windowSize, generate_plot);   % Trouver les 'coins'
[Yd, Xd]   = orderElements(Yd1, Xd1, Ncols);                   % Trier les points

% Afficher l'image originale avec les 'coins' triés (rouge).
padded_img = [imd zeros(size(imd,1),60)];
figure, imshow(padded_img); hold on;     
for i=1:N
    plot(Xd(i), Yd(i), 'r+');                         % Afficher les croix
    text(Xd(i)+3, Yd(i)+3, num2str(i), 'Color','r');  % Afficher les nombres
end
title('Coins dans l''image distordue')

% Générer le quadrillage autour du carré du milieu de l'image
Nc = 10; Nr = 8;               % Nombre de colonnes et lignes
[Yu,Xu] = createGrid(Yd(35), Xd(35), Yd(36), Xd(36), ...
                     Yd(45), Xd(45), Yd(46), Xd(46), Nr, Nc);

% Afficher le quadrillage désiré dans l'image distordue
figure, imshow(padded_img); hold on;     
for i=1:N
    plot(Xu(i), Yu(i), 'bo');                         
    plot(Xu(i), Yu(i), 'bx');                         
    text(Xu(i)+7, Yu(i)+3, num2str(i), 'Color','b');  
end
title('Quadrillage désiré (sans distorsion) dans l''image distordue')

% Afficher les points distordus et les désirés (non distordus)
figure
imshow(ones(size(padded_img))); hold on;
plot(Xd, Yd, 'ro'), hold on, plot(Xu, Yu, 'b+'), hold on, 
legend('distordu', 'désiré')
title('Points distordus et désirés ("sans distorsion")')

%% =========== Récupération des paramétres ================================

% Paramétres connus
alpha_v = 928.87667884511313;
alpha_u = 927.76480291420057;
xo = 509;
yo = 365;

% Coordonnées
xu=Xu'; yu=Yu'; 
xd=Xd'; yd=Yd';

% ----------- Compléter (Partie 1.1-3) -----------
% Calcul de R^2, R^4 et R^6
R = sqrt(((Xu - xo) ./ alpha_v).^2 + ((Yu - yo) ./ alpha_u).^2);
R2 = R.^2;
R4 = R.^4;
R6 = R.^6;

% Calcul des paramétres K
X = (xd - xu) ./ (xu - xo);
Y = (yd - yu) ./ (yu - yo);
K = pinv([R2' R4' R6' ; R2' R4' R6']) * [X; Y];

% Estimation de (xd,yd) en utilisant (xu,yu): eq (1)
xd_est = xu + (xu - xo) .* ([R2' R4' R6'] * K);    %r(xu, yu) 
yd_est = yu + (yu - yo) .* ([R2' R4' R6'] * K);    %s(xu, yu)

figure
imshow(ones(size(padded_img))); hold on;
% Affichage de (xd,yd), (xu,yu), (r,s)
plot(xd, yd, 'bo'), hold on, plot(xu, yu, 'r+'), hold on, plot(xd_est, yd_est, 'gx');


%% =========== Génération de l'image sans distortion ======================

Nrows = size(imd,1);              
Ncols = size(imd,2);
imu = uint8(zeros(size(imd)));    % Image corrigée
clear xd_est yd_est

% Pour tous les points dans l'image corrigée
for x=1:(Ncols-1)
    for y=1:(Nrows-1)
        % ----------- COMPLÉTER -----------
        % Calcul de R^2, R^4 et R^6
        R = sqrt(((x - xo) / alpha_v).^2 + ((y - yo) / alpha_u).^2);
        R2 = R.^2;
        R4 = R.^4;
        R6 = R.^6;
        
        % Calcul des valeurs estimées (xd_est, yd_est) qui correspondent é (xu,yu): eq(1)
        xd_est = x + (x - xo) .* ([R2' R4' R6'] * K);     % Modifier ici avec eq (1)
        yd_est = y + (y - xo) .* ([R2' R4' R6'] * K);     % Modifier ici avec eq (1) 
        
        % Valeurs pour l'interpolation bilinéaire
        xk = floor(xd_est);   dx = xd_est-xk;
        yk = floor(yd_est);   dy = yd_est-yk;

        % Interpolation bilinéaire
        if ( (xk>0) && (xk<Ncols) && (yk>0) && (yk<Nrows) )
            I1 = imd(yk, xk);
            I2 = imd(yk, xk + 1);
            I3 = imd(yk + 1, xk);
            I4 = imd(yk + 1, xk + 1);
            val = I1*(1-dx)*(1-dy) + I2*dx*(1-dy) + I3*(1-dx)*dy + I4*dx*dy;  % Modifier 'val' avec l'interpolation bilinéaire

            imu(y,x) = uint8(val);
        else
            imu(y,x) = uint8(0);
        end
    end
end

figure, imshow(imu)
title('Image corrigée')
