% ========================================================================
%                      M1 S14 - TP 2 MATLAB
%      Restauration interactive d'images par transformee de Fourier
%           Partie 2.3 - Correction d'une interference sinusoidale
% ========================================================================

% Ajouter le chemin vers les fonctions auxiliaires
addpath('./utils')
close all, clear all;

% Lecture et affichage de l'image
im = imread('louvreSin.png');
figure, imshow(im);


% ----------- COMPLETER --------------------
G = fftshift(fft2(im));
figure, imshow(log(1 + abs(G)), []);

u0 = 170;
v0 = 240;
u1 = 230;
v1 = 335;

type = 'ideal';
delta = 5;
h1 = pointfilter(type, size(G,1), size(G,2), u0, v0, 15, 2);
h2 = pointfilter(type, size(G,1), size(G,2), u1, v1, 15, 2);

h = h1 + h2;
GC = G .* (h == 0);
figure, imshow(log(1 + abs(GC)), []);

% Recuperer l'image corrigee
g = ifft2(ifftshift(GC));
figure, imshow(real(g), []);
