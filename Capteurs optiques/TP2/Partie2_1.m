% ========================================================================
%                      M1 S14 - TP 2 MATLAB
%      Restauration interactive d'images par transformee de Fourier
%           Partie 2.1 - Transformee de Fourier par l'exemple
% ========================================================================

% Ajouter le chemin vers les fonctions auxiliaires
addpath('./utils')
close all, clear all



%% =========== Image 1 ====================================================

% Lecture et affichage de l'image
im1 = imread('grille-n.png');
figure, subplot(3,3,1), imshow(im1);
title('Image (grille)');

% Calcul de la transformee de Fourier
Fim1 = fftshift(fft2(im1));

% Affichage de la T. Fourier sans transformation logarithmique
subplot(3,3,2), imshow(abs(Fim1),[]);
title('T.F. sans correction');

% Affichage de la T. Fourier avec transformation logarithmique
LFim1 = log(1+abs(Fim1));           
subplot(3,3,3), imshow( LFim1 ,[])
title('T.F. avec correction log')


% Filtrage de la ligne vertical et horizontal de la TF
Fim1_modif1 = Fim1;  Fim1_modif1(147:149,:) = 0;
Fim1_modif2 = Fim1;  Fim1_modif2(:,147:149) = 0;

% Inverse de la transformee de Fourier et affichage
im1_itf    = real( ifft2(ifftshift(Fim1)) );
im1_modif1 = real( ifft2(ifftshift(Fim1_modif1)) );
im1_modif2 = real( ifft2(ifftshift(Fim1_modif2)) );

% figure
subplot(3,3,4), imshow( log(1+abs(Fim1)), []), title('TF initial')
subplot(3,3,5), imshow( log(1+abs(Fim1_modif1)), []), title('TF - filtre 1')
axis on
subplot(3,3,6), imshow( log(1+abs(Fim1_modif2)), []), title('TF - filtre 2')
axis on
subplot(3,3,7), imshow( im1_itf ),    title('TF inverse initial')
subplot(3,3,8), imshow( im1_modif1 ), title('TF inverse - filtre 1')
subplot(3,3,9), imshow( im1_modif2 ), title('TF inverse - filtre 2')

%% =========== Images 2 ==============================================

% Lecture et affichage de l'image
im2 = rgb2gray(imread('gravier.png'));
im3 = rgb2gray(imread('tissu.png'));

% Calcul de la transform??e de Fourier
Fim2 = fftshift(fft2(im2));
Fim3 = fftshift(fft2(im3)); 

% Affichage
figure, 
subplot(2,2,1), imshow(im2); title('Image 2')
subplot(2,2,2), imshow(im3); title('Image 3')
subplot(2,2,3), imshow( log(1+abs(Fim2)), []); title('T Fourier image 2')
subplot(2,2,4), imshow( log(1+abs(Fim3)), []); title('T Fourier image 3')
axis on

%% Filtrage avec filtre passe-bas
D0 = [10, 25, 40]; type = 'ideal';
h1 = lpfilter(type, size(Fim2,1), size(Fim2,2), D0(1), 2);
h2 = lpfilter(type, size(Fim2,1), size(Fim2,2), D0(2), 2);
h3 = lpfilter(type, size(Fim2,1), size(Fim2,2), D0(3), 2);
Fim2filt1 = Fim2.*h1;
Fim2filt2 = Fim2.*h2;
Fim2filt3 = Fim2.*h3;
im2filt1 = real( ifft2(ifftshift(Fim2filt1)) );
im2filt2 = real( ifft2(ifftshift(Fim2filt2)) );
im2filt3 = real( ifft2(ifftshift(Fim2filt3)) );
im2if    = real( ifft2(ifftshift(Fim2)) );

figure, 
subplot(2,4,1), imshow(log(1+abs(Fim2)),[]), title('Sans filtre')
subplot(2,4,2), imshow(log(1+abs(Fim2filt1)),[]), title(['Passe-bas D=' num2str(D0(1))])
axis on
subplot(2,4,3), imshow(log(1+abs(Fim2filt2)),[]), title(['Passe-bas D=' num2str(D0(2))])
axis on
subplot(2,4,4), imshow(log(1+abs(Fim2filt3)),[]), title(['Passe-bas D=' num2str(D0(3))])
axis on
subplot(2,4,5), imshow(im2if,[]), title('Sans filtre')
subplot(2,4,6), imshow(im2filt1,[]), title(['Passe-bas D=' num2str(D0(1))])
subplot(2,4,7), imshow(im2filt2,[]), title(['Passe-bas D=' num2str(D0(2))])
subplot(2,4,8), imshow(im2filt3,[]), title(['Passe-bas D=' num2str(D0(3))])


%% Filtrage avec filtre passe-haut
D0 = [10, 25, 40]; type = 'ideal';
h1 = hpfilter(type, size(Fim2,1), size(Fim2,2), D0(1), 2);
h2 = hpfilter(type, size(Fim2,1), size(Fim2,2), D0(2), 2);
h3 = hpfilter(type, size(Fim2,1), size(Fim2,2), D0(3), 2);
Fim2filt1 = Fim2.*h1;
Fim2filt2 = Fim2.*h2;
Fim2filt3 = Fim2.*h3;
im2filt1 = real( ifft2(ifftshift(Fim2filt1)) );
im2filt2 = real( ifft2(ifftshift(Fim2filt2)) );
im2filt3 = real( ifft2(ifftshift(Fim2filt3)) );
im2if    = real( ifft2(ifftshift(Fim2)) );

figure, 
subplot(2,4,1), imshow(log(1+abs(Fim2)),[]), title('Sans filtre')
subplot(2,4,2), imshow(log(1+abs(Fim2filt1)),[]), title(['Passe-haut D=' num2str(D0(1))])
axis on
subplot(2,4,3), imshow(log(1+abs(Fim2filt2)),[]), title(['Passe-haut D=' num2str(D0(2))])
axis on
subplot(2,4,4), imshow(log(1+abs(Fim2filt3)),[]), title(['Passe-haut D=' num2str(D0(3))])
axis on
subplot(2,4,5), imshow(im2if,[]), title('Sans filtre')
subplot(2,4,6), imshow(im2filt1,[]), title(['Passe-haut D=' num2str(D0(1))])
subplot(2,4,7), imshow(im2filt2,[]), title(['Passe-haut D=' num2str(D0(2))])
subplot(2,4,8), imshow(im2filt3,[]), title(['Passe-haut D=' num2str(D0(3))])


% ----------- COMPLETER --------------------

% Filtrage avec filtre passe-bande
D0 = [10, 25, 40]; type = 'ideal';
delta=10;

h1 = lpfilter(type, size(Fim2,1), size(Fim2,2), D0(1)-delta/2, 2);
h2 = lpfilter(type, size(Fim2,1), size(Fim2,2), D0(2)-delta/2, 2);
h3 = lpfilter(type, size(Fim2,1), size(Fim2,2), D0(3)-delta/2, 2);
h1 = h1 + hpfilter(type, size(Fim2,1), size(Fim2,2), D0(1)+delta/2, 2);
h2 = h2 + hpfilter(type, size(Fim2,1), size(Fim2,2), D0(2)+delta/2, 2);
h3 = h3 + hpfilter(type, size(Fim2,1), size(Fim2,2), D0(3)+delta/2, 2);
Fim2filt1 = Fim2.*h1;
Fim2filt2 = Fim2.*h2;
Fim2filt3 = Fim2.*h3;
im2filt1 = real( ifft2(ifftshift(Fim2filt1)) );
im2filt2 = real( ifft2(ifftshift(Fim2filt2)) );
im2filt3 = real( ifft2(ifftshift(Fim2filt3)) );
im2if    = real( ifft2(ifftshift(Fim2)) );

figure, 
subplot(2,4,1), imshow(log(1+abs(Fim2)),[]), title('Sans filtre')
subplot(2,4,2), imshow(log(1+abs(Fim2filt1)),[]), title(['Passe-bande D=' num2str(D0(1))])
axis on
subplot(2,4,3), imshow(log(1+abs(Fim2filt2)),[]), title(['Passe-bande D=' num2str(D0(2))])
axis on
subplot(2,4,4), imshow(log(1+abs(Fim2filt3)),[]), title(['Passe-bande D=' num2str(D0(3))])
axis on
subplot(2,4,5), imshow(im2if,[]), title('Sans filtre')
subplot(2,4,6), imshow(im2filt1,[]), title(['Passe-bande D=' num2str(D0(1))])
subplot(2,4,7), imshow(im2filt2,[]), title(['Passe-bande D=' num2str(D0(2))])
subplot(2,4,8), imshow(im2filt3,[]), title(['Passe-bande D=' num2str(D0(3))])


%% =========== Images 3 ==============================================
% ----------- COMPLETER --------------------

% Faites la m??me chose avec l'image 'tissu.png'.
%% Filtrage avec filtre passe-bas
D0 = [10, 25, 40]; type = 'ideal';
h1 = lpfilter(type, size(Fim3,1), size(Fim3,2), D0(1), 2);
h2 = lpfilter(type, size(Fim3,1), size(Fim3,2), D0(2), 2);
h3 = lpfilter(type, size(Fim3,1), size(Fim3,2), D0(3), 2);
Fim3filt1 = Fim3.*h1;
Fim3filt2 = Fim3.*h2;
Fim3filt3 = Fim3.*h3;
im3filt1 = real( ifft2(ifftshift(Fim3filt1)) );
im3filt2 = real( ifft2(ifftshift(Fim3filt2)) );
im3filt3 = real( ifft2(ifftshift(Fim3filt3)) );
im3if    = real( ifft2(ifftshift(Fim3)) );

figure, 
subplot(2,4,1), imshow(log(1+abs(Fim3)),[]), title('Sans filtre')
subplot(2,4,2), imshow(log(1+abs(Fim3filt1)),[]), title(['Passe-bas D=' num2str(D0(1))])
axis on
subplot(2,4,3), imshow(log(1+abs(Fim3filt2)),[]), title(['Passe-bas D=' num2str(D0(2))])
axis on
subplot(2,4,4), imshow(log(1+abs(Fim3filt3)),[]), title(['Passe-bas D=' num2str(D0(3))])
axis on
subplot(2,4,5), imshow(im3if,[]), title('Sans filtre')
subplot(2,4,6), imshow(im3filt1,[]), title(['Passe-bas D=' num2str(D0(1))])
subplot(2,4,7), imshow(im3filt2,[]), title(['Passe-bas D=' num2str(D0(2))])
subplot(2,4,8), imshow(im3filt3,[]), title(['Passe-bas D=' num2str(D0(3))])


%% Filtrage avec filtre passe-haut
D0 = [10, 25, 40]; type = 'ideal';
h1 = hpfilter(type, size(Fim3,1), size(Fim3,2), D0(1), 2);
h2 = hpfilter(type, size(Fim3,1), size(Fim3,2), D0(2), 2);
h3 = hpfilter(type, size(Fim3,1), size(Fim3,2), D0(3), 2);
Fim3filt1 = Fim3.*h1;
Fim3filt2 = Fim3.*h2;
Fim3filt3 = Fim3.*h3;
im3filt1 = real( ifft2(ifftshift(Fim3filt1)) );
im3filt2 = real( ifft2(ifftshift(Fim3filt2)) );
im3filt3 = real( ifft2(ifftshift(Fim3filt3)) );
im3if    = real( ifft2(ifftshift(Fim3)) );

figure, 
subplot(2,4,1), imshow(log(1+abs(Fim3)),[]), title('Sans filtre')
subplot(2,4,2), imshow(log(1+abs(Fim3filt1)),[]), title(['Passe-haut D=' num2str(D0(1))])
axis on
subplot(2,4,3), imshow(log(1+abs(Fim3filt2)),[]), title(['Passe-haut D=' num2str(D0(2))])
axis on
subplot(2,4,4), imshow(log(1+abs(Fim3filt3)),[]), title(['Passe-haut D=' num2str(D0(3))])
axis on
subplot(2,4,5), imshow(im3if,[]), title('Sans filtre')
subplot(2,4,6), imshow(im3filt1,[]), title(['Passe-haut D=' num2str(D0(1))])
subplot(2,4,7), imshow(im3filt2,[]), title(['Passe-haut D=' num2str(D0(2))])
subplot(2,4,8), imshow(im3filt3,[]), title(['Passe-haut D=' num2str(D0(3))])

%% Filtrage avec filtre passe-bande
D0 = [10, 25, 40]; type = 'ideal';
delta = 10;
h1 = lpfilter(type, size(Fim3,1), size(Fim3,2), D0(1)-delta/2, 2);
h2 = lpfilter(type, size(Fim3,1), size(Fim3,2), D0(2)-delta/2, 2);
h3 = lpfilter(type, size(Fim3,1), size(Fim3,2), D0(3)-delta/2, 2);
h1 = h1 + hpfilter(type, size(Fim3,1), size(Fim3,2), D0(1)+delta/2, 2);
h2 = h2 + hpfilter(type, size(Fim3,1), size(Fim3,2), D0(2)+delta/2, 2);
h3 = h3 + hpfilter(type, size(Fim3,1), size(Fim3,2), D0(3)+delta/2, 2);
Fim3filt1 = Fim3.*h1;
Fim3filt2 = Fim3.*h2;
Fim3filt3 = Fim3.*h3;
im3filt1 = real( ifft2(ifftshift(Fim3filt1)) );
im3filt2 = real( ifft2(ifftshift(Fim3filt2)) );
im3filt3 = real( ifft2(ifftshift(Fim3filt3)) );
im3if    = real( ifft2(ifftshift(Fim3)) );

figure, 
subplot(2,4,1), imshow(log(1+abs(Fim3)),[]), title('Sans filtre')
subplot(2,4,2), imshow(log(1+abs(Fim3filt1)),[]), title(['Passe-bande D=' num2str(D0(1))])
axis on
subplot(2,4,3), imshow(log(1+abs(Fim3filt2)),[]), title(['Passe-bande D=' num2str(D0(2))])
axis on
subplot(2,4,4), imshow(log(1+abs(Fim3filt3)),[]), title(['Passe-bande D=' num2str(D0(3))])
axis on
subplot(2,4,5), imshow(im3if,[]), title('Sans filtre')
subplot(2,4,6), imshow(im3filt1,[]), title(['Passe-bande D=' num2str(D0(1))])
subplot(2,4,7), imshow(im3filt2,[]), title(['Passe-bande D=' num2str(D0(2))])
subplot(2,4,8), imshow(im3filt3,[]), title(['Passe-bande D=' num2str(D0(3))])