% ========================================================================
%                      M1 S14 - TP 2 MATLAB
%      Restauration interactive d'images par transformee de Fourier
%                 Partie 2.2 - Correccion d'un bouge
% ========================================================================

% Ajouter le chemin vers les fonctions auxiliaires
addpath('./utils')
close all, clear all;

% Lecture et affichage de l'image
im = imread('louvreBouge.png');
figure, imshow(im);

% ----------- COMPLETER --------------------
% Transformee de Fourier de l'image d'entree
G = fftshift(fft2(im));
figure, imshow(log(1 + abs(G)), []);


% Estimation de H
d = 1 / 20;   % 20 pixels entre les zeros du spectre
T = 2 * pi;
H = zeros(size(G));
for u = 1:size(G, 1)
    for v = 1:size(G, 2)
        k = v - (size(G, 2)/2);
        H(u, v) = T * exp(-1j*pi*k*T) * (sin(pi*k*d) / (pi*k*d));
    end
end
figure, imshow(abs(H), []);


% Estimation de F en frequence (corrigee)
F = G ./ H;
figure, imshow(log(1 + abs(F)), []);


% Recuperer l'image corrigee
f = real(ifft2(ifftshift(F)));
figure, imshow(f, []);
