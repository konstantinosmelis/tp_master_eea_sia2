function H = hpfilter(type, M, N, D0, n)
%HPFILTER Computes freq. domain highpass filters.
%   
%   THIS IS NOT A STANDARD MATLAB FUNCTION
%   H = hpfilter(type, M, N, D0, n) creates the transfer function of
%   a highpass filter, H, of the specified TYPE and size MxN. Acceptable
%   values for type, D0, and n are: 
%
%   'ideal'    Ideal highpass filter with cutoff frequency D0.  n
%              need not be supplied. D0 must be positive.
%
%   'btw'      Butterworth highpass filter of order n, and cutoff
%              D0.  The default value for n is 1.0. D0 must be
%              positive.
%
%   'gaussian' Gaussian highpass filter with cutoff (standard
%              deviation) D0. n need not be supplied. D0 must be
%              positive.
%   M and N should be even numbers for DFT filtering.
%
%   The transfer function Hhp of a highpass filter is 1 - Hlp, 
%   where Hlp is the transfer function of the corresponding lowpass 
%   filter.  Thus, we can use function lpfilter to generate highpass 
%   filters.
%
%   Class support: double, uint8, uint16
%   The output is of class double


if nargin == 4
   n = 1; % Default value of n.
end

% Generate highpass filter.
Hlp = lpfilter(type, M, N, D0, n);
H = 1 - Hlp;

%		End of function