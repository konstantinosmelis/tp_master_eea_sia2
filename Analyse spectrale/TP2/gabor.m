function win = gabor(N);

% GABOR   Gabor window.
%    GABOR(N) returns the N-point symmetric Gabor (Gaussian) window in a column 
%    vector. Note that the Gaussian is truncated in the interval
%    [-3sigma, 3sigma], where sigma is the standard deviation 

   P = (N-1)/2;
   t = (-P:P)/P;
   win = exp(-(3*t).^2/2)';





