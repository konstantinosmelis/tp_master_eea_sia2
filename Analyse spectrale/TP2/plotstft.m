% Construction du signal
  Fe =      % Fr�quence d'�chantillonnage
  dt =1/Fe; % Pas d'�chantillonnage
  N  =      % Nombre d'�chantillons du signal
  t  =      % Vecteur de temps
  f  =      % Vecteur de fr�quence
  x  =      % Signal � analyser

% Parametres de la STFT
  NFFT =    % taille des fft
  pas  =	   % pas d'�chantillonnage en temps de la STFT

% Choix de la fen�tre
  L   =     % longueur de la fen�tre
  fen =     % vecteur de la fen�tre

% Calcul de la Transform�e de Fourier � court terme
  [X,tp,fp] = stft(x,NFFT,Fe,fen,pas);


% Affichage
  figure(2); clf
% Transform�e de Fourier � court terme
  subplot('position',[0.1 0.1 0.67 0.58])
  imagesc(tp,fp,abs(X)); axis xy
  title('Spectrogramme')
  xlabel('Temps (sec)')
  ylabel('fr�quence (Hz)')
  Xlim = axis; Xlim=Xlim(1:2);

% P�riodogramme
  subplot('position',[0.85 0.1 0.14 0.58])
  N2 = 8*N;
  freq = (0:N2-1)/N2*Fe; TFx = fft(x,N2);
  plot(abs(TFx(1:end/2)).^2/N,freq(1:end/2))
  xlabel('Puissance')
  title('Spectre')

% Repr�sentation temporelle
  subplot('position',[0.1 0.75 0.67 0.2])
  plot(t,x)
  xlabel('Temps (sec)')
  ylabel('Amplitude')
  title('Repr�sentation temporelle du signal')
  ax = axis; ax(1:2) = Xlim; axis(ax)

% Barre des couleurs
  hand = subplot('position',[0.01 0.1 0.02 0.58]);
  colorbar(hand)




