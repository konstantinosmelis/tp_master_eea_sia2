clear all; close all; clc;

%% Analyse spectrale d'une sinusoide
% TFD d'une sinusoide
Fe = 1000;
N = 100;
t = (0:(N-1))/Fe;
f = (-(N-1)/2:(N-1)/2) * Fe/N;

% Question 2
x1 = cos(2 * pi * 100 * t); % signal sinusoidal de frequence f0=100
figure, plot(t, x1);

% Spectre de x(t)
tf_x1 = fft(x1);
figure, plot(f, fftshift(abs(tf_x1)));
% On peut bien retrouver la frequence de la sinusoide
% mais pas son amplitude.
% L'amplitude max de la TFD ne corespond pas à l'amplitude
% max du signal mais a N/2.


% Spectre normalise de x(t)
tf_x1_norm = tf_x1 * (2/N);
figure, plot(f, fftshift(abs(tf_x1_norm)));

ph = angle(tf_x1_norm);
figure, plot(f, fftshift(ph));
% Sur le graphe nous pouvons voir que à 
% l'origine, à f=-100 et f=100 la phase est nulle

% Question 3
x2 = cos(2 * pi * 95 * t); % signal sinusoidal de frequence f0=95
figure, plot(t, x2);

% Spectre de x2(t)
tf_x2 = fft(x2);
figure, plot(f, fftshift(abs(tf_x2)));
% On peut bien retrouver la frequence de la sinusoide
% mais pas son amplitude.
% Cependant dans ce cas l'amplitude de la TFD
% ne correspond pas a N / 2

% Spectre normalise de x(t)
tf_x2_norm = tf_x2 * (2/N);
figure, plot(f, fftshift(abs(tf_x2_norm)));
% Apres norlalisation du spectre nous ne retrouvons pas
% l'amplitude du signal

ph2 = angle(tf_x2_norm);
figure, plot(f, fftshift(ph2));
% Sur le graphe nous pouvons voir que à 
% l'origine, à f=-95 et f=95 la phase est nulle

% Question 4
er_x1 = abs((1 - max(tf_x1) * 2/N) / 1);
er_x2 = abs((1 - max(tf_x2) * 2/N) / 1);
fprintf("Q4 - Erreur relative en amplitude x1: %f, x2: %f\n", er_x1, er_x2);

% Question 5
N = 1000;
t = (0:N-1)/Fe; 
f = (-(N-1)/2: (N-1)/2) * Fe/N;

x1 = cos(2 * pi * 100 * t); % Signal a f0=100
%figure, plot(t, x1);

tf_x1 = fft(x1);
%figure, plot(f, fftshift(abs(tf_x1)));

tf_x1_norm = fft(x1) * (2/N);
%figure, plot(f, fftshift(abs(tf_x1_norm)));

x2 = cos(2 * pi * 99.5 * t); %Signal a f0=99.5
%figure, plot(t ,x2);

tf_x2 = fft(x2);
%figure, plot(f, fftshift(abs(tf_x2)));

tf_x2_norm = tf_x2 * (2/N);
%figure, plot(f, fftshift(abs(tf_x2_norm)));

% Erreur relative en amplitude
er_x1 = abs((1 - max(tf_x1) * 2/N) / 1);
er_x2 = abs((1 - max(tf_x2) * 2/N) / 1);
fprintf("Q5 - Erreur relative en amplitude x1: %f, x2: %f\n", er_x1, er_x2);


% Question 9
N = 100;
t = (0:N-1)/Fe; 

% Signal avec zero padding f0=100Hz
x1_zp = zeros(1, N*10);
x1_zp(1:N)= cos(2 * pi * 100 * t);

tf_x1_zp = fft(x1_zp);
tf_x1_zp_norm = tf_x1_zp * 2/N ;

% pour N'=1000
t1 = (0:N*10-1) / Fe; 
f1 = (-((N*10)-1)/2:((N*10)-1)/2) * Fe/(N*10) ;

figure, plot(t1, x1_zp), title("Signal Sinusoidal avec zero padding f=100Hz");
figure, plot(f1, fftshift(abs(tf_x1_zp))), title("Spectre Signal Sinusoidal avec zero padding f=100Hz");
figure, plot(f1, fftshift(abs(tf_x1_zp_norm))), title("Spectre Signal Sinusoidal normalisé avec zero padding f=100Hz");
% Grace au zero-padding on voir mieux
% le pic a la frequence du signal

% Signal avec zero padding f0=95Hz
x2_zp = zeros(1,N*10);
x2_zp(1:N) = cos(2 * pi * 95 * t);

tf_x2_zp = fft(x2_zp);
tf_x2_zp_norm = tf_x2_zp * 2/N ;

figure, plot(t1, x2_zp), title("Signal Sinusoidal avec zero padding f=95Hz");
figure, plot(f1, fftshift(abs(tf_x2_zp))), title("Spectre Signal Sinusoidal avec zero padding f=95Hz");
figure, plot(f1, fftshift(abs(tf_x2_zp_norm))), title("Spectre Signal Sinusoidal normalisé avec zero padding f=95Hz");
% Sur les figures on retrouve une
% frequence tres proche a celle du signal

% Erreur relative en amplitude
er_x1 = abs((1 - max(tf_x1_zp_norm)) / 1);
er_x2 = abs((1 - max(tf_x2_zp_norm)) / 1);
fprintf("Q9 - Erreur relative en amplitude x1: %f, x2: %f\n", er_x1, er_x2);

% Question 10
N = 100; 
t = (0:(N/2)-1) / Fe; 

t1 = (0:N-1) / Fe;
f1 = (-((N)-1)/2: ((N)-1)/2) * Fe/N ;

% Signal avec zero padding N/2 f0=100Hz
x1_zp_2 = zeros(1, N);
x1_zp_2(1:N/2) = cos(2 * pi * 100 * t);

tf_x1_zp_2 = fft(x1_zp_2);
tf_x1_zp_norm_2 = tf_x1_zp_2 * 4/N;

figure, plot(t1, x1_zp_2), title("Signal Sinusoidal avec zero padding f=100Hz");
figure, plot(f1, fftshift(abs(tf_x1_zp_2))), title("Spectre Signal Sinusoidal avec zero padding N/2 f=100Hz");
figure, plot(f1, fftshift(abs(tf_x1_zp_norm_2))), title("Spectre Signal Sinusoidal normalisé avec zero padding N/2 f=100Hz");
% On retrouve toujours les deux pics a f=-100 et f=100

% Signal avec zero padding N/2 f0=95Hz
x2_zp_2 = zeros(1, N);
x2_zp_2(1:N/2) = cos(2 * pi * 95 * t);

tf_x2_zp_2 = fft(x2_zp_2);
tf_x2_zp_norm_2 = tf_x2_zp_2 * 4/N ;

figure, plot(t1, x2_zp_2), title("Signal Sinusoidal avec zero padding N/2 f=95Hz");
figure, plot(f1, fftshift(abs(tf_x2_zp_2))); title("Spectre Signal Sinusoidal avec zero padding N/2 f=95Hz");
figure, plot(f1, fftshift(abs(tf_x2_zp_norm_2))); title("Spectre Signal Sinusoidal normalisé avec zero padding N/2 f=95Hz");

% Erreur relative en amplitude
er_x1 = abs((1 - max(tf_x1_zp_norm_2))/1);
er_x2 = abs((1 - max(tf_x2_zp_norm_2))/1);
fprintf("Q10 - Erreur relative en amplitude x1: %f, x2: %f\n", er_x1, er_x2);

% Fenetrage de la TFD
% Question 3
% Sans zero-padding
N = 100;
t = (0:N-1) / Fe; 
n = (0:N-1);
f = (-(N-1)/2:(N-1)/2) * Fe/N ;

% fenetre rectangulaire
wr = ones(1, N);
Wr = fft(wr);
% fenetre Hanning
wh = 0.5*(1- cos(2*pi*n/(N-1)));
Wh = fft(wh);
% fenetre Blackman
wb = (27/64)- 0.5*cos((2*pi*n)/(N-1)) + (5/64)*cos((4*pi*n)/(N-1));
Wb = fft(wb);

figure;
subplot(3,1,1); plot(t, wr); title("Fenetre rectangulaire");
subplot(3,1,2); plot(t, wh); title("Fenetre Hanning");
subplot(3,1,3); plot(t, wb); title("Fenetre Blackman");

figure;
subplot(3,1,1); plot(f, fftshift(abs(Wr))); title("Spectre de la fenetre rectangulaire");
subplot(3,1,2); plot(f, fftshift(abs(Wh))); title("Spectre de la fenetre Hanning");
subplot(3,1,3); plot(f, fftshift(abs(Wb))); title("Spectre de la fenetre Blackman");

% Avec zero padding
N = 100;
t = (0:N-1) / Fe;
f = (-(N-1)/2:(N-1)/2) * Fe/N;
n = (0:N-1);

n1 = (0:N*10-1);
f1 = (-(N*10-1)/2:(N*10-1)/2) * Fe/(N*10);

w = zeros(1, N*10);
% fenetre rectangulaire
wr_zp = w;
wr_zp(1:N) = ones(1, N);
Wr_zp = fft(wr_zp);
% fenetre Hanning
wh_zp = w;
wh_zp(1:N) = 0.5 * (1- cos(2 * pi * n/(N-1)));
Wh_zp = fft(wh_zp);
% fenetre Blackman
wb_zp = w;
wb_zp(1:N) = (27/64) - 0.5*cos((2 * pi * n)/(N-1)) + (5/64) * cos((4 * pi * n)/(N-1));
Wb_zp = fft(wb_zp);

figure;
subplot(3,1,1); plot(n1, wr_zp); title("Fenetre rectangulaire avec zero padding");
subplot(3,1,2); plot(n1, wh_zp); title("Fenetre Hanning avec zero padding");
subplot(3,1,3); plot(n1, wb_zp); title("Fenetre Blackman avec zero padding");

figure;
subplot(3,1,1); plot(f1, fftshift(abs(Wr_zp))); title("Spectre de la fenetre rectangulaire avec zero padding");
subplot(3,1,2); plot(f1, fftshift(abs(Wh_zp))); title("Spectre de la fenetre Hanning avec zero padding");
subplot(3,1,3); plot(f1, fftshift(abs(Wb_zp))); title("Spectre de la fenetre Blackman avec zero padding");

% Question 4
% fenetre rectangulaire
Wr_zp_norm = Wr_zp / max(Wr_zp);
% fenetre Hanning
Wh_zp_norm = Wh_zp / max(Wh_zp);
% fenetre Blackman
Wb_zp_norm = Wb_zp / max(Wb_zp);

figure;
subplot(3,1,1); plot(f1, fftshift(abs(Wr_zp_norm))); title("Spectre de la fenetre rectangulaire normalisé avec zero padding");
subplot(3,1,2); plot(f1, fftshift(abs(Wh_zp_norm))); title("Spectre de la fenetre Hanning normalisé avec zero padding");
subplot(3,1,3); plot(f1, fftshift(abs(Wb_zp_norm))); title("Spectre de la fenetre Blackman normalisé avec zero padding");

% Question 5
% Sans zero-padding
Fe = 1000;
N = 100;
t = (0:(N-1))/Fe;
f = (-(N-1)/2:(N-1)/2) * Fe/N;

x1 = cos(2 * pi * 100 * t); % signal sinusoidal de frequence f0=100

% Multiplication par la fenetre
vr_x1 = x1 .* wr;
vh_x1 = x1 .* wh;
vb_x1 = x1 .* wb;

Vr_x1 = fft(vr_x1);
Vh_x1 = fft(vh_x1);
Vb_x1 = fft(vb_x1);

figure;
subplot(3,1,1); plot(t, real(vr_x1)); title("Signal a f=100Hz sans zero-padding fenetre rectangulaire");
subplot(3,1,2); plot(t, real(vh_x1)); title("Signal a f=100Hz sans zero-padding fenetre Hanning");
subplot(3,1,3); plot(t, real(vb_x1)); title("Signal a f=100Hz sans zero-padding fenetre Blackman");

figure;
subplot(3,1,1); plot(f, fftshift(abs(Vr_x1))); title("Spectre du signal a f=100Hz sans zero-padding fenetre rectangulaire");
subplot(3,1,2); plot(f, fftshift(abs(Vh_x1))); title("Spectre du signal a f=100Hz sans zero-padding fenetre Hanning");
subplot(3,1,3); plot(f, fftshift(abs(Vb_x1))); title("Spectre du signal a f=100Hz sans zero-padding fenetre Blackman");

%Signal 95Hz
x2 = cos(2*pi*95*t);

%Multiplication par la fenetre
vr_x2 = x2 .* wr;
vh_x2 = x2 .* wh;
vb_x2 = x2 .* wb;

Vr_x2 = fft(vr_x2);
Vh_x2 = fft(vh_x2);
Vb_x2 = fft(vb_x2);

figure;
subplot(3,1,1); plot(t, real(vr_x2)); title("Signal a f=95Hz fenêtré rectangulaire");
subplot(3,1,2); plot(t, real(vh_x2)); title("Signal a f=95Hz fenêtré Hanning");
subplot(3,1,3); plot(t, real(vb_x2)); title("Signal a f=95Hz fenêtré Blackman");

figure;
subplot(3,1,1); plot(f, fftshift(abs(Vr_x2))); title("Spectre du signal a f=95Hz fenetré rectangulaire");
subplot(3,1,2); plot(f, fftshift(abs(Vh_x2))); title("Spectre du signal a f=95Hz fenetré Hanning");
subplot(3,1,3); plot(f, fftshift(abs(Vb_x2))); title("Spectre du signal a f=95Hz fenetré Blackman");

% Avec zero-padding
N = 100;
t1 = (0:(N*10)-1) / Fe;
f1 = (-((N*10)-1)/2:((N*10)-1)/2) * Fe/N;
n = (0:N-1);

% Signal 100Hz
% Multiplication par la fenetre
vr_zp_x1 = x1_zp .* wr_zp;
vh_zp_x1 = x1_zp .* wh_zp;
vb_zp_x1 = x1_zp .* wb_zp;

Vr_zp_x1 = fft(vr_zp_x1);
Vh_zp_x1 = fft(vh_zp_x1);
Vb_zp_x1 = fft(vb_zp_x1);

figure;
subplot(3,1,1); plot(t1, real(vr_zp_x1)); title("Signal a f=100Hz sans zero-padding fenetre rectangulaire");
subplot(3,1,2); plot(t1, real(vh_zp_x1)); title("Signal a f=100Hz sans zero-padding fenetre Hanning");
subplot(3,1,3); plot(t1, real(vb_zp_x1)); title("Signal a f=100Hz sans zero-padding fenetre Blackman");

figure;
subplot(3,1,1); plot(f1, fftshift(abs(Vr_zp_x1))); title("Spectre du signal a f=100Hz avec zero-padding fenetre rectangulaire");
subplot(3,1,2); plot(f1, fftshift(abs(Vh_zp_x1))); title("Spectre du signal a f=100Hz avec zero-padding fenetre Hanning");
subplot(3,1,3); plot(f1, fftshift(abs(Vb_zp_x1))); title("Spectre du signal a f=100Hz avec zero-padding fenetre Blackman");

%Multiplication par la fenetre
vr_x2_zp = x2_zp .* wr_zp;
vh_x2_zp = x2_zp .* wh_zp;
vb_x2_zp = x2_zp .* wb_zp;

Vr_x2_zp = fft(vr_x2_zp);
Vh_x2_zp = fft(vh_x2_zp);
Vb_x2_zp = fft(vb_x2_zp);

figure;
subplot(3,1,1); plot(t1, real(vr_x2_zp)); title("Signal a f=95Hz fenêtré rectangulaire");
subplot(3,1,2); plot(t1, real(vh_x2_zp)); title("Signal a f=95Hz fenêtré Hanning");
subplot(3,1,3); plot(t1, real(vb_x2_zp)); title("Signal a f=95Hz fenêtré Blackman");

figure;
subplot(3,1,1); plot(f1, fftshift(abs(Vr_x2_zp))); title("Spectre du signal a f=95Hz fenetré rectangulaire");
subplot(3,1,2); plot(f1, fftshift(abs(Vh_x2_zp))); title("Spectre du signal a f=95Hz fenetré Hanning");
subplot(3,1,3); plot(f1, fftshift(abs(Vb_x2_zp))); title("Spectre du signal a f=95Hz fenetré Blackman");

% Question 6
Vr_x1_norm = Vr_x1*2/N;
Vh_x1_norm = Vh_x1*(2/max(Wh));
Vb_x1_norm = Vb_x1*(2/max(Wb));

figure;
subplot(3,1,1); plot(f,fftshift(abs(Vr_x1_norm))); title("spectre Signal fenêtré normalisé f=100Hz, rectangulaire");
subplot(3,1,2); plot(f,fftshift(abs(Vh_x1_norm))); title(" spectre Signal fenêtré normalisé f=100Hz, Hanning");
subplot(3,1,3); plot(f,fftshift(abs(Vb_x1_norm))); title("spectre Signal fenêtré normalisé f=100Hz, Blackman");

Vr_x2_norm=Vr_x2*2/N;
Vh_x2_norm=Vh_x2*(2/max(Wh));
Vb_x2_norm=Vb_x2*(2/max(Wb));

figure;
subplot(3,1,1); plot(f,fftshift(abs(Vr_x2_norm))); title("spectre Signal fenêtré normalisé f=95Hz, rectangulaire");
subplot(3,1,2); plot(f,fftshift(abs(Vh_x2_norm))); title(" spectre Signal fenêtré normalisé f=95Hz, Hanning");
subplot(3,1,3); plot(f,fftshift(abs(Vb_x2_norm))); title("spectre Signal fenêtré normalisé f=95Hz, Blackman");

%% III
N=100;
Fe=1000;
f=(-(N-1)/2: (N-1)/2)*Fe/N ;
n = (0:N-1);

A1=1;
f1=95;
phi1=0;


A2=0.1;
f2=140;
phi2=0;

y= A1*sin(2*pi*n*f1/Fe + phi1) + A2*sin(2*pi*n*f2/Fe + phi2);
Y=fft(y); 

w=zeros(1,N);
%fenetre rectangulaire
wr=w;
wr(1:N)= ones(1,N);
Wr= fft(wr);
Wr_norm= Wr/max(Wr);

%fenetre Hanning
wh=w;
wh(1:N)= 0.5*(1- cos(2*pi*n/(N-1)));
Wh= fft(wh);
Wh_norm= Wh/max(Wh);

%fenetre Blackman
wb=w;
wb(1:N)=(27/64)- 0.5*cos((2*pi*n)/(N-1)) + (5/64)*cos((4*pi*n)/(N-1));
Wb= fft(wb);
Wb_norm= Wb/max(Wb);


yfr= y.*wr ;
yfh= y.*wh ;
yfb= y.*wb ;

Yfr= fft(yfr)*(2/max(Wr));
Yfh= fft(yfh)*(2/max(Wh));
Yfb= fft(yfb)*(2/max(Wb));

figure;
subplot(3,1,1); plot(n,real(yfr)); title(sprintf("Signal A1=1 A2= %d fenetré rectangulaire", A2));
subplot(3,1,2); plot(n,real(yfh)); title(sprintf("Signal A1=1 A2= %d fenetré Hanning",A2));
subplot(3,1,3); plot(n,real(yfb)); title(sprintf("Signal A1=1 A2= %d fenetré Blackman", A2));

figure;
subplot(3,1,1); plot(f,fftshift(abs(Yfr))); title(sprintf("spectre Signal A1=1 A2= %d fenêtré rectangulaire", A2));
subplot(3,1,2); plot(f,fftshift(abs(Yfh))); title(sprintf(" spectre Signal A1=1 A2= %d fenêtré Hanning",A2));
subplot(3,1,3); plot(f,fftshift(abs(Yfb))); title(sprintf("spectre Signal  A1=1 A2= %d fenêtré Blackman", A2));


% Question 3:
N = 100;
Fe = 1000;
f = (-(N-1)/2: (N-1)/2)*Fe/N ;
n = (0:N-1);

A1=1;
f1=95;
phi1=0;


A2=0.3;
f2=140;
phi2=0;
G=0.05;
bruit=  sqrt(G)*randn(1,N);
yb= A1*sin(2*pi*n*f1/Fe + phi1) + A2*sin(2*pi*n*f2/Fe + phi2) + bruit;
Yb=fft(yb); 

w=zeros(1,N);
%fenetre rectangulaire
wr=w;
wr(1:N)= ones(1,N);
Wr= fft(wr);
Wr_norm= Wr/max(Wr);

%fenetre Hanning
wh=w;
wh(1:N)= 0.5*(1- cos(2*pi*n/(N-1)));
Wh= fft(wh);
Wh_norm= Wh/max(Wh);

%fenetre Blackman
wb=w;
wb(1:N)=(27/64)- 0.5*cos((2*pi*n)/(N-1)) + (5/64)*cos((4*pi*n)/(N-1));
Wb= fft(wb);
Wb_norm= Wb/max(Wb);


yfr= yb .* wr ;
yfh= yb .* wh ;
yfb= yb .* wb ;

Yfr= fft(yfr)*(2/max(Wr));
Yfh= fft(yfh)*(2/max(Wr));
Yfb= fft(yfb)*(2/max(Wr));

figure;
subplot(3,1,1); plot(n,real(yfr)); title(sprintf("Signal bruité de sigma= %d  fenetré rectangulaire", G));
subplot(3,1,2); plot(n,real(yfh)); title(sprintf("Signal bruité de sigma= %d  fenetré Hanning",G));
subplot(3,1,3); plot(n,real(yfb)); title(sprintf("Signal bruité de sigma= %d  fenetré Blackman", G));

figure;
subplot(3,1,1); plot(f,fftshift(abs(Yfr))); title(sprintf("spectre Signal bruité de sigma= %d  fenêtré rectangulaire", G));
subplot(3,1,2); plot(f,fftshift(abs(Yfh))); title(sprintf(" spectre Signal bruité de sigma= %d  Hz fenêtré Hanning",G));
subplot(3,1,3); plot(f,fftshift(abs(Yfb))); title(sprintf("spectre Signal  bruité de sigma= %d  fenêtré Blackman", G));

%variance trouvé = 0.05

% Question 4:
N = 100;
Fe = 1000;
f = (-(N-1)/2: (N-1)/2)*Fe/N ;
n = (0:N-1);

A1 = 1;
%f1 = 100;
f1 = 95;
phi1 = 0;

A2 = A1;
f2 = 180;
phi2 = 0;

y = A1*sin(2*pi*n*f1/Fe + phi1); %+ A2*sin(2*pi*n*f2/Fe + phi2);
y(33:66) = 0;
Y = fft(y);

yfr = y .* wr ;
yfh = y .* wh ;
yfb = y .* wb ;

Yfr = fft(yfr);
Yfh = fft(yfh);
Yfb = fft(yfb);

figure;
subplot(3,1,1); plot(n, real(yfr)); title(sprintf("Signal f0=%d Hz fenetré rectangulaire", f1));
subplot(3,1,2); plot(n, real(yfh)); title(sprintf("Signal f0=%d Hz fenetré Hanning",f1));
subplot(3,1,3); plot(n, real(yfb)); title(sprintf("Signal f0=%d Hz fenetré Blackman", f1));

figure;
subplot(3,1,1); plot(f, fftshift(abs(Yfr))); title(sprintf("spectre Signal f0=%d Hz fenêtré rectangulaire", f1));
subplot(3,1,2); plot(f, fftshift(abs(Yfh))); title(sprintf(" spectre Signal f0=%d Hz fenêtré Hanning",f1));
subplot(3,1,3); plot(f, fftshift(abs(Yfb))); title(sprintf("spectre Signal  f0=%d Hz fenêtré Blackman", f1));

%Chirp 

N=100;
Fe=1000;
t= (0:N-1)/Fe; 
f=(-(N-1)/2: (N-1)/2)*Fe/N ;
n = (0:N-1);

a= 100;
b= 1010;


x= sin(2*pi*( a + b*t).*t);

X=fft(x); 


figure;
plot(t,x); title("Signal chirp");
figure;
plot(f,fftshift(abs(X))); title("Spectre Signal chirp"); 

%fenetre rectangulaire
wr= ones(1,N);
Wr= fft(wr);
Wr_norm= Wr/max(Wr);

%fenetre Hanning
wh= 0.5*(1- cos(2*pi*n/(N-1)));
Wh= fft(wh);
Wh_norm= Wh/max(Wh);

%fenetre Blackman
wb=(27/64)- 0.5*cos((2*pi*n)/(N-1)) + (5/64)*cos((4*pi*n)/(N-1));
Wb= fft(wb);
Wb_norm= Wb/max(Wb);

%Multiplication par la fenetre
vr=x.*wr;
vh=x.*wh;
vb=x.*wb;

Vr=fft(vr);
Vh=fft(vh);
Vb=fft(vb);


figure;
subplot(3,1,1); plot(t,real(vr)); title("Signal chirp fenêtré rectangulaire");
subplot(3,1,2); plot(t,real(vh)); title("Signal chirp fenêtré Hanning");
subplot(3,1,3); plot(t,real(vb)); title("Signal chirp fenêtré Blackman");

figure;
subplot(3,1,1); plot(f,fftshift(abs(Vr))); title("spectre Signal chirp fenêtré rectangulaire");
subplot(3,1,2); plot(f,fftshift(abs(Vh))); title(" spectre Signal chirp fenêtré Hanning");
subplot(3,1,3); plot(f,fftshift(abs(Vb))); title("spectre Signal  chirp fenêtré Blackman");

% QUESTION 2 
% APRES AVOIR TRACE LES DIFFERENTES TRANSFORMEES POUR LE SIGNAL ON VOIT
% QU'AVEC LA FENETRE RECTANGULAIRE ON NE VOIT RIEN TANDIS QUE AVEC LES DEUX
% AUTRES FENETREES ON ARRIVE A VOIR QUELQUE CHOSE AVEC CES DEUX FREQUENCES 
