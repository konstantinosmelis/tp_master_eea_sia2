clear all; close all; clc;

% Init
[N, T, Z, u, F, G, H, mX0, PX0, Qw, Rv, X] = simulationDonnees();

X_pred = cell(1, N);
X_est = cell(1, N);
P_pred = cell(1, N);
P_est = cell(1, N);
K = cell(1, N);
Z_est = cell(1, N);
Gamma = cell(1, N);
gamma = zeros(2, N);

X_est{1} = mX0;
P_est{1} = PX0;
Gamma{:, 1} = [0, 0; 0, 0];
Z_est{:, 1} = [0, 0; 0, 0];

% 4
for i = 2:N
    X_pred{i} = F * X_est{i-1} + G * u(:, i-1);
    P_pred{i} = F * P_est{i-1} * F' + Qw;

    K{i} = P_pred{i} * H' * inv(H * P_pred{i} * H' + Rv);
    X_est{i} = X_pred{i} + K{i} * (Z(:, i) - H * X_pred{:, i});
    P_est{i} = P_pred{i} - K{i} * H * P_pred{i};

    Gamma{i} = Rv + H * P_pred{i} * H';
    Z_est{i} = H * X_pred{i};
    gamma(:, i) = Z(:, i) - Z_est{i};
end

x = cell2mat(X_est);

c_sup_X = zeros(1, N);
c_inf_X = zeros(1, N);
for k = 1:N
    c_sup_X(k) = X_est{k}(1) + 3 * sqrt(P_est{k}(1, 1));
    c_inf_X(k) = X_est{k}(1) - 3 * sqrt(P_est{k}(1, 1));
end

figure;
plot(X(1, :)), hold all, plot(x(1, :)), plot(c_inf_X, 'r'), plot(c_sup_X, 'r');
legend("X reel", "X estime", "Intevale de confiance");
title("Estimation de X");

c_sup_Y = zeros(1, N);
c_inf_Y = zeros(1, N);
for k = 1:N
    c_sup_Y(k) = X_est{k}(2) + 3 * sqrt(P_est{k}(2, 2));
    c_inf_Y(k) = X_est{k}(2) - 3 * sqrt(P_est{k}(2, 2));
end

figure;
plot(X(2, :)), hold all, plot(x(2, :)), plot(c_inf_Y, 'r'), plot(c_sup_Y, 'r');
legend("Y reel", "Y estime", "Intevale de confiance");
title("Estimation de Y");

% 5
figure;
for i = 1:N
    mx = x(1:2, i);
    px = P_est{i}(1:2, 1:2);
    ellipse(mx, px, 'b'), hold all;
    plot(X(1, i), X(2, i), "r+");
    plot(x(1, i), x(2, i), "g*");
end
plot(c_inf_X, c_inf_Y, 'r'), plot(c_sup_X, c_sup_Y, 'r');
title("Ellipses de confiance pour Z");
legend("Ellipses de confiance", "Z reel", "Z estime");

% 6
gamma_px = zeros(1, N);
gamma_mx = zeros(1, N);
gamma_py = zeros(1, N);
gamma_my = zeros(1, N);
for i = 1:N
    G = Gamma{:, i};
    gamma_px(i) = 3 * sqrt(G(1, 1));
    gamma_mx(i) = -3 * sqrt(G(1, 1));
    gamma_py(i) = 3 * sqrt(G(2, 2));
    gamma_my(i) = -3 * sqrt(G(2, 2));
end

figure;
plot(gamma(1, :)), hold all, plot(gamma_px, 'r'), plot(gamma_mx, 'r');

figure;
plot(gamma(2, :)), hold all, plot(gamma_py, 'r'), plot(gamma_my, 'r');

z = cell2mat(Z_est);

% figure;
% plot(Z(1, :)), hold on;
% plot(z(1, :));
% legend("Z_1 reel", "Z_1 estime");
% title("Estimation de Z_1");
%
% figure;
% plot(Z(2, :)), hold on;
% plot(z(2, :));
% legend("Z_2 reel", "Z_2 estime");
% title("Estimation de Z_2");

c_sup_z1 = zeros(1, N);
c_inf_z1 = zeros(1, N);
for k = 1:N
    c_sup_z1(1, k) = z(1, k) + 3 * sqrt(Gamma{k}(1,1));
    c_inf_z1(1, k) = z(1, k) - 3 * sqrt(Gamma{k}(1,1));
end

figure;
plot(Z(1, :)), hold all;
plot(z(1, :)), plot(c_sup_z1, 'r'), plot(c_inf_z1, 'r');
legend("Z_1 reel", "Z_1 estime", "Intevale de confiance");
title("Estimation de Z_1");

c_sup_z2 = zeros(1, N);
c_inf_z2 = zeros(1, N);
for k = 1:N
    c_sup_z2(1, k) = z(2, k) + 3 * sqrt(Gamma{k}(2,2));
    c_inf_z2(1, k) = z(2, k) - 3 * sqrt(Gamma{k}(2,2));
end

figure;
plot(Z(2, :)), hold all;
plot(z(2, :)), plot(c_sup_z2, 'r'), plot(c_inf_z2, 'r');
legend("Z_2 reel", "Z_2 estime", "Intevale de confiance");
title("Estimation de Z_2");
