clear all; close all; clc;

load('data.mat');

% Init
N = length(x);
M = length(h);
m = 0:(M-1);

%% Partie 3: Estimation par moindres carrés
%3
sigma2 = 0.3;
y = syst(x, h, sigma2);

R = [toeplitz([0; y(1:(end-1))], [0, 0]), toeplitz(x, [x(1), 0])];
theta = inv(R' * R) * R' * y;
a1 = theta(1);
a2 = theta(2);
b0 = theta(3);
b1 = theta(4);

%4
h_mc = zeros(M, 1);
h_mc(1) = b0;
h_mc(2) = b1 + a1 * b0;

for i = 3:M
    h_mc(i) = a1 * h_mc(i-1) + a2 * h_mc(i-2);
end

%5
figure;
plot(m, h_mc), hold on, plot(m, h);
legend("h_{MC}[n]", "h[n]");

d_mc = norm(h - h_mc) / norm(h);

%6
% Cette estimation n'est pas satisfaisante

%% Partie 4: Methode Steiglitz et McBride
%3
K = 50;
y_t = y;
x_t = x;
h_smc = zeros(M, 1);


d_smc = 3;

for k = 1:K
    R_smc = [toeplitz([0; y_t(1:(end-1))], [0, 0]), toeplitz(x_t, [x_t(1), 0])];
    theta_smc = inv(R_smc' * R_smc) * R_smc' * y_t;

    a1_ = theta_smc(1);
    a2_ = theta_smc(2);
    b0_ = theta_smc(3);
    b1_ = theta_smc(4);

    G = [1, -a1_, -a2_];

    h_smc(1) = b0;
    h_smc(2) = b1 + a1 * b0;

    y_t = filter(1, G, y);
    x_t = filter(1, G, x);

    for j = 3:M
        h_smc(j) = a1_ * h_smc(j-1) + a2_ * h_smc(j-2);
    end

    d_smc = norm(h - h_smc) / norm(h);
end
% 8 iterations

figure;
plot(m, h_smc), hold on, plot(m, h);
legend("h_{SMC}[n]", "h[n]");

%% Parite 5: Biais et variance sur les parametres estimes
%%% 1. Estimation du biais et de la variance d'estimation grace a un
%%% ensemble de realisations

%1
Nr = 400;
thetas_smc = zeros(4, Nr);
H_smc = zeros(M, Nr);
for i = 1:Nr
    y_t = syst(x, h, sigma2);
    x_t = x;
    for k = 1:Nr
        R_smc = [toeplitz([0; y_t(1:(end-1))], [0, 0]), toeplitz(x_t, [x_t(1), 0])];
        thetas_smc(:, i) = inv(R_smc' * R_smc) * R_smc' * y_t;

        a1_ = thetas_smc(1, i);
        a2_ = thetas_smc(2, i);
        b0_ = thetas_smc(3, i);
        b1_ = thetas_smc(4, i);

        H_smc(1) = b0_;
        H_smc(2) = b1_ + a1_ * b0_;

        G = [1, -a1_, -a2_];
        y_t = filter(1, G, y);
        x_t = filter(1, G, x);

        for j = 3:M
            H_smc(j) = a1_ * H_smc(j-1) + a2_ * H_smc(j-2);
        end
    end
end

m_thetas = mean(thetas_smc, 2);
v_thetas = var(thetas_smc, 0, 2);

figure; plot(m_thetas);
figure; plot(v_thetas);

%2
m_h = mean(H_smc, 2);
v_h = var(H_smc, 0, 2);

figure; plot(m_h);
figure; plot(v_h);

% biais
figure;
plot(m_h - h);

%%% 2. Estimation du biais et de la variance d'estimatio par la methode de
%%% Boostrap

%1
K = 7;
y_g = y;
x_g = x;
for k=1:K
    R = [toeplitz([0; y_g(1:(end-1))], [0, 0]) toeplitz(x_g, [x_g(1) 0])];

    theta_b = (inv(R' * R)) * R' * y_g;
    a1 = theta_b(1);
    a2 = theta_b(2);
    b0 = theta_b(3);
    b1 = theta_b(4);

    G = [1, -a1, -a2];
    y_g = filter(1, G, y);
    x_g = filter(1, G, x);
end

y_m = R * theta_b;
epsilon = y - y_m;

figure;
plot(epsilon), title('Erreur du modele');

%2
H_b = zeros(M, Nr);
theta_b = zeros(4, Nr);
for p = 1:Nr
    eps_k = epsilon(randi(250, 1, 250));
    y_g = y + eps_k;
    x_g = x;

    for k = 1:K
        R = [toeplitz([0; y_g(1:(end-1))], [0, 0]) toeplitz(x_g, [x_g(1), 0])];

        theta_b(:, p) = (inv(R' * R)) * R' * y_g;
        a1 = theta_b(1, p);
        a2 = theta_b(2, p);
        b0 = theta_b(3, p);
        b1 = theta_b(4, p);

        G = [1, -a1, -a2];

        y_g = filter(1, G, y_g);
        x_g = filter(1, G, x_g);

        H_b(1,p) = b0;
        H_b(2,p) = (b1 + a1 * b0);
        G = [1, -a1, -a2];

        for j = 3:M
            H_b(j, p) = a1 * H_b(j-1, p) + a2 * H_b(j-2, p);
        end
    end
end

%3
m_theta_b = mean(theta_b, 2);
v_theta_b = var(theta_b, 0, 2);

m_H_b = mean(H_b, 2);
v_H_b = var(H_b, 0, 2);

figure;
subplot(2, 1, 1); plot(m_H_b); title("moyenne h estime");
subplot(2, 1, 2); plot(v_H_b); title("variance h estime");

%FolderName = 'img';
%FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
%for iFig = 1:length(FigList)
%    FigH = FigList(iFig);
%    FigN = ['Fig', num2str(length(FigList) - iFig + 1)];
%    saveas(FigH, fullfile(FolderName, [FigN '.png']));
%end

