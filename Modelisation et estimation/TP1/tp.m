clear all; close all; clc;

%% Init
load data.mat;

M = length(h);
N = length(x);
sigma_2 = 0.3;
n = 0:(M-1);

%% Estimation pour une entree impulsionnelle
delta = [1 zeros(1, length(h)-1)];

h_ri = syst(delta, h, sigma_2);
figure;
plot(n, h_ri), hold on, plot(n, h);
legend("h_{RI}", "h");

dh_hri = norm(h - h_ri) / norm(h);

Nr = 400;
H_RI = zeros(M, Nr);
for i = 1:400
    H_RI(:, i) = syst(delta, h, sigma_2);
end

figure;
plot(n, mean(H_RI, 2)), hold on, plot(n, h);
legend("mean(H_{RI})", "h");

figure;
plot(n, var(H_RI, 0, 2)), hold on, plot(n, ones(1, M) * 0.3);
legend("var(H_{RI})", "0.3");

biais_ri = mean(H_RI, 2) - h;
figure;
plot(n, biais_ri), hold on, plot(n, zeros(1, M));
legend("biais de H_{RI}", "biais theorique");

%% Estimation par moindres carres
y = syst(x, h, sigma_2);
X = toeplitz(x, [x(1) zeros(1, M-1)]);
G = diag(sigma_2 * ones(1, length(x)));
h_mc = inv(X' * inv(G) * X) * (X' * inv(G) * y);

figure;
plot(h_mc), hold on, plot(h);
legend("h_{MC}", "h");

dh_hmc = norm(h - h_mc) / norm(h);

H_MC = zeros(M, Nr);
for i = 1:400
    H_MC(:, i) = inv(X' * inv(G) * X) * (X' * inv(G) * y);
end

figure;
plot(n, mean(H_MC, 2)), hold on, plot(n, h);
legend("mean(H_{MC})", "h");

figure;
plot(n, var(H_MC, 0, 2)), hold on, plot(n, ones(1, M) * 0.3);
legend("var(H_{MC})", "0.3");

biais_mc = mean(H_MC, 2) - h;
figure;
plot(n, biais_mc), hold on, plot(n, zeros(1, M));
legend("biais de H_{MC}", "biais theorique");

%% Estimation par moindres carrés et donnees aberrantes
ya = y;
ya(42) = 15;
ya(69) = 20;
ya(142) = 25;
ya(169) = 30;
ya(242) = 25;

X = toeplitz(x, [x(1) zeros(1, M-1)]);
G = diag(sigma_2 * ones(1, length(x)));
h_mc_a = inv(X' * inv(G) * X) * (X' * inv(G) * ya);

figure;
plot(h_mc_a), hold on, plot(h);
legend("h_{MC}_a", "h");

dh_hmc_a = norm(h - h_mc_a) / norm(h);

b = ya - conv(x, h_mc_a, 'same');
figure, plot(b);

seuil = 5;
ya(b > 5) = 0;

h_mc_a_d = inv(X' * inv(G) * X) * (X' * inv(G) * ya);
figure;
plot(h_mc_a_d), hold on, plot(h);
legend("h_{MC}_{da}", "h");

%% Estimation par moindres carres pour une entree non causale
X = toeplitz(x, x(1:30));
G = diag(sigma_2 * ones(1, length(x)));
h_mc_nc = inv(X' * inv(G) * X) * (X' * inv(G) * y);

figure;
plot(h_mc_nc), hold on, plot(h);
legend("h_{MC_{non causal}}", "h");
