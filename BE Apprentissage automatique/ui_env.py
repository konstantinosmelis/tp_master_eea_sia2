# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 08:48:18 2023

@author: ML
"""
import cv2
import numpy as np
import streamlit as st
from PIL import Image, ImageOps
from tensorflow.keras.models import model_from_json

@st.cache_data  # Pour mettre les données dans la cache

def load_model():
    model_architecture = 'model.json'
    model_weights = 'model.h5'
    model = model_from_json(open(model_architecture).read())
    model.load_weights(model_weights)
    return model


def upload_predict(upload_image, model):
    size = (250, 250)
    image = ImageOps.fit(upload_image, size, Image.ANTIALIAS)
    image = np.asarray(image)
    img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    img_resize = cv2.resize(img, dsize=(130, 130), interpolation=cv2.INTER_CUBIC)
    img_reshape = img_resize[np.newaxis,...]
    prediction = model.predict(img_reshape / 255)
    return prediction[0]


with st.spinner('Model is being loaded..'):
    model = load_model()

st.write("""
         # Image Classification
         """)

st.set_option('deprecation.showfileUploaderEncoding', False)

file = st.file_uploader('Upload the image to a face', type=['jpg', 'png'])
if file is None:
    st.text('Please upload an image file')
else:
    image = Image.open(file)
    pred = upload_predict(image, model)
    image = np.asarray(image)
    n, m, _ = image.shape
    pred[0] *= n
    pred[1] *= n
    pred[2] *= m
    pred[3] *= m
    image = cv2.rectangle(image, (int(pred[0]), int(pred[1])), (int(pred[2]), int(pred[3])), (0, 255, 0), 2)
    image = Image.fromarray(np.uint8(image))
    st.image(image, use_column_width=True)
