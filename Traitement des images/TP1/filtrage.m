clear;

%% Créer une image de carrée

carre = zeros(100,100);
sz = 50;
for i = 0:sz-1
    for j = 0:sz-1
        carre(i+sz/2,j+sz/2) = 255;
    end
end

figure;
imshow(carre);

%% convolution
gradientx = [0 0 0; -1 0 1; 0 0 0];
gradienty = gradientx.';
gradximg = convolution(carre, gradientx);
gradyimg = convolution(carre, gradienty);
figure;
imshow(gradximg);
figure;
imshow(gradyimg);

camera = double(imread("img/cameraman.pgm"));
gradcamx = convolution(camera, gradientx);
gradcamy = convolution(camera, gradienty);
figure;
imagesc(gradcamx); colormap(gray);
figure;
imagesc(gradcamy); colormap(gray);

camnormgrad = round(sqrt(gradcamx.^2 + gradcamy.^2));
figure;
imagesc(camnormgrad); colormap(gray);
camnormgrad = round(sqrt(gradcamx.^2 + gradcamy.^2));
figure;
imagesc(camnormgrad); colormap(gray);

%% 2.2 Robustesse au bruit
bruit = randn(256) .* 20;

figure;
imshow(bruit);

cameramanBruit = camera + bruit;

figure;
imagesc(cameramanBruit); colormap(gray);

gradcambruitx = convolution(cameramanBruit, gradientx);
gradcambruity = convolution(cameramanBruit, gradienty);
figure;
imagesc(gradcambruitx); colormap(gray);
figure;
imagesc(gradcambruity); colormap(gray);

cambruitnormgrad = round(sqrt(gradcambruitx.^2 + gradcambruity.^2));
figure;
imagesc(cambruitnormgrad); colormap(gray);

%% le filtre de Sobel
sobelx = [-1 0 1; -2 0 2; -1 0 1];
sobely = sobelx.';

sobelx = [-1 0 1; -2 0 2; -1 0 1];
sobely = sobelx.';

cameraSobelX = convolution(camera, sobelx);
cameraSobelY = convolution(camera, sobely);

figure;
imagesc(cameraSobelX); colormap(gray);
figure;
imagesc(cameraSobelY); colormap(gray);

camsobelnormgrad = round(sqrt(cameraSobelX.^2 + cameraSobelY.^2));
figure;
imagesc(camsobelnormgrad); colormap(gray);

cameraBruitSobelX = convolution(cameramanBruit, sobelx);
cameraBruitSobelY = convolution(cameramanBruit, sobely);

figure;
imagesc(cameraBruitSobelX); colormap(gray);
figure;
imagesc(cameraBruitSobelY); colormap(gray);

cambruitsobelnormgrad = round(sqrt(cameraBruitSobelX.^2 + cameraBruitSobelY.^2));
figure;
imagesc(cambruitsobelnormgrad); colormap(gray);

%% 2.3 Détection de contours
seuil = 150;
[l c] = size(camnormgrad);
cameraContour = zeros(size(camnormgrad));
cameraContour = zeros(size(cambruitnormgrad));

cameraContour = camnormgrad > seuil;
cameraBruitContour = cambruitnormgrad > seuil;

figure;
imshow(cameraContour);
figure;
imshow(cameraBruitContour);

