//***************************************************
//    M2 SIA  - UE Informatique et Projets
//    TP 5  - Exercice 1   
//
//    Adb
//
//  Occupation memoire des divers types
//
//*************************************************** 

#include <stdio.h>

int main()
{  int A =10 ; 
   int * PTR = &A;
   
   printf("****************************************\n" ) ;
   printf(" TP5  -  exercice 1   \n\n");

   printf("\n Octets occupes : par un int  = %d, par son adresse = %d", sizeof(int), sizeof(int *));
   printf("\n Octets occupes : par un char = %d, par son adresse = %d", sizeof(char), sizeof(char *));
   printf("\n Octets occupes : par un float = %d, par son adresse = %d", sizeof(float), sizeof(float *));
   printf("\n\n                         &A = %p   ----    A  = %d  \n", &A , A);
   printf("\n &PTR = %p  ----  PTR = %p  ----   * PTR = %d  \n", &PTR, PTR, *PTR);

   getchar();
   return 0 ;

}

