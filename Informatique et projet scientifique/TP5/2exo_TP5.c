//***************************************************
//    M2 SIA  - UE Informatique et Projets
//    TP 5  - Exercice 2   
//
//    Adb
//
//    - Acces tableau avec notation pointeur
//    - quelques utilisations de notations pointeurs
//    - ecrasement de donnee par depassement de tableau
//    - depassement de la zone alloue au processus
//
//*************************************************** 


#include <stdio.h>

int main()
 {
  int tab_tmp [4] = {1, 2, 3, 4} ;
  int tab [4] = {100, 200, 300, 400} ;
  int *ptr1, *ptr2 ;   
  int i = 0 ; 
 
ptr1 = tab_tmp ;
/* test de depassement de reservation tableau */
  tab[5] = 600 ;
 printf("\n Contenu de tab[4] est : %d ", tab[4]);
 printf("\n Contenu de tab[5] est : %d \n", tab[5]);
 /* fin du test */

  for(i=0;i<4; i++)
    {   printf("\n adresses : tab[i]= %p", &tab[i]);
    }
  for(i=0;i<4; i++)
    {   printf("\n adresses : tab_tmp[i]= %p", &tab_tmp[i]);
    }



 printf("\n i = %d  --   &i = %p \n", i, &i);

 printf("\n ptr1 = %p  --  *ptr1 = %d  --   &ptr1 = %p ", ptr1, *ptr1, &ptr1);
 ptr2 = &tab[3];
 printf("\n ptr2 = %p  --  *ptr2 = %d  --   &ptr2 = %p ", ptr2, *ptr2, &ptr2);
 ptr1++ ;
 printf("\n ptr1 = %p  --  *ptr1 = %d  --   &ptr1 = %p ", ptr1, *ptr1, &ptr1);
 ++ptr1 ;
 printf("\n ptr1 = %p  --  *ptr1 = %d  --   &ptr1 = %p ", ptr1, *ptr1, &ptr1);
 *ptr2++;
 printf("\n ptr2 = %p  --  *ptr2 = %d  --   &ptr2 = %p ", ptr2, *ptr2, &ptr2);
 (*ptr2)++;
 printf("\n ptr2 = %p  --  *ptr2 = %d  --   &ptr2 = %p \n", ptr2, *ptr2, &ptr2);

 /* Illustration de ecrasement de donnes par debordement du tableau */

  for(i=0;i<4; i++)
    {   printf("\n tab_tmp[i]= %d", tab_tmp[i]);
    }


 /*   Boucle pour montrer le depassement de la zone memoire
 while (1)
   {printf("\n tab[%d] = %d", i, tab[i]);
   i++ ;
   }
 */


 getchar();

 return 0 ;
}

