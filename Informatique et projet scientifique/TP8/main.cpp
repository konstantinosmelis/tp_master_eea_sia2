#include <iostream>
#include "matrix.h"

int main() {
    Matrix I(3, 3), M(3, 3), N(3, 3), O, H, J, L;
    int i, j, k;

    for(i = 0; i < I.getLines(); i++) {
        for(j = 0; j < I.getColumns(); j++)
            if(i == j)
                I.setElement(i, j, 1);
    }

    k = 1;
    for(i = 0; i < M.getLines(); i++) {
        for(j = 0; j < M.getColumns(); j++) {
            M.setElement(i, j, k);
            k++;
        }
    }

    N.setElement(0, 0, 7);
    N.setElement(0, 1, 5);
    N.setElement(0, 2, 0);
    N.setElement(1, 0, 4);
    N.setElement(1, 1, 9);
    N.setElement(1, 2, 3);
    N.setElement(2, 0, 2);
    N.setElement(2, 1, 8);
    N.setElement(2, 2, 6);

    std::cout << "I = " << I << std::endl;
    std::cout << "M = " << M << std::endl;
    std::cout << "N = " << N << std::endl;

    O = M * I;
    H = M * N;
    J = M + N;

    std::cout << "O = " << O << std::endl;
    std::cout << "H = " << H << std::endl;
    std::cout << "J = " << J << std::endl;
    std::cout << "H^t = " << H.transpose() << std::endl;

    L = I.exp();

    std::cout << "L = " << L << std::endl;

    return 0;
}
