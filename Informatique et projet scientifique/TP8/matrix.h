#include <iostream>
#include <vector>

class Matrix {
    private:
        int _lines, _columns;
        std::vector<std::vector<float>> _data;

    public:
        Matrix();
        Matrix(const int, const int);

        int getLines() const;
        int getColumns() const;
        std::vector<std::vector<float>> getData() const;
        float getElement(int, int) const;
        
        std::vector<float> getLine(const int) const;
        std::vector<float> getColumn(const int) const;
        void setElement(const int, const int, const float);

        Matrix transpose();
        Matrix pow(const int);
        Matrix exp();

        Matrix operator+(const Matrix &);
        Matrix operator*(const Matrix &);
        Matrix operator=(const Matrix &);

        ~Matrix();
};

std::ostream &operator<<(std::ostream &, const Matrix &);
