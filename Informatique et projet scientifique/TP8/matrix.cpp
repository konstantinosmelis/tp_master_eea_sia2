#include "matrix.h"
#include <iostream>
#include <ostream>
#include <vector>

Matrix::Matrix(const int lines, const int columns) {
    int i;
    this->_lines = lines;
    this->_columns = columns;
    for(i = 0; i < lines; i++)
        this->_data.push_back(std::vector<float>(columns, 0));
}

Matrix::Matrix() {
    Matrix(3, 3);
}

int Matrix::getLines() const {
    return this->_lines;
}

int Matrix::getColumns() const {
    return this->_columns;
}

float Matrix::getElement(int line, int column) const {
    return this->_data[line][column];
}

std::vector<float> Matrix::getLine(const int line) const {
    return this->_data.at(line);
}

std::vector<float> Matrix::getColumn(const int column) const {
    int i;
    std::vector<float> col;
    for(i = 0; i < this->_lines; i++)
        col.push_back(this->_data.at(i)[column]);
    return col;
}

void Matrix::setElement(const int line, const int column, const float value) {
    this->_data.at(line)[column] = value;
}

Matrix Matrix::transpose() {
    int i, j;
    Matrix trans = Matrix(this->_columns, this->_lines);
    for(i = 0; i < this->_columns; i++)
        for(j = 0; j < this->_lines; j++)
            trans.setElement(i, j, this->_data[j][i]);
    return trans;
}

Matrix Matrix::pow(int power) {
    int i, j;
    Matrix pow = Matrix(this->_lines, this->_columns);
    // Initialise to 1
    for(i = 0; i < this->_lines; i++)
        for(j = 0; j < this->_columns; j++)
            pow.setElement(i, j, 1);

    for(i = 0; i < power; i++)
        pow = pow * (*this);
    return pow;
}

Matrix Matrix::exp() {
    int i, j, k, fact = 1;
    Matrix exp = Matrix(this->_lines, this->_columns) + (*this);
    for(k = 1; k < 10; k++) {
        fact *= k;
        exp = exp + exp.pow(k);
        for(i = 0; i < this->_lines; i++)
            for(j = 0; j < this->_columns; j++)
                exp.setElement(i, j, exp.getElement(i, j) / k);
    }
    return exp;
}

Matrix Matrix::operator+(const Matrix &matrix) {
    int i, j;
    Matrix somme = Matrix(this->_lines, this->_columns);
    if(this->_lines != matrix.getLines() || this->_columns != matrix.getColumns())
        return somme;
    for(i = 0; i < this->_lines; i++)
        for(j = 0; j < this->_columns; j++)
            somme.setElement(i, j, this->_data[i][j] + matrix.getElement(i, j));
    return somme;
}

Matrix Matrix::operator*(const Matrix &matrix) {
    int i, j, k;
    float val;
    Matrix prod = Matrix(this->_lines, matrix.getColumns());
    if(this->_columns != matrix.getLines())
        return prod;
    for(i = 0; i < this->_lines; i++) {
        for(j = 0; j < matrix.getColumns(); j++) {
            val = 0;
            for(k = 0; k < this->_columns; k++) {
                val += this->_data[i][k] * matrix.getElement(k, j);
            }
            prod.setElement(i, j, (val > 100 ? 100 : val));
        }
    }
    return prod;
}

Matrix Matrix::operator=(const Matrix &matrix) {
    int i, j;
    this->_lines = matrix.getLines();
    this->_columns = matrix.getColumns();
    this->_data = std::vector<std::vector<float>>(this->_lines, std::vector<float>(this->_columns, 0));
    for(i = 0; i < matrix.getLines(); i++)
        for(j = 0; j < matrix.getColumns(); j++)
            this->setElement(i, j, matrix.getElement(i, j));
    return *this;
}

Matrix::~Matrix() {

}

std::ostream &operator<<(std::ostream &stream, const Matrix &matrix) {
    int i, j;
    stream << "[";
    for(i = 0; i < matrix.getLines(); i++) {
        for(j = 0; j < matrix.getColumns(); j++)
            stream << matrix.getElement(i, j) << " ";
        stream << std::endl;
    }
    stream << "]";
    return stream;
}
