function [valeur, indice_ligne, indice_colonne] = min2D(M1, M2)
    if nargin == 1
        if nargout == 2
            [valeur, indice_l] = min(M1);
            [valeur, indice_c] = min(valeur);
            indice_ligne = [indice_l(indice_c), indice_c];
        elseif nargout == 3
            [valeur, indice_ligne] = min(M1);
            [valeur, indice_colonne] = min(valeur);
            indice_ligne = indice_ligne(indice_colonne);
        end 
    elseif nargin == 2
        valeur = min(M1, M2);
    end
end
