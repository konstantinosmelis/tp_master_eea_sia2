close all, clear, clc;

%% Manipulation des scalaires, vecteurs et matrices
% Scalaires
z = exp(-1j * pi) + 1;

% Vecteurs
u = (-10:10);
v = u';
w = u .^ 2;
x = 10 .^ u;
u_sup = sum(u > 3);
v_sup_inf = sum(v > -2 & v < 5);
M = repmat(v, 1, 5);

% Matrices
A = reshape(1:12, [4, 3])';
p = 9;
B = reshape(mod(1:(p*p), 2), p, p);

%% Affichage graphique et alphanumerique
% 1D
f = 1000;
Fe = 22400;
Te = 1 / Fe;
t = 0:Te:(1 - Te);
x = cos(2 * pi * f * t);

figure, plot(t, x);
title("Signal sinusoidal pour f=1000Hz");
xlabel("t"), ylabel("x(t)");

sound(x, Fe);

S = 5;
y = x(1:S:end);
figure, stem(t(1:S:end), y);
title(sprintf("Signal sinusoidal avec S=%d", S));
xlabel("t"), ylabel("y(t)");

N = 200;
X = rand(N, 1) - .5;
Y = sqrt(1/2) * randn(N, 1) + 1;
figure;
subplot(1, 2, 1);
hist(X, -0.5:0.1:0.5);
title("Histogramme de la variable X");
subplot(1, 2, 2);
hist(Y, -0.5:0.1:3);
title("Histogramme de la variable Y");

% 2D
x = -2:0.01:2;
y = -2:0.01:2;
p = 100;
f = zeros(size(x, 2), size(y, 2));
for k=1:size(x, 2)
   f(k, :) = (x(k) - 1).^2 + p * (x(k)^2 - y).^2;
end
figure, contour(x, y, f', 1:10);

%% Programmation et utilisation de fonctions
% Sur les boucles
t = 1:.1:10^5;
tmp = cputime;
for k = 1:size(t, 2)
    v1(k) = log(t(k));
end
fprintf("Temps avec boucle, sans initialisation: %f\n", cputime - tmp);

tmp = cputime;
v2 = zeros(1, size(t, 2));
for k = 1:size(t, 2)
    v2(k) = log(t(k));
end
fprintf("Temps avec boucle, avec initialisation: %f\n", cputime - tmp);

tmp = cputime;
v3 = log(t);
fprintf("Temps sans boucle: %f\n", cputime - tmp);

% Fonctions
A = [12 11 15; 4 1 6; 8 3 4];
B = [4 6 16; 5 3 7; 2 -2 9];
M = min2D(A, B);

%% Signal sonore
[Y, Fe] = audioread('Notes_Piano.wav');

