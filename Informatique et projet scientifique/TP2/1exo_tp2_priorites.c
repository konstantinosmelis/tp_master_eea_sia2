       /************************************************/           
       /*         M2 SIA  - UE Informatique et Projets */ 
       /*   		                                 */
       /*   	    TP2 - Exo1                        */
       /***********************************************/           


/*   Programme pour tester les expressions "papier" de l'exercice 1 
     Pour verifier les resultats, il faut ... les afficher
     ATTENTION : ne pas introduire d'erreurs par le biais de 
                 cette affichage ...
     En effet, selon comment et quand l'affichage est fait, 
     certains "expressions" peuvent etre calculees plusieurs fois ..
     Plusieurs solutions sont proposees ici.

     Ce corrige montre aussi comment "cadrer" l'affichage.
*/

#include <stdio.h>

int x , y , z;
void main() {
   x = -3 + 4 * 5 - 6 ;    // ligne 2
   printf ("\n (2) x=%5d   y=%5d  z=%5d  expression = %d ", x, y,z, x = -3 + 4 * 5 - 6);
   // Remarque 1 : l'affichage fait executer une 2d fois x = -3 + 4 * 5 - 6
   //              --> ici, vu l'expression, c'est juste une perte de temps

   x = 3 + 4 % 5 - 6 ;     // ligne 3
   printf ("\n (3) x=%5d   y=%5d  z=%5d  expression = %d ", x, y,z, x = 3 + 4 % 5 - 6);

   x = -3 * 4 % 6+5 ;      // ligne 4
   printf ("\n (4) x=%5d   y=%5d  z=%5d  expression = %d ", x, y,z, x = -3 * 4 % 6+5);

   //   x *=  3 + 5  ;          // ligne 5
   //   printf ("\n (5) x=%5d   y=%5d  z=%5d  expression = %d ", x, y,z, x *=  3 + 5 );
   // Remarque 2 : la 2d execution de  l'expression  x *=  3 + 5 dans l'affichage
   //              pose un pb supplementaire (plus seulement une perte de temps) car
   //              cette fois le calcul de x fait intervenir la valeur de x de 
   //              l'etape precedente --> la valeur de x affichee n'est plus
   //              celle de la solution papier
   // --> il faut changer le "principe" d'affichage.

   printf ("\n (5) expression = %5d  x=%5d   y=%5d  z=%5d  ", x *=  3 + 5 , x, y,z);

   printf ("\n (6) expression = %5d  x=%5d   y=%5d  z=%5d  ", x *= (y = (z = 4)) , x, y,z);

   printf ("\n (7) expression = %5d  x=%5d   y=%5d  z=%5d  ", x =  y ==z , x, y,z);

   printf ("\n (8) expression = %5d  x=%5d   y=%5d  z=%5d  ", x == ( y=z) , x, y,z);

   x = 0; y = -1; z = 0;
   printf ("\n (9) x=%5d   y=%5d  z=%5d  ", x, y,z);

   printf ("\n(10) expression = %5d  x=%5d   y=%5d  z=%5d  ",x = x && y || z , x, y,z);

   printf ("\n(11) expression = %5d  x=%5d   y=%5d  z=%5d  ", z =  x++  - 1 , x, y,z);

   printf ("\n(12) expression = %5d  x=%5d   y=%5d  z=%5d  ", z +=  -x++ + ++y , x, y,z);
}
