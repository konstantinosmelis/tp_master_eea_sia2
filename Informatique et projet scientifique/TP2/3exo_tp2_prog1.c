       /**************************************************/           
       /*    	  M2 SIA  - UE Informatique et Projets  */ 
       /*   		                                   */
       /*   	  TP2 - Exo3 : programme 1              */
       /**************************************************/           


#include <stdio.h>
main () {
    int i, j , k ;
    i = 0 ; k = (i++);
    printf("\n  i = %d    k = %d  \n", i, k);
    i = 10 ; k = (++i);
    printf("\n  i = %d    k = %d  \n", i , k);
    i = 20; j = 5;  k = ((i++) * (++j));
    printf("\n  i = %d    j= %d     k = %d  \n ", i , j,  k);
    i = 15;  k = (i += 3);
    printf("\n  i = %d     k = %d  \n ", i , k);
}
