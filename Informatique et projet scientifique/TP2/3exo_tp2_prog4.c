       /**************************************************/           
       /*    	  M2 SIA  - UE Informatique et Projets   */ 
       /*   		                                   */
       /*   	    TP2 - Exo3 : programme 4             */
       /**************************************************/           


#include <stdio.h>
main () {
    int n = 5, p = 2, q;

    q = n++ < p || p++ !=3;
    printf("\n   n= %d    p= %d    q= %d    \n", n, p ,q);

    q = n++ == 3 && ++p == 3;
    printf("\n   n= %d    p= %d    q= %d    \n", n, p, q);
}
