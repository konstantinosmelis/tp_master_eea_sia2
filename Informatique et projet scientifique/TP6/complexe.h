#pragma once

#include <iostream>

class Complexe {

private:
    float _re;
    float _im;

public:
    // constructeurs
    Complexe();  // constructeur vide
    Complexe(float, float);   // constructeur à 2 arguments 
    Complexe(const Complexe &);

    void set(const Complexe c);
    void set(const float, const float);

    // getters
	float getRe() const;
	float getIm() const;

    // setters
	void setRe(const float);
	void setIm(const float);

    // autres méthodes
	void afficheSur(std::ostream &) const;
    Complexe plus(const Complexe);
    Complexe fois(const Complexe);

    // operateurs
    Complexe operator+(const Complexe);
    Complexe operator*(const Complexe);
};

std::ostream & operator<<(std::ostream &, const Complexe);
