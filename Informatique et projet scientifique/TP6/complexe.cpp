#include "complexe.h"
#include <math.h>
#include <iostream>
#include <ostream>

using namespace std;

Complexe::Complexe() {}  // constructeur vide

Complexe::Complexe(float re, float im) {  // constructeur à 2 arguments
    this->_re = re;
    this->_im = im;
}

Complexe::Complexe(const Complexe &c) {
    this->_re = c.getRe();
    this->_im = c.getIm();
}

void Complexe::set(const Complexe c) {
    this->_re = c.getRe();
    this->_im = c.getIm();
}

void Complexe::set(const float re, const float im) {
    this->_re = re;
    this->_im = im;
}

float Complexe::getIm() const {
	return this->_im;
}

float Complexe::getRe() const {
	return this->_re;
}

void Complexe::setRe(const float re) {
	this->_re = re;
}

void Complexe::setIm(const float im) {
	this->_im = im;
}

void Complexe::afficheSur(std::ostream &flot) const {
    flot << this->_re << (this->_im >= 0 ? "+" : "") << this->_im << "i" << endl;
}

Complexe Complexe::plus(const Complexe c) {
    return Complexe(this->_re + c.getRe(), this->_im + c.getIm());
}

Complexe Complexe::fois(const Complexe c) {
    return Complexe((this->_re * c.getRe()) - (this->_im * c.getIm()), (this->_re * c.getIm())+ (this->_im * c.getRe()));
}

Complexe Complexe::operator+(const Complexe c) {
    return this->plus(c);
}

Complexe Complexe::operator*(const Complexe c) {
    return this->fois(c);
}

ostream &operator<<(ostream &flot, const Complexe c) {
    return flot << c.getRe() << (c.getIm() >= 0 ? "+" : "") << c.getIm() << "i";
}
