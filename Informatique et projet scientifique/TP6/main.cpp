#include <iostream>
#include "complexe.h"

using namespace std;

int main() {
    // Q1
    cout << "Q1 ---" << endl;
    Complexe c1(1, 1), c2(2.5, -2), c3(1, 0);

    cout << "Partie reelle de c1 = ";
    cout << c1.getRe() << endl;
    cout << "Partie imaginaire de c1 = ";
    cout << c1.getIm() << endl;

    c3.afficheSur(cout);
    c3 = c2;  // equivaut a c3.operator=(c2);
    c3.afficheSur(cout);   

    // Q2
    cout << "Q2 ---" << endl;
    Complexe c;

    c = c1.plus(c2);
    c.afficheSur(cout);
    c = c1.fois(c2);
    c.afficheSur(cout);

    c = c1 + c2;  // c1 + c2 equivaut a c1.operator+(c2)
    c.afficheSur(cout);
    c = c1 * c2;  // c1 * c2 equivaut a c1.operator*(c2)
    c.afficheSur(cout);

    // Q3
    cout << "Q3 ---" << endl;
    c1.setRe(2);
    c1.setIm(3);
    cout << c1 << endl;  // cout << c1 equivaut a operator<<(cout, c1);

    // Q4
    cout << "Q4 ---" << endl;
    Complexe c4(c1);

    cout << c1 << endl << c4 << endl;

    c4.set(3, 5);
    c1.set(c4);

    cout << c1 << endl << c4 << endl;

    return 0;
}
