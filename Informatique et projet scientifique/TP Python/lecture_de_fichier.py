import matplotlib.pyplot as plt
import csv

year = []
val = []
with open('co2_annmean_mlo.csv') as f:
    reader = csv.reader(f, delimiter=',')
    for r in reader:
        if r[0][0] == "#" or r[0] == "year":
            continue
        year.append(r[0])
        val.append(r[1])

plt.figure()
plt.plot(year, val, '-')
plt.xlabel("Annee")
plt.ylabel("Moyenne")
plt.title("Evolution du taux de CO2 mesure au Mauna Loa au cours de annees")
plt.show()
