import numpy as np
import matplotlib.pyplot as plt
import pickle

data = []
with open("16nov18_norm_hd22049_004.s", "r") as f:
    f.readline()
    f.readline()
    for line in f:
        data.append(np.array(line.split(), dtype=float))
f.close()

data = np.array(data)

plt.figure()
plt.plot(data[:, 0], data[:, 1])
plt.vlines([849.802, 854.209, 866.214], 0, ymax=1, colors='red')
plt.xlabel("Longueur d'onde (nm)")
plt.title("Spectre de Epsilon Eri")
plt.show()

idx = np.where((data[:, 0] >= 848.0) & (data[:, 0] < 868.0))
idxMin = np.min(idx)
idxMax = np.max(idx)
del idx

with open("data.pickle", "wb") as f:
    pickle.dump(data[idxMin:idxMax, :], f)
f.close()

with open("data.pickle", "rb") as f:
    loaded_data = pickle.load(f)
f.close()

plt.figure()
plt.plot(loaded_data[:, 0], loaded_data[:, 1])
plt.xlabel("Longueur d'onde (nm)")
plt.title("Spectre de Epsilon Eri")
plt.show()

data = []
with open("solar_spectre.txt", "r") as f:
    for line in f:
        data.append(np.array(line.split(), dtype=float))
data = np.array(data)
f.close()

plt.figure()
plt.plot(loaded_data[:, 0], 6000 * loaded_data[:, 1])
plt.plot(data[:, 0] / 10, data[:, 1])
plt.xlabel("Longueur d'onde (nm)")
plt.title("Spectre solaire et spectre de Epsilon Eri")
plt.show()

lobs = 866.264
lref = 866.212

V = 299792458 * (lobs - lref) / lobs
print(f"Vitesse radiale: {V} m/s")