import math

N = 2
coef = []
for i in range(0, N + 1):
    t = float(input(f"Entrer la {i + 1}eme valeur: "))
    coef.append(t)

delta = coef[1]**2 - (4 * coef[0] * coef[2])
if delta == 0:
    print(f"La solution unique est {-coef[1] / (2 * coef[0])}")
elif delta > 0:
    print(f"La premiere solution reelle est {(-coef[1] + math.sqrt(delta)) / (2 * coef[0])}")
    print(f"La deuxieme solution est {(-coef[1] - math.sqrt(delta)) / (2 * coef[0])}")
else:
    print(f"La premiere solution reelle est {-coef[1] / (2 * coef[0])}+i{math.sqrt(-delta) / (2 * coef[0])}")
    print(f"La premiere solution reelle est {-coef[1] / (2 * coef[0])}-i{math.sqrt(-delta) / (2 * coef[0])}")
