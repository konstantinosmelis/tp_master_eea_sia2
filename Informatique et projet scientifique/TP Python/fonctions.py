import math

def delta(coef):
    return coef[1]**2 - (4 * coef[0] * coef[2])

def MinMax(mat):
    mi = mat[0][0]
    ma = mat[0][0]
    for i in range(0, len(mat)):
        for j in range(0, len(mat[i])):
            mi = min(mi, mat[i][j])
            ma = max(ma, mat[i][j])
    return (mi, ma)

N = 2
coef = []
for i in range(0, N + 1):
    t = float(input(f"Entrer la {i + 1}eme valeur: "))
    coef.append(t)

d = delta(coef)
if d == 0:
    print(f"La solution unique est {-coef[1] / (2 * coef[0])}")
elif d > 0:
    print(f"La premiere solution reelle est {(-coef[1] + math.sqrt(d)) / (2 * coef[0])}")
    print(f"La deuxieme solution est {(-coef[1] - math.sqrt(d)) / (2 * coef[0])}")
else:
    print(f"La premiere solution reelle est {-coef[1] / (2 * coef[0])}+i{math.sqrt(-d) / (2 * coef[0])}")
    print(f"La premiere solution reelle est {-coef[1] / (2 * coef[0])}-i{math.sqrt(-d) / (2 * coef[0])}")

mat = [[2, 7, 6], [1, 8, 3], [7, 3, 6]]
print(MinMax(mat))
