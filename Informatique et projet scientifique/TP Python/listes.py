N = int(input("Entrer un entier N: "))
P = int(input("Entrer un entier P: "))

T = []
for i in range(min(N, P), max(N, P)):
    if i % 3 == 0:
        T.append(i)

print(f"Les nombres divisibles par 3 dans l'intervalle [{min(N, P)};{max(N, P)}] sont {T}")