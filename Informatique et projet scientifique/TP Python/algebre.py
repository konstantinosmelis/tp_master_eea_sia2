import numpy as np

mat1 = np.matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
mat2 = np.matrix([[9, 8, 7], [6, 5, 4], [3, 2, 1]])

print("mat1:", mat1)
print("mat2:", mat2)

print("det(mat1):", np.linalg.det(mat1))
print("det(mat2):", np.linalg.det(mat2))

print(f"Le produit des deux matrice est:\n{np.dot(mat1, mat2)}")
print(f"Le produit des deux matrice est:\n{np.dot(mat1, mat2)}")


