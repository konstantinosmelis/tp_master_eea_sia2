import numpy as np
import matplotlib.pyplot as plt

Fe = 2048
t = np.arange(1000) / Fe
x = np.sin(2 * np.pi * 1000 * t)

plt.figure()
plt.plot(t, x, '-')
plt.xlabel("t (en s)")
plt.ylabel("x(t)")
plt.title("Signal x(t)")
plt.show()

x = np.sin(3 * t)
y = np.sin(4 * t)

plt.figure()
plt.plot(x, y, '-')
plt.xlabel("sin(3t)")
plt.ylabel("sin(4t)")
plt.title("Figure de Lissajous")
plt.show()
