#include <stdio.h>

int main() {
    int N, i, imin = 0, ival = -1, nbval = 0;
    float vals[100];
    float somme = 0, min, val;

    printf("Entrer le nombre de valeurs a saisir: ");
    scanf("%d", &N);

    printf("Entrer des valeurs:\n");
    for(i = 0; i < N; i++)
        scanf("%f", &vals[i]);

    min = vals[0];
    printf("Les valeurs ecrites sont: [");
    for(i = 0; i < N; i++) {
        printf("%.2f ", vals[i]);
        somme += vals[i];
        if(min > vals[i]) {
            min = vals[i];
            imin = i;
        }
    }
    printf("]\n");
    printf("Somme=%.2f, moyenne=%.2f, Min=%.2f, iMin=%d\n", somme, somme / N, min, imin);

    printf("Entrer une valeur a trouver dans le tableau: ");
    scanf("%f", &val);
    for(i = 0; i < N; i++) {
        if(val == vals[i]) {
            ival = i;
            break;
        }
    }
    for(i = 0; i < N; i++) {
        if(val == vals[i]) {
            nbval += 1;
        }
    }
    printf("Premiere occurence de cette valeur=%d, nombre d'occurences=%d\n", ival, nbval);

    return 0;
}
