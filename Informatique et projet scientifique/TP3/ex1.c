#include <stdio.h> 

int main() {
    int N, i = 0, somme = 0;

    printf("Entrer un entier N: ");
    scanf("%d", &N);

    while(N >= 0) {
        if(i % 2 == 0) {
            somme += i;
            N--;
        }
        i++;
    }

    printf("La somme des N nombre pairs est: %d\n", somme);

    return 0;
}
