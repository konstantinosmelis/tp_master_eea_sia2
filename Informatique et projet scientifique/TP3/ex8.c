#include <stdio.h>

int main() {
    int N = 0, i = 0;
    float temp, min = 0, max = 0, somme = 0;

    while(N <= 0) {
        printf("Entrer le nombre de valeurs: ");
        scanf("%d", &N);
    }

    printf("Entrer les temperatures une par une: ");
    scanf("%f", &temp);
    min = temp;
    max = temp;
    somme += temp;
    while(i < (N - 1)) {
        scanf("%f", &temp);
        min = (min > temp ? temp : min);
        max = (max < temp ? temp : max);
        somme += temp;
        i++;
    }
    printf("Temperature maximale=%.2f, minimale=%.2f, moyenne=%.2f\n", max, min, somme / N);

    return 0;
}
