#include <stdio.h>

int main() {
    char c, prevc = NULL;
    int mot = 0, longueur = 0, lgmax = 0;

    printf("Ecrire une phrase: ");
    // gets(phrase);

    // longueur = 0;
    // while((c = phrase[i]) != '\0') {
    //     longueur++;
    //     if((c == ' ' || c == '.') && prevc != ' ') {
    //         mot++;
    //         lgmax = (lgmax > longueur ? lgmax : longueur - 1);
    //         longueur = 0;
    //     }
    //     prevc = c;
    //     i++;
    // }

    while((c = getchar()) != EOF) {
        longueur++;
        if((c == ' ' || c == '.') && prevc != ' ') {
            mot++;
            lgmax = (lgmax > longueur ? lgmax : longueur - 1);
            longueur = 0;
        }
        prevc = c;
    }

    printf("Cette phrase contient %d mots\n", mot);
    printf("La longueur du mot du plus long est %d\n", lgmax);

    return 0;
}

