#include <stdio.h>

int main() {
    int N, Nenver = 0, i = 1, tmp;

    printf("Choisir un entier N a inverser: ");
    scanf("%d", &N);

    tmp = N;
    while(tmp / 10.0 > 1) {
        i *= 10;
        tmp /= 10;
    }

    while(N / 10.0 >= 1) {
        Nenver += (N % 10) * i;
        i /= 10;
        N /= 10;
    }
    Nenver += (N % 10) * i;
    printf("N a l'enver: %d\n", Nenver);

    return 0;
}
