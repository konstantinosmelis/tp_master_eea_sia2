#pragma once

#include <iostream>

class Rationnel {

private:
    int _num;
    int _den;

public:
	Rationnel();
	Rationnel(int);
	Rationnel(int, int);
	Rationnel(const Rationnel &);
		
	int getNumerateur() const;
	int getDenominateur() const;
    
    void setNumerateur(const int);
    void setDenominateur(const int);
    
    void canonise();
    
    Rationnel &operator=(const Rationnel &);
    
    Rationnel operator+(const Rationnel &) const;
	Rationnel operator-(const Rationnel &) const;
	Rationnel operator*(const Rationnel &) const;
	Rationnel operator/(const Rationnel &) const;

	bool operator==(const Rationnel &) const;
	bool operator>(const Rationnel &) const;
	bool operator>=(const Rationnel &) const;
	bool operator<(const Rationnel &) const;
	bool operator<=(const Rationnel &) const;

};

std::ostream & operator<<(std::ostream &, const Rationnel &);
