#include "monome.h"
#include "rationnel.h"

using namespace std;

Monome::Monome() {
    this->_coef = Rationnel(1, 1);
    this->_exp = 0;
}

Monome::Monome(Rationnel r, int exp) {
    this->_coef = r;
    this->_exp = exp;
}

Monome::Monome(const Monome &m) {
    this->_coef = m.getCoef();
    this->_exp = m.getExp();
}

Rationnel Monome::getCoef() const {
    return this->_coef;
}

int Monome::getExp() const {
    return this->_exp;
}

void Monome::setCoef(const Rationnel &coef) {
    this->_coef = coef;
}

void Monome::setExp(const int exp) {
    this->_exp = exp;
}

Monome Monome::operator+(const Monome &m) {
    return Monome(this->_coef + m.getCoef(), this->_exp);
}

Monome Monome::operator-(const Monome &m) {
    return Monome(this->_coef - m.getCoef(), this->_exp);
}

Monome Monome::operator*(const Monome &m) {
    return Monome(this->_coef * m.getCoef(), this->_exp + m.getExp());
}

Monome Monome::operator/(const Monome &m) {
    return Monome(this->_coef / m.getCoef(), this->_exp - m.getExp());
}

Monome &Monome::operator=(const Monome &m) {
    if(this != &m) {
        this->_coef = m.getCoef();
        this->_exp = m.getExp();
    }
    return *this;
}

ostream &operator<<(ostream &flot, const Monome &m) {
    flot << m.getCoef();
    if(m.getExp() != 0)
        flot << "x^" << m.getExp();
    return flot;
}
