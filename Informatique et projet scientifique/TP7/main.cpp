#include <iostream>
#include "rationnel.h"
#include "monome.h"
#include "polynome.h"

using namespace std;

int main() {
    // Premier test de la classe Rationnel
    cout << "Premier tets ----" << endl;
    Rationnel r1(4, 6);
    cout << r1 << endl;

    // Q1
    cout << "Q1 ----" << endl;
    Monome M1(Rationnel(1, 2), 3);
    Monome M2(Rationnel(2, 3), 1);

    cout << M1 << endl;
    cout << M2 << endl;

    Monome  M3;
    cout << M3 << endl;

    M3 = M1;
    cout << M3 << endl;

    cout << (M1 * M2) << endl;

    // Q2
    cout << "Q2 ----" << endl;

    Polynome P1(M1), P2(M2), P3;

    cout << "P1 = " << P1 <<endl;
    cout << "P2 = " << P2 << endl;

    cout << "Somme / Difference avec Monome" << endl;
    P1 = P1 + Monome(Rationnel(2, 3), 3);
    P2 = P2 + Monome(Rationnel(3, 5), 6);
    P3 = P3 - Monome(Rationnel(-3, 7), 2);

    cout << "P1 = M1 + 2/3x^3 = " << P1 << endl;
    cout << "P2 = M2 + 3/5x^6 = " << P2 << endl;
    cout << "P3 = 0 - -3/7x^2 = " << P3 << endl;

    cout << "Produit / Division avec Monome" << endl;
    P1 = P1 * Monome(3,5);
    P2 = P2 / Monome(3,5);

    cout << "P1 = P1 * 3x^5 = " << P1 << endl;
    cout << "P2 = P2 / 3x^5 = " << P2 << endl;

    cout << "Calculs entre Polynomes" << endl;
    cout << "P1 = " << P1 << endl;
    cout << "P2 = " << P2 << endl;
    cout << "P3 = " << P3 << endl;

    P2 = (P1 + P2) * P3;
    cout << "P2 = (P1 + P2) * P3 = " << P2 << endl;

    return 0;
}
