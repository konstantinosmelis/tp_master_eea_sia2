#include "rationnel.h"

// --------------------------------------------------------
// Constructeurs
// --------------------------------------------------------

// Constructeur vide : 0/1
Rationnel::Rationnel() {
	_num = 0;
	_den = 1;
}

// Constructeur à 1 argument : N/1
Rationnel::Rationnel(int N) {
	_num = N;
	_den = 1;
}

// Constructeur à 2 arguments : N/D
Rationnel::Rationnel(int N, int D) {
	// si le rationnel negatif, on met le signe au numerateur
	if(D < 0) {
		_den = D*(-1);
		_num = N*(-1);
	}
	else {
		_den = D;
		_num = N;
	}
	canonise();
}

// Rend le rationnel sous forme canonique
// exemple : 1/3 a lieu de 2/6
void Rationnel::canonise() {
    int a = abs(_num);
    int b = abs(_den);
    int r;

    while(b > 0) {
        r = a % b;
        a = b;
        b = r;
    }
    if(a != 1) {
        _num = _num / a;
        _den = _den / a;
    }
}

Rationnel::Rationnel(const Rationnel &r) {
    _num = r.getNumerateur();
    _den = r.getDenominateur();
}

// --------------------------------------------------------
// getters et setters
// --------------------------------------------------------
int Rationnel::getNumerateur() const {
	return _num;
}

int Rationnel::getDenominateur() const {
	return _den;
}

void Rationnel::setNumerateur(const int num) {
    _num = num;
}

void Rationnel::setDenominateur(const int den) {
    _den = den;
}

// --------------------------------------------------------
// Surcharge des opérateurs
// --------------------------------------------------------
Rationnel &Rationnel::operator=(const Rationnel &r) {
    if(this != &r) {
        _num = r.getNumerateur();
        _den = r.getDenominateur();
    }
    return *this;
}

Rationnel Rationnel::operator+(const Rationnel &r) const {
	Rationnel local((_num * r._den) + (r._num * _den), _den * r._den);
	return local;
}

Rationnel Rationnel::operator-(const Rationnel &r) const {
	Rationnel local(r._num * (-1), r._den);
	return ((*this) + local);
}

Rationnel Rationnel::operator*(const Rationnel &r) const {
	Rationnel local(_num * r._num, _den * r._den);
	return local;
}

Rationnel Rationnel::operator/(const Rationnel &r) const {
	Rationnel inv(r._den, r._num);
	return (*this) * inv;
}

bool Rationnel::operator==(const Rationnel &r) const {
	return ((_num == r.getNumerateur()) & (_den == r.getDenominateur()));
}

bool Rationnel::operator>(const Rationnel &r) const {
	return ((_num / _den) > (r.getNumerateur() / r.getDenominateur()));
}	
	
bool Rationnel::operator<(const Rationnel &r) const {
	return (r > (*this));
}	

bool Rationnel::operator>=(const Rationnel &r) const {
	return !(r > (*this));
}	

bool Rationnel::operator<=(const Rationnel &r) const {
	return !((*this) > r);
}


// --------------------------------------------------------
// Affichage
// --------------------------------------------------------
std::ostream &operator<<(std::ostream &flot, const Rationnel &R) {
    if(R.getDenominateur() == 1)
        flot << R.getNumerateur();
    else
        flot << R.getNumerateur() << "/" << R.getDenominateur();
    return flot;
}

