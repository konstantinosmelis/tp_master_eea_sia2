#pragma once

#include "rationnel.h"
#include <iostream>

class Monome {

    private:
        Rationnel _coef;
        int _exp;

    public:
        Monome();
        Monome(Rationnel, int);
        Monome(const Monome &);

        Rationnel getCoef() const;
        int getExp() const;

        void setCoef(const Rationnel&);
        void setExp(const int);

        Monome operator+(const Monome &);
        Monome operator-(const Monome &);
        Monome operator*(const Monome &);
        Monome operator/(const Monome &);

        Monome &operator=(const Monome &);
};

std::ostream &operator<<(std::ostream &, const Monome &);
