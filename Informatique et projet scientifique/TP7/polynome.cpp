#include <cstddef>
#include <iostream>
#include <vector>
#include "monome.h"
#include "polynome.h"

using namespace std;

Polynome::Polynome() { }

Polynome::Polynome(const Monome &m) {
    this->_poly.push_back(m);
}

Polynome::Polynome(const vector<Monome> vm) {
    this->_poly = vm;
}

Polynome::Polynome(const Polynome &p) {
    this->_poly = p.getPoly();
}

vector<Monome> Polynome::getPoly() const {
    return this->_poly;
}

void Polynome::setPoly(const std::vector<Monome> vm) {
    this->_poly = vm;
}

void Polynome::addMonome(const Monome m) {
    this->_poly.push_back(m);
}

Polynome Polynome::operator+(const Monome &m) {
    Polynome somme;
    bool found = false;
    for(Monome monome : this->_poly) {
        if(monome.getExp() == m.getExp()) {
            somme.addMonome(monome + m);
            found = true;
        }
        else
            somme.addMonome(monome);
    }
    if(!found)
        somme.addMonome(m);
    return somme;
}

Polynome Polynome::operator-(const Monome &m) {
    Polynome diff;
    bool found = false;
    for(Monome monome : this->_poly) {
        if(monome.getExp() == m.getExp()) {
            diff.addMonome(monome - m);
            found = true;
        }
        else
            diff.addMonome(monome);
    }
    if(!found)
        diff.addMonome(Monome(Rationnel(-m.getCoef().getNumerateur(), m.getCoef().getDenominateur()), m.getExp()));
    return diff;
}

Polynome Polynome::operator*(const Monome &m) {
    Polynome prod;
    for(Monome monome : this->_poly) {
        prod.addMonome(monome * m);
    }
    return prod;
}

Polynome Polynome::operator/(const Monome &m) {
    Polynome div;
    for(Monome monome : this->_poly) {
        div.addMonome(monome / m);
    }
    return div;
}

Polynome Polynome::operator+(const Polynome &poly) {
    Polynome somme = Polynome(poly);
    for(Monome monome : this->_poly) {
        somme = somme + monome;
    }
    return somme;
}

Polynome Polynome::operator-(const Polynome &poly) {
    Polynome diff = Polynome(poly);
    for(Monome monome : this->_poly) {
        diff = diff - monome;
    }
    return diff;
}

Polynome Polynome::operator*(const Polynome &poly) {
    Polynome prod;
    for(Monome monome1 : this->_poly)
        for(Monome monome2 : poly.getPoly())
            prod = prod + (monome1 * monome2);
    return prod;
}

Polynome Polynome::operator/(const Polynome &poly) {
    Polynome div = Polynome(poly);
    for(Monome monome1 : this->_poly)
        for(Monome monome2 : poly.getPoly())
            div = div + (monome1 / monome2);
    return div;
}

Polynome &Polynome::operator=(const Polynome &poly) {
    if(this != &poly)
        this->_poly = poly.getPoly();
    return *this;
}

ostream &operator<<(ostream &flot, const Polynome &poly) {
    uint i;
    for(i = 0; i < poly.getPoly().size(); i++) {
        flot << (poly.getPoly().at(i).getCoef() >= 0 ? "+" : "") << poly.getPoly().at(i);
    }
    return flot;
}
