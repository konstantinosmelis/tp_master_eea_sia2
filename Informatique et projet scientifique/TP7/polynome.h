#pragma once

#include "monome.h"
#include <vector>

class Polynome {

    private:
        std::vector<Monome> _poly;

    public:
        Polynome();
        Polynome(const Monome &);
        Polynome(const std::vector<Monome>);
        Polynome(const Polynome &);

        std::vector<Monome> getPoly() const;

        void setPoly(const std::vector<Monome>);
        
        void addMonome(const Monome);

        Polynome operator+(const Monome &);
        Polynome operator-(const Monome &);
        Polynome operator*(const Monome &);
        Polynome operator/(const Monome &);

        Polynome operator+(const Polynome &);
        Polynome operator-(const Polynome &);
        Polynome operator*(const Polynome &);
        Polynome operator/(const Polynome &);

        Polynome &operator=(const Polynome &);
};

std::ostream &operator<<(std::ostream &, const Polynome &);
