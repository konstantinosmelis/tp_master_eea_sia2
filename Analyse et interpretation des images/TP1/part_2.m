%pkg load image;

clear all, close all;

img = imread("imageCultures.png");

%% Calcul de l'indice NDVI
img_ndvi = (double(img(:,:,1)) - double(img(:,:,2))) ./ (double(img(:,:,1)) + double(img(:,:,2)));

figure, imagesc(img_ndvi);
figure, imhist(img_ndvi);

%% Seuillage
seuil = 0.7;
img_seuil = img_ndvi > seuil;
figure, imagesc(img_seuil);

%% Identification des objets & calcul des aires
[L, N] = bwlabel(uint8(img_seuil));

A = zeros(1, N);
for i = 1:N
    A(i) = sum(L(:) == i); % length(find(L == i));
end
figure, plot(sort(A));

%% Affichage des grandes parcelles
seuil = 430;
ids = find(A < seuil);
for i = ids
    L(find(L == i)) = 0;
end

L(L > 0) = 1;
figure, imagesc(L);
