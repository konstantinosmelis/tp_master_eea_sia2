%pkg load image;

clear all, close all,

% img = imread("imageAerienne.jpg");
img = imread("imageAerienneFinale.jpg"); %% Image finale
figure, imshow(img);

%% Calcul de l'image bleu maj.
img_piscines = img(:,:,3) > img(:,:,1) & img(:,:,3) > img(:,:,2);
figure, imagesc(img_piscines);

%% Identification des objets & calcul d'aire
[L, N] = bwlabel(uint8(img_piscines));
A = zeros(1, N);
for i = 1:N
    A(i) = sum(L(:) == i); % length(find(L == i));
end
figure, plot(sort(A));

<<<<<<< HEAD
%% Suppression des petits objets
seuil = 160;
ids = find(A < seuil);
for i = ids
    L(find(L == i)) = 0;
end
L(L > 0) = 1;
figure, imagesc(L);

%% Affichage des piscines en rouge
%for i = size(img, 1)
%    for j = size(img, 2)
%        if L(i, j) == 1
%            img(i,j,1) = 255;
%            img(i,j,2) = 0;
%            img(i,j,2) = 0;
%        end
%    end
%end
red_i = img(:, : ,1);
green_i = img(:, : ,2);
blue_i = img(:, : ,3);
red_i(L == 1) = 255;
green_i(L == 1) = 0;
blue_i(L == 1) = 0;
img = cat(3, red_i, green_i, blue_i);

figure, imshow(img);

