%pkg load image;
close all;

img = imread("imageCiel.png");

%% Extraction du soleil
img_s = img .* uint8(img < 180);
figure, imshow(img_s);

%% Rapport Rouge Bleu (R/B)
rrb = double(img_s(:,:,1)) ./ double(img_s(:,:,3));
figure;
subplot(121), imagesc(double(rrb));
subplot(122), imhist(rrb);

%% Difference Rouge Bleu (R-B)
drb = img_s(:,:,1) - img_s(:,:,3);
figure, imagesc(drb);

%% Couverture nuageuse
seuil = 0.8;
nuages = rrb .* (rrb > seuil);
figure, imshow(nuages);

%% Rapport Nuage/Ciel
C = img > 1;
AC = 0;
AN = 0;
for i = 1:size(C, 1)
    for j = 1:size(C, 2)
        if nuages(i,j) > 0
            AN = AN + 1;
        end
        if(C(i,j) > 0)
            AC = AC + 1;
        end
    end
end

fprintf("Taux de couverture nuageuse: %f\n", AN / AC);
