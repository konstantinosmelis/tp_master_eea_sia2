function [C, idI] = kmeans_gs(I, k)
    I = double(I);
    [N, M] = size(I);
    C = randi([0 255], 1, k);
    newC = zeros(size(C));
    idI = zeros(N, M);
    while norm(C - newC) ~= 0
        for i = 1:N
            for j = 1:M
                dist = sqrt((I(i, j) - C) .^ 2);
                [~, index] = min(dist);
                idI(i, j) = index;
            end
        end
        for i = 1:k
            newC(i) = uint8(mean(I(idI == i)));
        end
        C = newC;
    end
end
