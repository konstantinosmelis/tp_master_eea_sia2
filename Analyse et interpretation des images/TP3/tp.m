clear all; close all; clc;
pkg load image;

%%% Niveau de gris %%%
%% Formes
img = imread("formes.pgm");
for k = 1:5
    [C, att] = kmeans_gs(img, k);
    figure(k), imagesc(att);
end

%% Formes bruitees
img = img + (2 * rand(size(img)));
for k = 1:5
    [C, att] = kmeans_gs(img, k);
    figure(k), imagesc(att);
end

%% Brain
img = imread("brain.pgm");
for k = 1:5
    [C, att] = kmeans_gs(img, k);
    figure(k), imagesc(att);
end

%% Brain bruitee
img = img + (2 * rand(size(img)));
for k = 1:5
    [C, att] = kmeans_gs(img, k);
    figure(k), imagesc(att);
end

%%% Couleurs
%% Bird
img = imread("bird.tiff");
for k = 10:20
    [C, att] = kmeans_col(img, k);
    figure, imagesc(att);
end

%% Carres bruites
img = imread("carre_bruit.png");
for k = 1:5
    [C, att] = kmeans_col(img, k);
    figure(k), imagesc(att);
end
