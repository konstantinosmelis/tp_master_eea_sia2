# TPs master EEA SIA2

## UEs

S1
- [Application du traitement du signal et des images](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Application%20du%20traitement%20du%20signal%20et%20des%20images)
- [Instrumentation et chaîne de mesure](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Instrumentation%20et%20chaine%20de%20mesures)
- [Introduction en analyse statistique des données](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Introduction%20en%20analyse%20statistique%20des%20donnees)
- [Introduction en apprentissage automatique](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Introduction%20en%20apprentissage%20automatique)
- [Signaux et systèmes](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Signaux%20et%20systemes)
- [Technique et implémentation des méthodes numériques](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Technique%20et%20implementation%20des%20methodes%20numeriques)
- [Traitement des images](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Traitement%20des%20images)
- [Traitement numérique du signal](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Traitement%20numerique%20du%20signal)

S2
- [Analyse et interprétation des images](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Analyse%20et%20interpretation%20des%20images)
- [Analyse spectrale](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Analyse%20spectrale)
- [BE Apprentissage automatique](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/BE%20Apprentissage%20automatique)
- [Capteurs optiques](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Capteurs%20optiques)
- [Modélisation et estimation](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Modelisation%20et%20estimation)
- [Signaux et télecommunications II](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Signaux%20et%20Telecommunications%20II)

S3
- [Analyse statistique des données](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Analyse%20statistique%20des%20donnees)
- [Capteurs et instrumentation](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Capteurs%20et%20instrumentation)
- [Estimation et optimisation](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Estimation%20et%20optimisation)
- [Informatique et projet scientifique](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Informatique%20et%20projet%20scientifique)
- [Outils avancés pour le traitement des images et de vidéos](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/OA%20pour%20le%20traitement%20d'images%20et%20de%20videos)
- [Outils avancés pour le traitement du signal](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/OA%20pour%20le%20traitement%20du%20signal)
- [Vision par ordinateur](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Vision%20par%20ordinateur)

S4
- [Cartographie thématique](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Cartographie%20thematique)
- [Machine learning et séparation des sources](https://gitlab.com/konstantinosmelis/tp_master_eea_sia2/-/tree/main/Machine%20learning%20et%20separation%20des%20sources)

## License
[GNU General Public License v3.0](https://opensource.org/license/gpl-3-0/)

## Maintainers
[Konstantinos Melissaratos](https://gitlab.com/konstantinosmelis)
