close all; clear all; clc;

%% Correlation
x = randn(1, 50);
y = sin(2 * pi * (1:50) * 0.1);
z = sin(2 * pi * (1:50) * 0.1) .* exp(-(-24:25) .^ 2 / 100);
w = sin(2 * pi * (1:50) .^ 2 / 100);

RxxNB = xcorr(x, x, 'unbiased');
RyyNB = xcorr(y, y, 'unbiased');
RzzNB = xcorr(z, z, 'unbiased');
RwwNB = xcorr(w, w, 'unbiased');

RxxB = xcorr(x, x, 'biased');
RyyB = xcorr(y, y, 'biased');
RzzB = xcorr(z, z, 'biased');
RwwB = xcorr(w, w, 'biased');

figure;
subplot(2, 4, 1);
plot((-50+1):(50-1), RxxNB);
subplot(2, 4, 2);
plot((-50+1):(50-1), RyyNB);
subplot(2, 4, 3);
plot((-50+1):(50-1), RzzNB);
subplot(2, 4, 4);
plot((-50+1):(50-1), RwwNB);
subplot(2, 4, 5);
plot((-50+1):(50-1), RxxB);
subplot(2, 4, 6);
plot((-50+1):(50-1), RyyB);
subplot(2, 4, 7);
plot((-50+1):(50-1), RzzB);
subplot(2, 4, 8);
plot((-50+1):(50-1), RwwB);


%% Detection de cibles dans le cas non-bruite
sigx = [zeros(1, 15), x, zeros(1, 435)] + [zeros(1, 120), x, zeros(1, 330)] + [zeros(1, 136), x, zeros(1, 314)] + [zeros(1, 379), x, zeros(1, 71)] + [zeros(1, 400), x, zeros(1, 50)];
sigy = [zeros(1, 15), y, zeros(1, 435)] + [zeros(1, 120), y, zeros(1, 330)] + [zeros(1, 136), y, zeros(1, 314)] + [zeros(1, 379), y, zeros(1, 71)] + [zeros(1, 400), y, zeros(1, 50)];
sigz = [zeros(1, 15), z zeros(1, 435)] + [zeros(1, 120), z, zeros(1, 330)] + [zeros(1, 136), z, zeros(1, 314)] + [zeros(1, 379), z, zeros(1, 71)] + [zeros(1, 400), z, zeros(1, 50)];
sigw = [zeros(1, 15), w, zeros(1, 435)] + [zeros(1, 120), w, zeros(1, 330)] + [zeros(1, 136), w, zeros(1, 314)] + [zeros(1, 379), w, zeros(1, 71)] + [zeros(1, 400), w, zeros(1, 50)];

figure;
plot(sigx);
figure;
plot(sigy);
figure;
plot(sigz);
figure;
plot(sigw);

[corrx, dec] = xcorr(sigx, x);
figure;
plot(dec, corrx);
[corry, dec] = xcorr(sigy, y);
figure;
plot(dec, corry);
[corrz, dec] = xcorr(sigz, z);
figure;
plot(dec, corrz);
[corrw, dec] = xcorr(sigw, w);
figure;
plot(dec, corrw);

corrx_norm = corrx ./ max(corrx);
plot(corrx_norm);
for i = 1:length(corrx_norm)
    if corrx_norm(i) > 0.7
        fprintf("cible detectee a i=%d\n", i);
    end
end


%% Detection de cibles dans le cas bruite
for i = [0.25, 0.5, 1]
    fprintf("bruit = %f\n", i)
    b = randn(1, 500) * i;
    sigxb = sigx + b;
    [corrxb, dec] = xcorr(sigxb, x);
    figure;
    plot(dec, corrxb);
    corrxb_norm = corrxb ./ max(corrxb);
    for j = 1:length(corrx_norm)
        if corrx_norm(j) > 0.7
            fprintf("\tcible detectee a i=%d\n", j);
        end
    end
end


%% Stationnarite
M = 100000;
N = 100;
f_0 = 0.1;
t = 1:N;
X = zeros(M, N);
for i = 1:M
    phi = (2 * pi) .* rand;
    X(i, :) = cos((2 * pi * f_0 * t) + phi);
end

figure;
subplot(1, 2, 1);
plot(mean(X));
subplot(1, 2, 2);
plot(var(X));
