clear; close all; clc;

%% Part 1
[signal, Fe] = audioread('signal0.wav');
audioinfo('signal0.wav');

[tf_signal, f_signal] = tfsc2(signal, Fe);

plot(f_signal, abs(tf_signal));
title("Spectre du signal"), xlabel("Frequence (en Hz)"), ylabel("SIGNAL(f)");

%% Part 2
load SignalReconst.mat;

figure;
plot(t, x);
title("Representation du signal x"), xlabel("Temps"), ylabel("Amplitude");

[tf_x, f_x] = tfsc2(x, 1);
figure;
plot(f_x, abs(tf_x));
title("Spectre du signal x"), xlabel("Frequence (en Hz)"), ylabel("X(f)");

for S = 2:5
    ech = zeros(1, length(x));
    for i = 1:length(x)
        if mod(i, S) == 0
            ech(i) = x(i);
        else
            ech(i) = 0;
        end
    end

    figure;
    plot(t, ech, '-');
    title(sprintf("Signal x echantillonne avec S=%d", S)), xlabel("Temps"), ylabel("Amplitude");
    
    [tf_xs, f_xs] = tfsc2(ech, 1);
    figure;
    plot(f_xs, abs(tf_xs));
    title(sprintf("Spectre du signal x avec S=%d", S)), xlabel("Frequence (en Hz)"), ylabel("X(f)");
    
    filtre = f_xs > -0.12 & f_xs < 0.12;
    tf_xs_f = filtre .* tf_xs;
    figure;
    plot(f_xs, abs(tf_xs_f));
    title(sprintf("Spectre filtre pour S=%d", S)), xlabel("Frequence (en Hz)"), ylabel("X(f)");
    
    figure;
    plot(t, tfsc2_inv(tf_xs_f));
    title(sprintf("Signal reconstruit pour S=%d", S)), xlabel("Temps"), ylabel("Amplitude");
end

%% Part 3
y = -2:0.01:1;
for N = 2:5
    figure;
    plot(y), hold on, plot(quantification(y, N));
    title(sprintf("Signal quantifie avec N=%d", N)), xlabel("Temps"), ylabel("Amplitude");
end

load SignalQuantif.mat;

%PlaySignal(quantification(x, 1), Fe, 1) % A N=1 on entend rien mais a N=5 on entend la musique clairement

%%
N = 1:15;
rsb = zeros(1,length(N));
for i = N
    sq = quantification(x, i);
    eb = x - sq;
    rsb(i) = 10*log10(mean(x .^ 2) ./ mean(eb .^ 2));
    
    figure;
    hist(eb, 100);
    title(sprintf("Histogramme de l'erreur de quantification pour N=%d", i));
end

figure;
plot(N, rsb);
title("Rapport Signal Bruit en fonction des bits"), xlabel("Nombre de bits"), ylabel("RSB"); 
