function res = quantifiction(x, N)
    a = (max(x) - min(x)) / 2^N;
    b = min(x);
    y = round((x - b) / a);
    res = a * y + b;
end
