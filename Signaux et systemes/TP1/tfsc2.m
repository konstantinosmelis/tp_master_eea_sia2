function [Xhat,f] = tfsc2(x,fe)
%% Sans zero-padding (quand le signal contient bcp d'échantillons)
	N = length(x);
	Nfft = N;
	f=[0:Nfft-1]/Nfft*fe-fe/2;
	Xhat = fftshift(fft(x,Nfft))/fe;
end
