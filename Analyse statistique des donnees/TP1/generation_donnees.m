function X = generation_donnees(n, a1, a2, y1, y2)
    X = zeros((2 * n) + 1 + 1, 2);
    for i = -n:n
        X(i + (n + 1), 1) = a1 * i;
        X(i + (n + 1), 2) = a2 * i;
    end
    X((2 * n) + 2, :) = [y1, y2]';
end
