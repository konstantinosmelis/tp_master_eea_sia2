function R = mat_coef_corr(X)
    [n, p] = size(X);
    W = eye(n) / n;
    g = X' * W * ones(n, 1);
    C = (X' * W * X) - (g * g');
    D = eye(p) ./ sqrt(diag(C));
    R = D * C * D;
end

