close all, clear, clc;

load("X.mat");
[n, p] = size(X);

%% Premiere etude de la reflectance des materiaux
% Representations partielles des donnees
figure, plot(X(:, 1));
hold on;
plot(X(:, 2));
plot(X(:, p));
hold off;
title("Variation de la variable x^j en fonction de i");
legend("j = 1", "j = 2", "j = p");
xlabel("i"), ylabel("x^j_i");

figure, scatter(X(:, 1) , X(:, 2));
title("Nuage de point par rapport aux variables d'indices 1 et 2");
xlabel("Variable 1"), ylabel("Variable 2");

figure, scatter(X(:, 1) , X(:, p));
title("Nuage de point par rapport aux variables d'indices 1 et p");
xlabel("Variable 1"), ylabel("Variable p");

figure, plot(X(1, :));
hold on;
plot(X(2, :));
plot(X(n, :));
hold off;
title("Courbe de variation de l'individu i");
legend("i = 1", "i = 2", "i = n");
xlabel("j"), ylabel("x^j_i");

% Analyse monodimentionnelle
W = eye(n) / n;

%q1
g = X' * W * ones(n, 1);
figure, plot(g);
title("Variation de la moyenne, obtenue par calcul matriciel");
xlabel("j"), ylabel("Moyenne");

moy_mean = mean(X, 1);
figure, plot(moy_mean);
title("Variation de la moyenne");
xlabel("j"), ylabel("Moyenne");

figure, plot(g' - moy_mean);
title("Difference des moyennes obtenues");
xlabel("j"), ylabel("Différence");

%q2
C = (X' * W * X) - (g * g');

ecart_mat = sqrt(diag(C));
figure, plot(ecart_mat);
title("Variation de l'ecart-type, obtenu par calcul matriciel");
xlabel("j"), ylabel("Ecart-type");

ecart_std = std(X, 1);
figure, plot(ecart_std);
title("Variation de l'ecart-type");
xlabel("j"), ylabel("Ecart-type");

figure, plot(ecart_mat' - ecart_std);
title("Difference des ecart-types obtenus");
xlabel("j"), ylabel("Différence");

% Analyse bidimentionnelle
%q1
R = mat_coef_corr(X);

figure, plot(R(1, :));
title("Variation du coefficient de correlationde la variable 1");
xlabel("j"), ylabel("r_{1j}");

%q2
r = zeros(1, p - 1);
for k = (1:p-1)
    r(k) = mean(diag(R, k));
end

figure, plot(r);
title("Moyenne des coefficient de correlation");

%% Deuxieme etude de la reflectance des materiaux
% Histogramme
x = X(:);
minX = min(x);
maxX = max(x);

for m = [10, 20]
    num_var = zeros(1, m);
    step = (maxX - minX) / m;
    num_var(1:m) = sum(x >= minX + step * (0:m-1) & x < minX + step * (1:m));
    f = num_var / (n * p);

    figure;
    bar(f);
    title(sprintf("Histogramme des frequences pour m=%d", m));
end

% Moyenne et mode
moyx = mean(x);
% Mode pour m = 10 => [0.8285; 0.9170]
% Mode pour m = 20 => [0.7400; 0.7842]

%% Etude de donnees synthetiques
%q1
n = 10;
a1 = 1;
a2 = 2;
y1 = 6;
y2 = 3;

X = generation_donnees(n, a1, a2, y1, y2);

figure, scatter(X(:, 1) , X(:, 2));
title("Nuage de point");

%q2
r = zeros(50, 1);
for n = 1:100
    r(n) = ((a1 * a2) / (2 * n + 1)) / (std(X(:, 1)) * std(X(:, 2)));
end

figure, plot(r);
title("Evolution du coefficient de correlation");
xlabel("n"), ylabel("Coefficient de correlation");

%q3
y2 = 0;
r = zeros(2 * 30 + 1, 1);
for y1 = -30:30
    X(n, 1) = y1;
    r(y1 - y1 + 1) = ((a1 * a2) / (2 * n + 1)) / (std(X(:, 1)) * std(X(:, 2)));
end

figure, plot(y1, r);
title("Evolution du coefficient de correlation");
xlabel("y^1"), ylabel("Coefficient de correlation");

