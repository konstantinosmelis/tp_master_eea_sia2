close all, clear, clc;

%% Variables aleatoires
% q1
u = -1 + (2 - (-1)) .* rand(1000, 1);
fprintf("Moyenne theorique: %f, moyenne estimee: %f\n", (-1 + 2) / 2, mean(u));
fprintf("Variance theorique: %f, variance estimee: %f\n", ((2 - (-1))^2) / 12, var(u));

% q2
for N = [1000 10]
    fprintf("\nPour N=%d realisations des gaussienne\n", N)
    g = (randn(N, 1) * sqrt(3)) + 5;
    fprintf("Moyenne theorique: 5, moyenne estimee: %f\n", mean(g));
    fprintf("Variance theorique: 3, variance estimee: %f\n", var(g));
end

%% Theoreme central limite
u = rand(30, 1000);
figure, hist(u(1, :));
title("Histogramme sur la premiere ligne");

figure, hist(sum(u, 1));
title("Histogramme sur la somme de colonnes");

%% Test sur les variance et moyennes d'echantillons independants
load("y.mat");
load("z.mat");

% q1
my = mean(y);
mz = mean(z);
sy = var(y);
sz = var(z);

vy = ((length(y) - 1) * sy) / 0.4;
vz = ((length(z) - 1) * sz) / 0.4;

A = 24.33;
B = 59.342;

fprintf("\nTest sur les variance\n");
fprintf("H0 accepte? pour y: %d\n", vy > A && vy < B);
fprintf("H0 accepte? pour z: %d\n", vz > A && vz < B);

% q2
Z = (my - mz) / sqrt(0.8 / 40);
A = 1.96;
fprintf("\nTest sur les moyennes\n");
fprintf("H0 accepte?: %d\n", Z > -A && Z < A);

% q3
s = (((40 - 1) * sy) + ((40 - 1) * sz)) / (40 + 40 - 2);
t = sqrt(1 / (2 * s / 40)) * (my - mz);

A = 1.990;
fprintf("\nTest de student sur des echantillons independants\n");
fprintf("H0 accepte?: %d\n", t > -A && t < A);

%% Comparaison des moyennes deechantillons apparies
load("sang.mat");
d = sang(2, :) - sang(1, :);

t = mean(d) / sqrt(var(d) / length(d));

A = 2.093;
fprintf("\nTest de student sur des echantillons apparies\n");
fprintf("H0 accepte?: %d\n", t > -A && t < A);

%% Test de correlation
load("x.mat");

% q1
x1 = x(1:30);
x2 = x(2:31);

% q2
r = corrcoef(x1, x2);
r = r(1, 2);

% q3
t = sqrt(30 - 2) * (r / sqrt(1 - r^2));

A = 2.048;
fprintf("\nTest de correlation sur x\n");
fprintf("H0 accepte?: %d\n", t > -A && t < A);

% q4
y = filter(1, [1 -.4], x);

y1 = y(1:30);
y2 = y(2:31);
r = corrcoef(y1, y2);
r = r(1, 2);
t = sqrt(30 - 2) * (r / sqrt(1 - r^2));

fprintf("\nTest de correlation sur y\n");
fprintf("H0 accepte?: %d\n", t > -A && t < A);

%% Tests d’adequation
rand('seed', 1);

% q1
x = rand(60, 1);

% q2
h = hist(x, 6);

p0 = ones(6, 1) / 6;
p = h' ./ 60;
v = 60 * sum(((p0 - p).^2) / (1 / 6));

A = 11.070;
fprintf("\nTest d'adequation du Chi^2\n");
fprintf("H0 accepte?: %d\n", v < A);

% q3
x = sort(x);
y = linspace(0, 1, 60);
Fx = zeros(1, length(y));

for m = 1:length(y)
    if y(m) < x(1)
        Fx(m) = 0;
    elseif y(m) >= x(end)
        Fx(m) = 1;
    else
        for n = 1:(length(x) - 1)
            if y(m) >= x(n) && y(m) < x(n + 1)
                Fx(m) = n / length(x);
            end
        end
    end
end

[Dks, idxDks] = max(abs(Fx - y));

figure, stairs(sort(Fx));
hold on, stairs(y);
line([idxDks idxDks], [Fx(idxDks) y(idxDks)]), hold off;
title("Fonctions de repartitions theoriques et empiriques");
legend("F_X theorique", "F_X empirique");

A = 0.1723;
fprintf("\nTest d'adequation de Kolmogorov-Smirnov de la fonction rand\n");
fprintf("H0 accepte?: %d\n", Dks < A);

% q4
randn('seed', 1)
x = randn(60, 1);
x = sort(x);
y = (1 / 2) * (1 + erf(x / sqrt(2)));
Fx = zeros(1, length(y));

for m = 1:length(y)
    if y(m) < x(1)
        Fx(m) = 0;
    elseif y(m) >= x(end)
        Fx(m) = 1;
    else
        for n = 1:(length(x) - 1)
            if y(m) >= x(n) && y(m) < x(n + 1)
                Fx(m) = n / length(x);
            end
        end
    end
end

[Dks, idxDks] = max(abs(Fx - y'));

figure, stairs(sort(Fx));
hold on, stairs(y);
line([idxDks idxDks], [Fx(idxDks) y(idxDks)]), hold off;
title("Fonctions de repartitions theoriques et empiriques");
legend("F_X theorique", "F_X empirique");

fprintf("\nTest d'adequation de Kolmogorov-Smirnov de la fonction randn\n");
fprintf("H0 accepte?: %d\n", Dks < A);

%% Rapport de vraisemblance
load("Rayleigh.mat");

V = sum(Rvar.^2);

A = 233;
fprintf("\nTest du rapport de vraisemblance\n");
fprintf("H0 accepte?: %d\n", V > -A && V < A);

