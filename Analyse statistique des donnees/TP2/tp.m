close all, clear, clc;

load("X.mat");
[n, p] = size(X);

%% ACP: Premiere etude de la reflectance des materiaux
% ACP avec metrique identitee
C = cov(X);

[vec_propres, val_propres] = eig(C);
val_propres = abs(diag(val_propres));

[val_propres, index] = sort(val_propres, 'descend');
vec_propres = vec_propres(:, index);

figure, semilogy(val_propres);
title("Variation des valeurs propres (echelle logarithmique)");

figure, plot(val_propres(1:10));
title("Variation des 10 premieres valeurs propres");

pourcentage_inertie = cumsum(val_propres) / sum(val_propres);

figure, plot(pourcentage_inertie);
title("Variation du pourcentage d'inertie");

figure, plot(pourcentage_inertie(1:10));
title("Variation du pourcentage d'inertie des espace 1 a 10");

proj1 = X * vec_propres(:, 1);
figure, plot(proj1, 0, '*');
title("Nuage projette sur une dimension");

proj2 = X * vec_propres(:, 1:2);
figure, scatter(proj2(:, 1), proj2(:, 2), '*');
title("Nuage projette sur deux dimensions");

%% ACP: Deuxieme etude de la reflectance des materiaux
% Donnees considerees
clear;
load("ign_crs.mat");
load("ign_fn.mat");
load("sed_crs.mat");

X = [ign_crs; ign_fn; sed_crs];
[n, p] = size(X);

% Representation partielle des donnees
figure, scatter(ign_crs(:, 1), ign_crs(:, 2), 'b*');
hold on;
scatter(ign_fn(:, 1), ign_fn(:, 2), 'ro');
scatter(sed_crs(:, 1), sed_crs(:, 2), 'm+');
hold off;
title("Nuage des points pours les variables 1 et 2");
legend(["Igneous - Coarse", "Igneous - Fine", "Sediment - Coarse"]);

figure, scatter(ign_crs(:, 1), ign_crs(:, p), 'b*');
hold on;
scatter(ign_fn(:, 1), ign_fn(:, p), 'ro');
scatter(sed_crs(:, 1), sed_crs(:, p), 'm+');
hold off;
title("Nuage des points pours les variables 1 et p");
legend(["Igneous - Coarse", "Igneous - Fine", "Sediment - Coarse"]);

% ACP avec metrique identite
C = cov(X);
[vec_propres, val_propres] = eig(C);
val_propres = abs(diag(val_propres));

[val_propres, index] = sort(val_propres, 'descend');
vec_propres = vec_propres(:, index);

projX = X * vec_propres(:, 1);
figure, scatter(projX(1:34, 1), zeros(34, 1), 'b*');
hold on;
scatter(projX(35:67, 1), zeros(33, 1), 'ro');
scatter(projX(68:end, 1), zeros(15, 1), 'm+');
hold off;
title("Nuage projette sur une dimension");
legend(["Igneous - Coarse", "Igneous - Fine", "Sediment - Coarse"]);

projX = X * vec_propres(:, 1:2);
figure, scatter(projX(1:34, 1), projX(1:34, 2), 'b*');
hold on;
scatter(projX(35:67, 1), projX(35:67, 2), 'ro');
scatter(projX(68:end, 1), projX(68:end, 2), 'm+');
hold off;
title("Nuage projette sur deux dimensions");
legend(["Igneous - Coarse", "Igneous - Fine", "Sediment - Coarse"]);

%% AFD: etude de la reflectance des materiaux
% AFD
Y = X - mean(X);
C = cov(X);

q1 = size(Y(1:34), 1) / n;
q2 = size(Y(34:67), 1) / n;
q3 = size(Y(68:end), 1) / n;

b1 = q1 * mean(ign_crs)' * mean(ign_crs);
b2 = q2 * mean(ign_fn)' * mean(ign_fn);
b3 = q3 * mean(sed_crs)' * mean(sed_crs);

B = b1 + b2 + b3;

[vec_propres, val_propres] = eig(B / C);
val_propres = abs(diag(val_propres));

[val_propres, index] = sort(val_propres, 'descend');
vec_propres = vec_propres(:, index);

figure, semilogy(val_propres);
title("Variation des valeurs propres (echelle logarithmique)");

figure, plot(val_propres(1:10));
title("Variation des 10 premieres valeurs propres");

projX = X * vec_propres(:, 1);
figure, scatter(projX(1:34, 1), zeros(34, 1), 'b*');
hold on;
scatter(projX(35:67, 1), zeros(33, 1), 'ro');
scatter(projX(68:end, 1), zeros(15, 1), 'm+');
hold off;
title("Nuage projette sur une dimension");
legend(["Igneous - Coarse", "Igneous - Fine", "Sediment - Coarse"]);

projX = X * vec_propres(:, 1:2);
figure, scatter(projX(1:34, 1), projX(1:34, 2), 'b*');
hold on;
scatter(projX(35:67, 1), projX(35:67, 2), 'ro');
scatter(projX(68:end, 1), projX(68:end, 2), 'm+');
hold off;
title("Nuage projette sur deux dimensions");
legend(["Igneous - Coarse", "Igneous - Fine", "Sediment - Coarse"]);

