close all ;

img = imread("lena.tif");

img = rgb2xyz(img);

imrg = img(:,:,1);
imv = img(:, :, 2);
imb = img(:, :, 3);

figure;
subplot(2, 3, 1), imshow(imrg), title("Rouge");
subplot(2, 3, 2), imshow(imv), title("Vert");
subplot(2, 3, 3), imshow(imb), title("Bleu");
subplot(2, 3, 4), imhist(imrg);
subplot(2, 3, 5), imhist(imv);
subplot(2, 3, 6), imhist(imb);

imrg_norm = imadjust(imrg);
imv_norm = imadjust(imv);
imb_norm = imadjust(imb);

figure;
subplot(2, 3, 1), imshow(imrg_norm), title("Rouge normalise");
subplot(2, 3, 2), imshow(imv_norm), title("Vert normalise");
subplot(2, 3, 3), imshow(imb_norm), title("Bleu normalise");
subplot(2, 3, 4), imhist(imrg_norm);
subplot(2, 3, 5), imhist(imv_norm);
subplot(2, 3, 6), imhist(imb_norm);

img_norm = cat(3, imrg_norm, imv_norm, imb_norm);
figure; imshow(img_norm), title("Image reconstruite");

