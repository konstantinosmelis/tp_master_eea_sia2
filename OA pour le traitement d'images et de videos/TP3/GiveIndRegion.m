function [lQ, cQ] = GiveIndRegion(R, IndR)
% Donne les lignes et colonnes de la region IndR
	[lQ, cQ] = find(R == IndR);
    lQ = unique(lQ);
    cQ = unique(cQ);
end

