function IndN = neighborhoodRegion(R, IndR)
% Donne les indices des régions voisines d'une region donnée
    Mask = zeros(size(R));
    Mask(R == IndR) = 1;

    % Nhood = [0 1 0;1 1 1;0 1 0];
    Nhood = ones(3);
    se = strel('arbitrary', Nhood);
    MaskDilate = imdilate(Mask, se);
    V = MaskDilate - Mask; % zones voisines
	IndN = unique(R(V == 1));
end
