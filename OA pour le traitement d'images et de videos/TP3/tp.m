close all, clear, clc;

img = imread("medtest.png");
img = double(img);

%% Etape 1: Split
k = 1;
R = ones(size(img));
s_split = 100;

while k <= max(R(:))
    [rowR, colR] = GiveIndRegion(R, k);
    Ik = img(rowR, colR);
    maxR = max(R(:));
    if var(Ik(:)) > s_split
        R(rowR(1:ceil(length(rowR)/2)), colR(1:ceil(length(colR)/2))) = maxR + 1;
        R(rowR((ceil(length(rowR)/2)+1):end), colR(1:ceil(length(colR)/2))) = maxR + 2;
        R(rowR(1:ceil(length(rowR)/2)), colR((ceil(length(colR)/2)+1):end)) = maxR + 3;
        R(rowR((ceil(length(rowR)/2)+1):end), colR((ceil(length(colR)/2)+1):end)) = maxR + 4;
    end
    k = k + 1;
end

figure, imagesc(R);
axis image;

%% Merge
mergeR = R;
listeRegion = unique(R);
s_merge = 5000;
while ~isempty(listeRegion)
    merged = 0;
    k = listeRegion(1);
    listeRegion(1) = [];
    [rowR, colR] = GiveIndRegion(mergeR, k);
    Ik = img(rowR, colR);
    for vk = neighborhoodRegion(mergeR, k)'
        [rowRVk, colRVk] = GiveIndRegion(mergeR, vk);
        Ivk = img(rowRVk, colRVk);
        if var(union(Ik(:), Ivk(:))) < s_merge
            mergeR(rowRVk, colRVk) = k;
            listeRegion(listeRegion == vk) = [];
            merged = 1;
        end
    end
    if merged
        listeRegion = [k; listeRegion];
    end
end

figure, imagesc(mergeR);
axis image;

c = mergeR(130, 250);
mask = (mergeR == c);

figure, imshow(uint8(img .* mask))

mask = cat(3, mask, mask, mask * 150);
out = cat(3, img, img, img) + mask;

figure, imshow(uint8(out));
