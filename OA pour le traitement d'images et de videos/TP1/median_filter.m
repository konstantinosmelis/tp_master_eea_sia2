function [out] = median_filter(img, n)
    out = img;
    for k = 2:(size(img, 1) - 1)
        for l = 2:(size(img, 2) - 1)
            win = img((-1:1) + k, (-1:1) + l);
            out(k, l) = median(win(:));
        end
    end
end

