function [out] = VMF(img)
    out = img;
    for k = 2:(size(img, 1) - 1)
        for l = 2:(size(img, 2) - 1)
            win = reshape(img((-1:1) + k, (-1:1) + l, :), 9, 1, 3);
            %win = img((-1:1) + k, (-1:1) + l, :);
            win = [win(:, :, 1) win(:, :, 2) win(:, :, 3)];
            d = zeros(9, 1);
            for m = 1:9
                d(m) = sum(sqrt(sum((win - win(m, :)) .^ 2)));
            end
            [~, imin] = min(d);
            out(k, l, :) = win(imin, :);
        end
    end
end
