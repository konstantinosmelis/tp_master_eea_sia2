close all, clear, clc;

%% Image bruitee
img = imread('img/lena.pgm');
img = double(img);

for v = 10:2:20
    img_b = img + randn(size(img)) * v;
    figure, imshow(uint8(img_b));
    title(sprintf('Image bruitee avec un bruit blanc d''equart-type %d', v));
    imwrite(uint8(img_b), sprintf('./out/img_b_v%d.png', v));
end

m = randi([0, 20], size(img));
img_ps = img;
img_ps(m == 0) = 0;
img_ps(m == 20) = 255;

figure, imshow(uint8(img_ps));
title('Image bruitee avec un bruit de type poivre et sel');
imwrite(uint8(img_ps), './out/img_b_ps.png');

%% Algorithme simple de debruitage
for v = 10:2:20
    img_b = img + randn(size(img)) * v;
    img_m = uint8(convolution(img_b, [1 1 1 ; 1 1 1 ; 1 1 1] / 9)); % moyenneur
    img_g = uint8(convolution(img_b, [1 2 1 ; 2 4 2 ; 1 2 1] / 16)); % gaussien

    figure, imshow(img_m);
    title(sprintf('Filtre moyenneur applique a une image bruitee par un bruit blanc d''equart-type %d', v));
    imwrite(img_m, sprintf('./out/img_db_v%d_moy.png', v));

    figure, imshow(img_g);
    title(sprintf('Filtre gaussien applique a une image bruitee par un bruit blanc d''equart-type %d', v));
    imwrite(img_g, sprintf('./out/img_db_v%d_gaus.png', v));
end

img_ps_moy = uint8(convolution(img_ps, [1 1 1 ; 1 1 1 ; 1 1 1] / 9));
figure, imshow(img_ps_moy);
title('Filtre moyenneur applique a une image bruitee par un bruit de type poivre et sel');
imwrite(img_ps_moy, './out/img_db_ps_moy.png');

img_ps_gaus = uint8(convolution(img_ps, [1 2 1 ; 2 4 2 ; 1 2 1] / 16));
figure, imshow(img_ps_gaus);
title('Filtre gaussien applique a une image bruitee par un bruit de type poivre et sel');
imwrite(img_ps_gaus, './out/img_db_ps_gaus.png');

img_ps_med = uint8(median_filter(img_ps));
figure, imshow(img_ps_med);
title('Image avec bruit de type poivre et sel corrigee par un filtre median');
imwrite(img_ps_med, './out/img_db_ps_med.png');

%% Filtrage en couleur
img = imread('img/carres.tif');
img = img * 255;
img = double(img);

% Bruitage de l'image couleur
R = img(:, :, 1);
G = img(:, :, 2);
B = img(:, :, 3);

r = randi([0, 15], size(img, 1, 2));
g = randi([0, 15], size(img, 1, 2));
b = randi([0, 15], size(img, 1, 2));

R(r == 0) = 0;
R(r == 20) = 255;
G(g == 0) = 0;
G(g == 20) = 255;
B(b == 0) = 0;
B(b == 20) = 255;

img = cat(3, R, G, B);

figure, imshow(uint8(img));
title('Image couleur bruitee');
imwrite(uint8(img), './out/img_col_bruit.png');

moyR = convolution(img(:, :, 1), [1 1 1; 1 1 1; 1 1 1] / 9);
moyG = convolution(img(:, :, 2), [1 1 1; 1 1 1; 1 1 1] / 9);
moyB = convolution(img(:, :, 3), [1 1 1; 1 1 1; 1 1 1] / 9);
img_moy = uint8(cat(3, moyR, moyG, moyB));

figure, imshow(img_moy);
title('Image couleur corrigee par un filtre moyenneur');
imwrite(img_moy, './out/img_col_db_moy.png');

medR = median_filter(img(:, :, 1));
medG = median_filter(img(:, :, 2));
medB = median_filter(img(:, :, 3));
img_med = uint8(cat(3, medR, medG, medB));

figure, imshow(img_med);
title('Image couleur corrigee par un filtre median');
imwrite(img_med, './out/img_col_db_med.png');

img_med_v = uint8(VMF(double(img)));
figure, imshow(img_med_v);
title('Image couleur corrigee par un filtre median vectoriel');
imwrite(img_med_v, './out/img_col_db_med_v.png');

