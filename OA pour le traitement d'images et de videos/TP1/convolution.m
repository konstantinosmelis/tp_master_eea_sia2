function [out] = convolution(img, kernel)
    out = img;
    for k = 2:(size(img, 1) - 1)
        for l = 2:(size(img, 2) - 1)
            win = img((-1:1) + k, (-1:1) + l);
            conv_val = sum(sum(win .* kernel));
            out(k, l) = conv_val;
        end
    end
end

