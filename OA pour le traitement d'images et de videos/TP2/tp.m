close all, clear, clc;

load("seq100.mat");

%% Detection de mouvement par differences d'images
% v = VideoReader("autoroute.mp4");
% FPS = v.FrameRate;
% figure;
% while hasFrame(v)
%     frame = readFrame(v);
%     imshow(frame);
%     pause(1 / FPS);
% end

figure;
for k = 1:numFrames
    imshow(seq100(:, :, :, k));
    title("100 premieres frames de la video");
    pause(0.05);
end

% Difference deux a deux
diff = zeros(size(seq100));
for k = 1:(numFrames-1)
    diff(:, :, :, k) = seq100(:, :, :, k + 1) - seq100(:, :, :, k);
end

v = VideoWriter('out/vid_diff2o2.avi', 'Motion JPEG AVI');
open(v);
figure;
for k = 1:numFrames
    imshow(uint8(diff(:, :, :, k)));
    writeVideo(v, uint8(diff(:, :, :, k)));
    title("Differences deux a deux");
    pause(.05);
end
close(v);

% Soustraction du fond
[n, p, d, ~] = size(seq100);
background = zeros(n, p, d);
for a = 1:d
    for k = 1:n
        for l = 1:p
            background(k, l, a) = median(seq100(k, l, a, :));
        end
    end
end

figure, imshow(uint8(background));
title("Image du fond");
imwrite(uint8(background), "out/background.png");

diff_back = zeros(size(seq100));
for k = 1:numFrames
    diff_back(:, :, :, k) = seq100(:, :, :, k) - uint8(background);
end

v = VideoWriter('out/vid_diff2o2_back.avi', 'Motion JPEG AVI');
open(v);
figure;
for k = 1:numFrames
    imshow(uint8(diff_back(:, :, :, k)));
    writeVideo(v, uint8(diff_back(:, :, :, k)));
    title("Differences avec le fond");
    pause(.1);
end

S = 30;
vid_seuillee = zeros(n, p, numFrames);
for k = 1:numFrames
    vid_seuillee(:, :, k) = double(rgb2gray(uint8(diff_back(:, :, :, k)))) > S;
end

v = VideoWriter('out/vid_seuillee_diff2o2_back.avi', 'Motion JPEG AVI');
open(v);
figure;
for k = 1:numFrames
    imshow(vid_seuillee(:, :, k));
    writeVideo(v, vid_seuillee(:, :, k));
    title("Video seuillee");
    pause(.05);
end
close(v);

%% Detection de mouvement au fil de l'eau
% Moyenne recursive du fond
alpha = .05;
S = 30;
mt = seq100(:, :, :, 1);
dt = zeros(n, p, d, numFrames);
vid_seuillee = zeros(n, p, numFrames);
for k = 1:numFrames
    mt = alpha * seq100(:, :, :, k) + ((1 - alpha) * mt);
    dt(:, :, :, k) = abs(seq100(:, :, :, k) - mt);
    vid_seuillee(:, :, k) = rgb2gray(uint8(dt(:, :, :, k))) > S;
end

v = VideoWriter('out/vid_seuillee_moy_rec.avi', 'Motion JPEG AVI');
open(v);
figure;
for k = 1:numFrames
    imshow(vid_seuillee(:, :, k));
    writeVideo(v, vid_seuillee(:, :, k));
    title("Video seuillee par calcul recursif du fond");
    pause(.05);
end
close(v);

% Estimation gaussienne
alpha = .6;
N = 1.7;
Mt = seq100(:, :, :, 1);
Vt = 0;
vid_seuillee = zeros(n, p, d, numFrames);
for k = 1:numFrames
    Mt = alpha * seq100(:, :, :, k) + ((1 - alpha) * Mt);
    Dt = abs(seq100(:, :, :, k) - Mt);
    Vt = (alpha * Dt.^2) + ((1 - alpha) * Vt);
    vid_seuillee(:, :, :, k) = Dt > (N * sqrt(double(Vt)));
end

v = VideoWriter('out/vid_seuillee_est_gauss.avi', 'Motion JPEG AVI');
open(v);
figure;
for k = 1:numFrames
    imshow(vid_seuillee(:, :, k));
    writeVideo(v, vid_seuillee(:, :, k));
    title("Video seuillee par estimation gaussienne");
    pause(.05);
end
close(v);
