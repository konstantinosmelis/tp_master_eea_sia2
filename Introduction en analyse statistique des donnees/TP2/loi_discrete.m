clc;

%% loi de Poisson de param 3
classes = 5;
lamda = 3;
nptheo = 0;
for i=1:length(Y)
    nptheo = nptheo + ((exp(-lamda) * lamda^Y(i)) / factorial(Y(i)));
end
k = hist(Y, 5).';
chi2 = sum(((k - nptheo) .^ 2) ./ nptheo);
fprintf("chi2: %f\n", chi2);
fprintf("hypothese acceptee: %d\n", chi2 < 30.14);