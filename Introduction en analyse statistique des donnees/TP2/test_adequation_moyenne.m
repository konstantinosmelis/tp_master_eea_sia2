clc;

%% loi gaussienne de moyenne 0.2 et var 3
g = sqrt(3) * randn(20, 1) + 0.2;
m_supposee = 0;

t = (mean(g) - m_supposee) / sqrt(3 / 20);

fprintf("t: %f\n", t);
