%% Part 1
N = 100;
X = gamm_rnd(N, 1, 1, 2);

moy_emp = mean(X);
var_emp = var(X);

k_mm = ((moy_emp^2) / var_emp);
theta_mm = var_emp / moy_emp;

fprintf("Moyenne emp. de X: %f, Variance emp. de X: %f\n", moy_emp, var_emp);
fprintf("k_mm: %f, theta_mm: %f\n", k_mm, theta_mm);

L = prod((X.^(k_mm-1)) .* (exp(-X./theta_mm))/((theta_mm^k_mm) * gamma(k_mm)));
LL = log(L);

theta_mv = sum(X) / (N * k_mm);

somme = sum(X);
k = 0:0.001:20;
phi = (-N .* k) .* log(somme ./ (N .* k)) - (N .* log(gamma(k))) + ((k - 1) .* sum(log(X))) - (N .* k); %% LL(theta_mv, k)

figure;
plot(k, phi);
xlabel("k");
ylabel("φ(k)");

[val_max, i_max] = max(phi);
k_mv = k(i_max);

fprintf("k_mv: %f, theta_mv: %f\n", k_mv, theta_mv);

fprintf("\n========================================\n\n");

%% Part 2
N = 100;
M = 200;
param_k = 5;
theta = 1;
X = gamm_rnd(N, M, param_k, theta);

k_mm = (mean(X).^2) ./ var(X);
theta_mm = var(X) ./ mean(X);

moyenne_k_mm = mean(k_mm);
moyenne_theta_mm = mean(theta_mm);
var_k_mm = var(k_mm);
var_theta_mm = var(theta_mm);
eqm_k_mm = var_k_mm + (moyenne_k_mm - param_k)^2;
eqm_theta_mm = var_theta_mm + (moyenne_theta_mm - theta)^2;

fprintf("moy_k_mm: %f, var_k_mm: %f, eqm_k_mm: %f\n", moyenne_k_mm, var_k_mm, eqm_k_mm);
fprintf("moy_th_mm: %f, var_th_mm: %f, eqm_th_mm: %f\n", moyenne_theta_mm, var_theta_mm, eqm_theta_mm);

theta_mv = sum(X) ./ (N * k_mm);

k = 0:0.001:20;
k_mv = 1:200;
for i = 1:200
    somme = sum(X(:,i));
    phi = (-N .* k) .* log(somme ./ (N .* k)) - (N .* log(gamma(k))) + ((k - 1) .* sum(log(X(:,i)))) - (N .* k); %% LL(theta_mv, k)
    [val_max, i_max] = max(phi);
    k_mv(i) = k(i_max);
end

moyenne_k_mv = mean(k_mv);
moyenne_theta_mv = mean(theta_mv);
var_k_mv = var(k_mv);
var_theta_mv = var(theta_mv);
eqm_k_mv = var_k_mv + (moyenne_k_mv - param_k)^2;
eqm_theta_mv = var_theta_mv + (moyenne_theta_mv - theta)^2;

fprintf("moy_k_mv: %f, var_k_mv: %f, eqm_k_mv: %f\n", moyenne_k_mv, var_k_mv, eqm_k_mv);
fprintf("moy_th_mv: %f, var_th_mv: %f, eqm_th_mv: %f\n", moyenne_theta_mv, var_theta_mv, eqm_theta_mv);
