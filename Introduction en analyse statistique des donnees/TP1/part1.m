% Q1
x1 = randn();
x2 = 3 * randn() + sqrt(4);
x3 = rand();
x4 = rand() < 0.5;

% Q2
n = 500;
g = randn(n, 1);
gmv = 2 .* randn(n, 1) + 3;
ur = rand(n, 1);
b = rand(n, 1) < 0.5;

figure(1);
subplot(2, 2, 1);
scatter(linspace(min(g), max(g), n), g);
title("Gaussienne centr�e r�duite");
subplot(2, 2, 2);
scatter(linspace(min(gmv), max(gmv), n), gmv);
title("Gaussienne avec �=3 et sigma=2");
subplot(2, 2, 3);
scatter(linspace(min(ur), max(ur), n), ur);
title("Uniformement r�partie dans [0;1]");
subplot(2, 2, 4);
scatter(linspace(min(b), max(b), n), b);
title("Binaire �quir�partie");

% Q3
figure(2);
subplot(2, 2, 1);
histogram(g, "normalization", "pdf");
hold on;
dg = linspace(-4, 4, n);
fxg = (1 / sqrt(2*pi)) .* exp((-1/2) .* (dg .^ 2));
plot(dg, fxg);
title("Histogramme de la gaussienne centr�e r�duite");

subplot(2, 2, 2);
histogram(gmv, "normalization", "pdf");
hold on;
dgmv = linspace(-4, 10, n);
fxgmv = (1 / (sqrt(2*pi) * 2)) .* exp((-1/2) .* (((dgmv - 3) / 2) .^ 2));
plot(dgmv, fxgmv);
title("Histogramme de la gaussienne avec �=3 et sigma=2");

subplot(2, 2, 3);
histogram(ur, "normalization", "pdf");
hold on;
dur = linspace(0, 1, n);
%fxur = 1 / (1 - 0);
fxur = dur <= 1;
plot(dur, fxur);
title("Histogramme de la loi uniformement r�partie dans [0;1]");

subplot(2, 2, 4);
histogram(b, "normalization", "pdf");
title("Histogramme de la loi binaire �quir�partie");

% Q4


% Q5
figure(3);
plot(X1, X2, '.');

%Q6
sx1 = sqrt(var(X1));
sx2 = sqrt(var(X2));
r = ((1 / n) * sum((X1 - mean(X1)) .* (X2 - mean(X2)))) / (sx1 * sx2);
fprintf("r = %f\n", r);
