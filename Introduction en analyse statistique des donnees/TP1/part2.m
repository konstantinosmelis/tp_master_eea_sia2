clear;
% Q1
data = xlsread("DonneesNaissances.xls");

% Q2
% h1: Les p�res fument plus (++) 10 ans apr�s la naissance qu'� la naissance de leur fille
% h2: 10 ans apres la naissance la taille et le poids augmente
% lineairemment

poids0 = data(:,2);
poids10 = data(:,4);
taille0 = data(:,1);
taille10 = data(:,3);

figure(1);
plot(poids0, taille0, '.');
sx1 = sqrt(var(poids0));
sx2 = sqrt(var(taille0));
r = ((1 / 115) * sum((poids0 - mean(poids0)) .* (taille0 - mean(taille0)))) / (sx1 * sx2);
fprintf("r = %f\n", r);

figure(2);
plot(poids10, taille10, '.');
sx1 = sqrt(var(poids10));
sx2 = sqrt(var(taille10));
r2 = ((1 / 115) * sum((poids10 - mean(poids10)) .* (taille10 - mean(taille10)))) / (sx1 * sx2);
fprintf("r2 = %f\n", r2);

figure(3);
plot(taille0, taille10, '.');
sx1 = sqrt(var(taille0));
sx2 = sqrt(var(taille10));
r3 = ((1 / 115) * sum((taille0 - mean(taille0)) .* (taille10 - mean(taille10)))) / (sx1 * sx2);
fprintf("r3 = %f\n", r3);
