clc;

%% Q a -> moyenne et variance
moy_emp = mean(x);
var_emp = var(x);
fprintf("================[Q(a)]================\n");
fprintf("Moyenne empirique: %f\n", moy_emp);
fprintf("Variance empirique: %f\n", var_emp);

%% Q b -> test de chi-2
fprintf("\n================[Q(b)]================\n");
classes = 5;
niveau_conf = 0.9;
fprintf("niveau de confiance: %.2f\n", niveau_conf);
fprintf("nombre de classes: %d\n", classes);
deg_lib = classes - 1;
fprintf("degr�s de libert�: %d\n", deg_lib);
fprintf("d'apr�s la table de chi-2 le seuil est: 7.78\n");
a = min(x);
b = max(x);
k = hist(x, classes).';
ntheo = length(x) / classes;
chi2 = sum(((k - ntheo) .^ 2) ./ ntheo);
fprintf("chi2: %f\n", chi2);
fprintf("hypoth�se accept�e? : %d\n", chi2 < 7.78);

%% Q c -> test de Kolmogorov-Smirnov
fprintf("\n================[Q(c)]================\n");
futh = [linspace(-2 , 2, 100); linspace(-2 , 2, 100)];
femp = [linspace(-2 , 2, 100); linspace(-2 , 2, 100)];
% F the
for i = 1:length(futh)
    if futh(1, i) < a
        futh(2, i) = 0;
    elseif futh(1, i) >= b
        futh(2, i) = 1;
    else
        futh(2, i) = (futh(1, i) - a) / (b - a);
    end
end
% F emp
for i = 1:length(femp)
    femp(2, i) = sum(x <= femp(1, i)) / length(x);
end
figure;
plot(futh(1, :), futh(2, :));
hold on;
plot(femp(1, :), femp(2, :));

dist_max = max(abs(femp(2, :) - futh(2, :)));
fprintf("distance max: %f\n", dist_max);
fprintf("distance max accept�e: 0.189130\n");

%% Q d -> test de Kolmogorov-Smirnov avec une loi normale
fprintf("\n================[Q(d)]================\n");
fnth = erf(femp(1, :));
figure;
plot(futh(1, :), futh(2, :));
hold on;
plot(femp(1, :), fnth);

dist_max2 = max(femp(2, :) - fnth);
fprintf("distance max: %f\n", dist_max2);
fprintf("distance max accept�e: 0.189130\n");
