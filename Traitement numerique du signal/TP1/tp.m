pkg load signal;

%% Part 1
load "fichsig.mat"

figure;
plot(sig);
xlabel("Tamps"), ylabel("Amplitude"), title("Signal sig");

Fe = 8000;
N = length(sig);
f = Fe * (0:(1/N):(1-(1/N))) - (Fe/2);
tf_sig = fft(sig);

figure;
% plot(f, 20 * log10(abs(fftshift(tf_sig))));
plot(f, abs(fftshift(tf_sig)));
xlabel("Frequence"), ylabel("Reponse frequentielle"), title("Spectre du signal");


%% Part 2 RII
fc = 610;
P = 60;
ri = 2 * (fc/Fe) * sinc(2 * fc/Fe * (-P:P));
%% Ordre = 2P+1

%figure;
%plot(-P:P, ri);

bm = blackman(2 * P + 1)';
%figure;
%plot(-P:P, ri .* bm); %% RI plus douce

[H, freq] = freqz(ri .* bm, 1);

figure;
plot(freq / pi * Fe, 20 * log10(abs(H)));
hold on;
plot([0 8000], [-1 -1]);
plot([0 8000], [-72 -72]);
plot([610 610], [0 -160]);
plot([1610 1610], [0 -160]);

figure;
plot(filter(ri .* bm, 1, sig));

P = 8000;
ri = 2 * (fc / Fe) * sinc(2 * fc / Fe * (-P:P));
[H, freq] = freqz(ri, 1);
figure;
plot(freq / pi * Fe, 20 * log10(abs(H)));
hold on;
plot([0 8000], [-1 -1]);
plot([0 8000], [-72 -72]);
plot([610 610], [0 -160]);
plot([1610 1610], [0 -160]);

figure;
plot(filter(ri, 1, sig));

figure;
plot(sig_ideal);

%% Part 3 RII
[z, p, g] = buttap(10);
[n, c] = zp2tf(z, p, g);
p = p';
[zb, za, zg] = bilinear(z, p, g, Fe);
sos = zp2sos(zb, za, zg);
sos = reshape(sos, 1, size(sos)(1) * size(sos)(2));
[H, freq] = freqz(sos, [], Fe);
figure;
plot(freq / pi * Fe, 20 * log10(abs(H)));
hold on;
plot([0 8000], [-1 -1]);
plot([0 8000], [-72 -72]);
plot([610 610], [0 -160]);
plot([1610 1610], [0 -160]);

figure;
plot(filter(ri, 1, sig));
