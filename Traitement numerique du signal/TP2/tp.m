pkg load signal;
load Multi.mat

%% Analyse du signal
Fe = 132300;
N = length(Signal);
f = Fe * (0:(1/N):(1-(1/N))) - (Fe/2);
tf_sig = fft(Signal);

figure;
% plot(f, 20 * log10(abs(fftshift(tf_sig))));
plot(f, abs(fftshift(tf_sig)));
xlabel("Frequence"), ylabel("Reponse frequentielle"), title("Spectre du signal");

%% Recuperation du signal x1
f1 = fir1(500, (12800 * 2) / Fe); %%% retard de 250
[H, freq] = freqz(f1, 1);
figure;
plot(freq / (2*pi) * Fe, 20 * log10(abs(H)));
hold on;
plot([0 14300], [-40 -40]);
plot([0 14300], [-1 -1]);
plot([12500 12500], [0 -60]);
plot([14500 14500], [0 -60]);

x1 = filter(f1, 1, Signal);
tf_x1 = fft(x1);
figure;
plot(x1);
figure;
plot(f, abs(fftshift(tf_x1)));

%% Recuperation de la porteuse
f2 = fir1(300, [(17700 * 2) / Fe, (20500 * 2) / Fe]); %%% retard de 150
[H, freq] = freqz(f2, 1);
figure;
plot(freq / (2 * pi) * Fe, 20 * log10(abs(H)));
hold on;
plot([0 23000], [-40 -40]);
plot([0 23000], [-1 -1]);
plot([16200 16200], [0 -60]);
plot([18200 18200], [0 -60]);
plot([20000 20000], [0 -60]);
plot([22000 22000], [0 -60]);

p = filter(f2, 1, Signal);
tf_p = fft(p);
figure;
plot(p);
figure;
plot(f, abs(fftshift(tf_p)));

dp = 2 * p.^2 - 1;
figure;
plot(dp);

%% Recuperation de x2
f3 = fir1(300, [(26150 * 2) / Fe, (49760 * 2) / Fe]); %%% retard de 150
[H, freq] = freqz(f3, 1);
figure;
plot(freq / (2 * pi) * Fe, 20 * log10(abs(H)));
hold on;
plot([10000 53000], [-40 -40]);
plot([10000 53000], [-1 -1]);
plot([24550 24550], [0 -60]);
plot([26550 26550], [0 -60]);
plot([49260 49260], [0 -60]);
plot([51260 51260], [0 -60]);

x2m = filter(f3, 1, Signal);
tf_x2m = fft(x2m);
figure;
plot(x2m);
figure;
plot(f, abs(fftshift(tf_x2m)));

f4 = fir1(300, (12700 * 2) / Fe); %%% retard de 150
[H, freq] = freqz(f4, 1);
figure;
plot(freq / (2*pi) * Fe, 20 * log10(abs(H)));
hold on;
plot([0 14200], [-40 -40]);
plot([0 14200], [-1 -1]);
plot([12200 12200], [0 -60]);
plot([14200 14200], [0 -60]);

v = 2 * x2m .* dp;
figure;
plot(f, abs(fftshift(fft(v))));

x2 = filter(f4, 1, );
tf_x2 = fft(x2);
figure;
plot(x2);
figure;
plot(f, abs(fftshift(tf_x2)));

%% Recuperation des signaux g et d
x2 = circshift(x2, [1, -50]);
g = (x1 + x2) / 2;
d = x1 - g;
figure;
plot(g);
figure;
plot(d);
