function Grad_K = Grad_Crit_K(theta, D)
% Function Grad_Crit_K
%   Grad_K = Grad_Crit_K(theta,D)
%
% Calcule le gradient du critère à minimiser pour estimer au sens du
% maximum de vraisemblance les paramètres theta de l'étoile
%
% Paramètres d'entrée:
%   theta : vecteur des paramètres (vecteur colonne)
%           theta = [p_1; p_2; l_0; c_0; beta_1; beta_2];
%   D : image (L lignes et C colonnes)
%
% Paramètres de sortie :
%   Grad_K : valeur du gradient
%            (vecteur colonne de même dimension que theta)

% H. Carfantan, IRAP, Dec. 2023

    % Parameters of the model
    p_1 = theta(1); p_2 = theta(2);
    l_0 = theta(3); c_0 = theta(4);
    beta_1 = theta(5); beta_2 = theta(6);

    % Temporary variables
    [L, C] = size(D);
    [Cols, Ligs] = meshgrid(1:C, 1:L);
    % J = sum(sum(Res.^2))
    % Res = Y - g;
    % g = p_1*h^(-beta_2) + p_2;
    % h = 1 + rho2/beta_1^2;
    % rho2 = (L - l_0).^2 + (C - c_0).^2;
    rho2 = (Ligs - l_0).^2 + (Cols - c_0).^2;
    h = (1 + rho2/beta_1^2 );
    hbeta = h .^ (-beta_2);
    g =  p_1 * hbeta + p_2;
    Res = (D - g);
    hbetaRes = hbeta .* Res;

    % Comptation of the gradient
    Grad_K = zeros(size(theta));
    % derivative of J with respect to p_1
    Grad_K(1) = sum(sum(-2 * hbetaRes));
    % derivative of J with respect to p_2
    Grad_K(2) = sum(sum(-2*Res));
    % derivative of J with respect to l_0
    GradTemp = -4 * beta_2 * p_1/beta_1^2 * hbetaRes./h;
    Grad_K(3) = sum(sum((Ligs - l_0) .* GradTemp));
    % derivative of J with respect to c_0
    Grad_K(4) = sum(sum((Cols - c_0) .* GradTemp));
    % derivative of J with respect to beta_1
    Grad_K(5) = sum(sum(rho2 .* GradTemp / beta_1));
    % derivative of J with respect to beta_2
    Grad_K(6) = sum(sum(2 * p_1 * log(h) .* hbetaRes));
end

