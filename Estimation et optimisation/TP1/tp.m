close all, clear, clc;

load("data.mat");

%% Modelisation du probleme
figure, imagesc(D);
colorbar, colormap gray;

d = D(:);
psf = PSF(:);
p = theta_th(1:2);

%% Estimation a parametres de disposition de PSF connus
% q5
R = [psf ones(length(psf), 1)];
Q = sigma_b * eye(length(R));

p_est_mc = inv(R' * inv(Q) * R) * R' * inv(Q) * d;

% q6
diff_p = abs(p - p_est_mc);
diff_var_p = abs(var(p) - var(p_est_mc));

% q7
res = d - (p_est_mc(1) * psf + p_est_mc(2));

figure, hist(res);
title("Densite de probabilite empirique du residu");

%% Estimation par maximum de vraisemblance de tous les parametres
% Etude de la fonction banana
x = -2:0.01:2;
y = -1:0.01:3;
b = zeros(length(y), length(x));
grad_b = zeros([length(y) length(x) 2]);
for k = 1:length(x)
    for l = 1:length(y)
        b(l, k) = banana([x(k), y(l)]);
        grad_b(l, k, :) = grad_banana([x(k), y(l)]);
    end
end

figure;
contour(x, y, b, 1:4);
hold on, scatter([-1 1], [1 1], '*'), hold off;
title("Foncton banana");

% Algorithme de descente et minimisation de la fonction banana
FUN = @(theta) banana(theta);
GRADFUN = @(theta) grad_banana(theta);

% q2
% pour P_init = [0; 0]
[P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [0; 0], 1, [1500, 0.0001, 0.001], [1 0.0001 1]); % 123 sec [.959 .921]
% [P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [0; 0], 1, [1500, 0.0001, 0.001], [2 5 1]); % 350 sec [.963 .927]

% pour P_init = [5; 5]
% [P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [5; 5], 1, [1500, 0.0001, 0.001], [1 0.0001 1]); % ne converge pas
% [P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [5; 5], 1, [1500, 0.0001, 0.001], [2 5 1]); % ne converge pas

% q3
figure, contour(x, y, b, 1:4);
hold on, scatter(ALL_P(1, :), ALL_P(2, :), '+'), hold off;
title("Fonction banana avec le trajet de minimisation");
legend("Fonction banana", "Trajet");

% q4
% gradients avec correction de Vigne
% [P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [0; 0], 2, [1500, 0.00001, 0.001], [1 0.0001 1]); % 60 sec
[P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [0; 0], 2, [1500, 0.00001, 0.001], [2 5 1]); % 73 sec [1.0003, 1.0005]

figure, contour(x, y, b, 1:4);
hold on, scatter(ALL_P(1, :), ALL_P(2, :), '+'), hold off;
title("Fonction banana avec le trajet de minimisation");
legend("Fonction banana", "Trajet");

% gradients conjugues
[P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [5; 5], 3, [1500, 0.00001, 0.001], [1 0.0001 1]); % 6 sec [.9994, .9988]
% [P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, [5; 5], 3, [1500, 0.00001, 0.001], [2 5 1]); % 24 sec [.9999, .9998]

figure, contour(x, y, b, 1:4);
hold on, scatter(ALL_P(1, :), ALL_P(2, :), '+'), hold off;
title("Fonction banana avec le trajet de minimisation");
legend("Fonction banana", "Trajet");

% Minimisation de la fonction Crit-K
FUN = @(theta) Crit_K(theta, D);
GRADFUN = @(theta) Grad_Crit_K(theta, D);

% q3
l0 = floor(length(D) / 2);
c0 = floor(length(D(1, :)) / 2);
p1 = max(d) / 2;
p2 = min(d);
th_ini = [p1; p2; l0; c0; 2; 2];
[K, modele] = Crit_K(th_ini, D);

figure, imagesc(modele);
colorbar, colormap gray;
title("Modele genere");

figure, imagesc(abs(D - modele));
colorbar, colormap gray;
title("Residu du modele");

% q4
[P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, th_ini, 3, [5000, 1e-5, 1e-3], [2 10 1]);

diff_theta_th_P = theta_th - P;

% q6
C = size(D, 1);
L = size(D, 2);
[Col,Lig] = meshgrid(1:C, 1:L);
mod_est = P(1) * Moffat(P(3:end), Lig, Col) + P(2);

figure, imagesc(mod_est);
colorbar, colormap gray;

% q7
res = D - mod_est;

figure, hist(res);
title("Densite de probabilite empirique du residu");

