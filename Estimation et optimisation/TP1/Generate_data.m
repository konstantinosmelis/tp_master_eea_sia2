% Script Matlab pour générer les données correspondant à l'observation d'une étoile
% pour le TP d'estimation et optimisation
% Master 2 SIA2, UPS, 2023-2024

% Paramètres de l'image :
L = 20;
C = 20;
sigma_b = 20;

% Paramètres de l'étoile :
p_1 = 700;
p_2 = 400;
l_0 = 8;
c_0 = 12;
beta_1 = 4;
beta_2 = 5;

% Génération des données :
[Col,Lig] = meshgrid(1:C, 1:L);
gamma = [l_0; c_0; beta_1; beta_2];
PSF = Moffat(gamma, Lig, Col);
Modele = p_1 * PSF + p_2;
D = Modele + sigma_b * randn(L, C);
imagesc(D); colorbar;
colormap gray;

% Sauvegarde des données
theta_th = [p_1; p_2; l_0; c_0; beta_1; beta_2];
save data.mat D theta_th PSF sigma_b;

