function f = banana(theta);
    f = (theta(1) - 1) .^ 2 + 100 * (theta(1) .^ 2 - theta(2)) .^ 2;
end

