function grad_f = grad_banana(theta);
  grad_f = [2 * (theta(1) - 1) + 400 * theta(1) .* (theta(1) .^ 2 - theta(2)); -200 * (theta(1) .^ 2 - theta(2))];
end

