function [K, Modele] = Crit_K(theta, D)
% Calcule le critère à minimiser pour estimer au sens du maximum
% de vraisemblance les paramètres theta de l'étoile
% Paramètres d'entrée:
%   theta : vecteur des paramètres (vecteur colonne)
%           theta = [p_1; p_2; l_0; c_0; beta_1; beta_2];
%   D : image (L lignes et C colonnes)
%
% Paramètres de sortie :
%   K : valeur du critère
%   Modele : modèle de l'image de l'étoile pour les paramètres theta

    % Calcul du modèle
    [L, C] = size(D);
    [Col, Lig] = meshgrid(1:C, 1:L);
    PSF = Moffat(theta(3:end), Lig, Col);
    Modele = theta(1) * PSF + theta(2);

    % Calcul du critère
    d = D(:);
    p = theta(1:2);
    psf = PSF(:);
    R = [psf ones(length(psf), 1)];
    K = (d - R * p)' * (d - R * p);
end
