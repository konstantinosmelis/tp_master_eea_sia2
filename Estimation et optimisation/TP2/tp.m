close all, clear all, clc;

load("data.mat");

%% Estimation bayesienne
% Estimation de la moyenne a posteriori
d = D(:);

% q2
N = 3000;

thetas = zeros(6, N + 1);
thetas(:, 1) = [max(d); max(d)/2; length(D); length(D(1, :)); 500; 500] / 2;

epsilon = .003;
lambdas = epsilon * [max(d); max(d) / 2; 2 * length(D) / 4; 2 * length(D(1, :)) / 4; 500; 500 - 1];
Lambda = diag(lambdas .^ 2);

Q = @(theta) (sqrt(Lambda) * randn(length(theta), 1) + theta);
acc = 0;
for n = 2:N+1
    theta = thetas(:, n - 1);
    theta_pr = Q(theta);

    if theta_pr(1) < 0 || theta_pr(1) > max(d) || theta_pr(2) < 0 || theta_pr(2) > max(d) / 2 || theta_pr(3) < length(D) / 4 || theta_pr(3) > 3 * length(D) / 4 || theta_pr(4) < length(D(1, :)) / 4 || theta_pr(4) > 3 * length(D(1, :)) / 4 || theta_pr(5) < 0 || theta_pr(5) > 500 || theta_pr(6) < 1 || theta_pr(6) > 500
        thetas(:, n) = theta;
        continue;
    end

    if log(rand()) < (- 1 / (2 * sigma_b ^ 2)) * (Crit_K(theta_pr, D) - Crit_K(theta, D))
        thetas(:, n) = theta_pr;
        acc = acc + 1;
    else
        thetas(:, n) = theta;
    end
end

fprintf("Taux d'acceptation: %2f\n", acc * 100 / N);

% q3
labels = ["p_1"; "p_2"; "l_0"; "c_0"; "beta_1"; "beta_2"];
figure;
for m = 1:6
    subplot(2, 3, m);
    plot(thetas(m, :));
    title(sprintf("Variation de %s", labels(m, :)))
end

% q4
No = 1500;
theta_est = mean(thetas(:, (No+1):end), 2);

% q6
[Col, Lig] = meshgrid(1:length(D(1, :)), 1:length(D));
PSF = Moffat(theta_est(3:end), Lig, Col);
model_est = theta_est(1) * PSF + theta_est(2);

figure, imagesc(model_est);
colormap gray, colorbar;
title("Image de l'etoile estimee");

% q7
res = D - model_est;

figure, hist(res);
title("Densite de probabilite empirique du residu");

% q8
ecart_type = std(thetas(:, (No+1):end), 0, 2);

% q9
figure;
for m = 1:6
    subplot(2, 3, m);
    hist(thetas(m, (No+1):end), 10);
    title(sprintf("Densite de probabilite du parametre %s", labels(m, :)));
end

