function PSF = Moffat(gamma, Lig, Col)
% Function MOFFAT
% PSF =  PSF = Moffat(gamma,Lig,Col)
%
% Generate a PSF according to the Moffat model
%     PSF(l,c) = (1 + ((l-l_0)^2 + (c-c_0)^2)/beta_1^2)^(-beta_2)
%
% Input :
% - gamma, list of parameters gamma = [l_0, c_0, beta_1, beta_2]
%   with (l_0,c_0) location of the center of the PSF
%   beta_1 scale parameter
%   beta_2 shape parameter
% - Lig and Col : row-wise and column-wise grids
%   Typically [Lig,Row] = meshgrid(1:C,1:L);

    l_0 = gamma(1);
    c_0 = gamma(2);
    beta_1 = gamma(3);
    beta_2 = gamma(4);
    PSF = (1 + ((Lig - l_0) .^ 2 + (Col - c_0) .^ 2) / beta_1 ^ 2 ) .^ (-beta_2);
end
