close all, clear all, clc;

load("TomographieX_2024small.mat");

%% Presentation du TP
L = 40;
C = 40;

figure, imagesc(reshape(x, L, C));
title("Image theorique");
colorbar, colormap gray;

% q2
y_th = H * x;

Py = mean(y_th.^2);
sigma_b = sqrt(Py / (10^(25/10)));
y = y_th + randn(length(y_th), 1) * sigma_b;

figure, imagesc(reshape(-y_th, [], 90));
title("Donnees non bruitees");
colorbar, colormap gray;

figure, imagesc(reshape(-y, [], 90));
title("Donnees bruitee");
colorbar, colormap gray;

%% Resolution par retroprojection
x_rp = (1 / (L * C)) * H' * y_th;
x_rp_b = (1 / (L * C)) * H' * y;

figure, imagesc(reshape(x_rp, L, C));
title("x_{retroprojection} a partir des donnees theoriques");
colorbar, colormap gray;

figure, imagesc(reshape(x_rp_b, L, C));
title("x_{retroprojection} a partir des donnees bruitees");
colorbar, colormap gray;

L_1_rp = sum(abs(x - x_rp));
L_1b_rp = sum(abs(x - x_rp_b));

L_2_rp = sum((x - x_rp).^2);
L_2b_rp = sum((x - x_rp_b).^2);

fprintf("Retroprojection\n");
fprintf("Donnees theoriques => L1=%f, L2=%f\n", L_1_rp, L_2_rp);
fprintf("Donnees bruitees => L1=%f, L2=%f\n", L_1b_rp, L_2b_rp);

%% Reslution par moindres carres
x_mc = inv(H' * H) * H' * y_th;
Q = sigma_b^2 * eye(length(H));
x_mc_b = inv(H' * inv(Q) * H) * H' * inv(Q) * y;

figure, imagesc(reshape(x_mc, L, C));
title("x_{MC} a partir des donnees theoriques");
colorbar, colormap gray;

figure, imagesc(reshape(x_mc_b, L, C));
title("x_{MC} a partir des donnees bruitees");
colorbar, colormap gray;

L_1_mc = sum(abs(x - x_mc));
L_1b_mc = sum(abs(x - x_mc_b));

L_2_mc = sum((x - x_mc).^2);
L_2b_mc = sum((x - x_mc_b).^2);

fprintf("\nMoindres carres\n");
fprintf("Donnees theoriques => L1=%f, L2=%f\n", L_1_mc, L_2_mc);
fprintf("Donnees bruitees => L1=%f, L2=%f\n", L_1b_mc, L_2b_mc);

%% Resolution par decomposition tronquee de valeurs singulieres
% q2
nb_cond = cond(full(H));
fprintf("Nombre de conditions de H: "), display(nb_cond);

% q3-4
[U, S, V] = svd(full(H));
S = diag(S);

L_1_tsvd = zeros(1500, 1);
L_1b_tsvd = zeros(1500, 1);
L_2_tsvd = zeros(1500, 1);
L_2b_tsvd = zeros(1500, 1);

x_tsvd = zeros(1500, L * C);
x_tsvd_b = zeros(1500, L * C);

for K = 1:1500
    val_x_tsvd = 0;
    val_x_tsvd_b = 0;

    for k = 1:K
        val_x_tsvd = val_x_tsvd + (U(:, k)' * y_th * V(:, k)) / S(k);
        val_x_tsvd_b = val_x_tsvd_b + (U(:, k)' * y * V(:, k)) / S(k);
    end

    x_tsvd(K, :) = val_x_tsvd';
    x_tsvd_b(K, :) = val_x_tsvd_b';

    L_1_tsvd(K) = sum(abs(x - val_x_tsvd));
    L_1b_tsvd(K) = sum(abs(x - val_x_tsvd_b));
    L_2_tsvd(K) = sum((x - val_x_tsvd).^2);
    L_2b_tsvd(K) = sum((x - val_x_tsvd_b).^2);
end

figure, plot(L_1_tsvd);
hold on;
plot(L_1b_tsvd), plot(L_2_tsvd), plot(L_2b_tsvd);
hold off;
title("Variation des erreurs en fonction de K");
legend("L_1", "L_1 bruit", "L_2", "L_2 bruit");

fprintf("\nTSVD\n");
[~, T] = min(L_1_tsvd);
figure, imagesc(reshape(x_tsvd(T, :), L, C));
title(sprintf("x_{TSVD} a partir des donnees theoriques avec K=%d", T));
colorbar, colormap gray;
fprintf("Donnees theoriques => L1=%f, L2=%f\n", L_1_tsvd(T), L_2_tsvd(T));

[~, T] = min(L_1b_tsvd);
figure, imagesc(reshape(x_tsvd_b(T, :), L, C));
title(sprintf("x_{TSVD} a partir des donnees bruitees avec K=%d", T));
colorbar, colormap gray;
fprintf("Donnees bruitees => L1=%f, L2=%f\n", L_1b_tsvd(T), L_2b_tsvd(T));

%% Resolution par optimisation de criteres
%% Penalisation de l'energie
% q2
alphas = [.01 .1 1 10 100 1000];

L_1_ptt = zeros(length(alphas), 1);
L_1b_ptt = zeros(length(alphas), 1);
L_2_ptt = zeros(length(alphas), 1);
L_2b_ptt = zeros(length(alphas), 1);

K = 1;
for alpha = alphas
    x_ptt(:, K) = inv(H' * H + alpha * eye(L * C)) * H' * y_th;
    x_ptt_b(:, K) = inv(H' * H + alpha * eye(L * C)) * H' * y;

    L_1_ptt(K) = sum(abs(x - x_ptt(:, K)));
    L_1b_ptt(K) = sum(abs(x - x_ptt_b(:, K)));
    L_2_ptt(K) = sum((x - x_ptt(:, K)).^2);
    L_2b_ptt(K) = sum((x - x_ptt_b(:, K)).^2);
    K = K + 1;
end

figure, plot(L_1_ptt);
hold on;
plot(L_1b_ptt), plot(L_2_ptt), plot(L_2b_ptt);
hold off;
title("Variation des erreurs en fonction de alpha");
legend("L_1", "L_1 bruit", "L_2", "L_2 bruit");

fprintf("\nPenalisation de l'energie\n");
[~, T] = min(L_1_ptt);
figure, imagesc(reshape(x_ptt(:, T), L, C));
title("x_{penalisation de l'energie} a partir donnees theoriques");
colorbar, colormap gray;
fprintf("Donnees theoriques => L1=%f, L2=%f\n", L_1_ptt(T), L_2_ptt(T));

[~, T] = min(L_1b_ptt);
figure, imagesc(reshape(x_ptt_b(:, T), L, C));
title("x_{penalisation de l'energie} a partir des donnees bruitees");
colorbar, colormap gray;
fprintf("Donnees bruitees => L1=%f, L2=%f\n", L_1b_ptt(T), L_2b_ptt(T));

%% Douceur spatiale
% q2
L_1_ptt = zeros(length(alphas), 1);
L_1b_ptt = zeros(length(alphas), 1);
L_2_ptt = zeros(length(alphas), 1);
L_2b_ptt = zeros(length(alphas), 1);

K = 1;
for alpha = alphas
    x_ptt(:, K) = inv(H' * H + alpha * D' * D) * H' * y_th;
    x_ptt_b(:, K) = inv(H' * H + alpha * D' * D) * H' * y;

    L_1_ptt(K) = sum(abs(x - x_ptt(:, K)));
    L_1b_ptt(K) = sum(abs(x - x_ptt_b(:, K)));
    L_2_ptt(K) = sum((x - x_ptt(:, K)).^2);
    L_2b_ptt(K) = sum((x - x_ptt_b(:, K)).^2);
    K = K + 1;
end

figure, plot(L_1_ptt);
hold on;
plot(L_1b_ptt), plot(L_2_ptt), plot(L_2b_ptt);
hold off;
title("Variation des erreurs en fonction de alpha");
legend("L_1", "L_1 bruit", "L_2", "L_2 bruit");

fprintf("\nDouceur Spatiale\n");
[~, T] = min(L_1_ptt);
figure, imagesc(reshape(x_ptt(:, T), L, C));
title("x_{douceur spatiale} a partir donnees theoriques");
colorbar, colormap gray;
fprintf("Donnees theoriques => L1=%f, L2=%f\n", L_1_ptt(T), L_2_ptt(T));

[~, T] = min(L_1b_ptt);
figure, imagesc(reshape(x_ptt_b(:, T), L, C));
title("x_{douceur spatiale} a partir des donnees bruitees");
colorbar, colormap gray;
fprintf("Donnees bruitees => L1=%f, L2=%f\n", L_1b_ptt(T), L_2b_ptt(T));

%% Preservation des discontinuites
% q3
phi = 1;
%alpha = 2;
%p = 1.1;

alpha = 3;
p = 1.2;

FUN = @(x) crit(x, y, H, alpha, phi, p);
GRADFUN = @(x) dcrit(x, y, H, alpha, phi, p);

[P, FVAL, COMP_COST, ALL_P, ALL_J, EXITFLAG] = minimize(FUN, GRADFUN, zeros(L * C, 1), 3, [1500, 1e-5, 1e-3], [2 10 0]);

figure, imagesc(reshape(P, L, C));
title("x_{preservation des discontinuites} a partir des donnees bruitees");
colorbar, colormap gray;

L_1b = sum(abs(x - P));
L_2b = sum((x - P).^2);

fprintf("\nPreservation des discontinuites\n");
fprintf("L1=%f, L2=%f\n", L_1b, L_2b);

