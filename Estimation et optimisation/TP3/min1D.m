function [lambda, P, fP, nf] = min1D(fun,P_k,D_k,option,lambda);
%  [lambda, P, fP, nf] = min1D(FUN,P,D,OPTION1D,lambda0);
% 
%  Fonction min1D : minimisation unidimensionnelle
%  Recherche d'un lambda positif tel que
%                lambda = min_lambda FUN(P + lambda D)
 
% Fonction min1D : minimisation unidimensionnelle
%
% [lambda, P, fP, nf] = min1D(FUN,P,D,OPTION1D,lambda0);
%
% Recherche d'un lambda positif tel que
%	                lambda = min_lambda FUN(P + lambda D)
% 
% Paramètres d'entrée
% -------------------
%    FUN       : Nom de la fonction (chaine de caractère)
%    P         : Point courant
%    D         : Direction de descente
%    OPTION1D  : vecteur d'options (optionnel, voir plus loin)
%    lambda0   : valeur initiale pour lambda (optionnel)
%
% Paramètres de sortie
% --------------------
%    lambda    : Pas optimal (à la précision voulue)
%    P         : Point à l'optimum (optionnel)
%    fP        : Valeur de la fonction à l'optimum (optionnel)
%    nf        : Nombre d'evaluations de la fonction fun (optionnel)
%
% Vecteur OPTION1D
% ----------------
% Pour une recherche dichotomique (par défaut)
%    OPTION1D(1) : 1
%    OPTION1D(2) : Précision sur les paramètres (1e-3 par defaut)
% Pour une recherche parabolique
%    OPTION1D(1) : 2
%    OPTION1D(2) : Nombre d'iterations (10 par défaut)
% Dans les deux cas
%    OPTION1D(3) : 1 pour un affichage graphique de la minimisation 1D 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Master 2 EEA parcours SIA-AMS                       Université Toulouse 3 %
% Signal Imagerie et Applications                     Paul Sabatier         %
% 2019-2020                                                                 %
%                TP Estimation et optimisation                              %
%                                                                           %
% H. Carfantan, janvier 2020                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Traitement des arguments
  if (nargin<5)            % min1D(fun,P_k,D_k,option) ou min1D(fun,P_k,D_k)
     if (nargin<4)         % min1D(fun,P_k,D_k)
        option =[1, 1e-3, 0];
     end
     lambda = .1;
  end
  if length(option)<2       
     if option(1)==1        % Dichotomie
        option(2) = 1e-3;
     elseif option(1)==2    % Interpolation parabolique
        option(2) = 10;
     end
  end
  if length(option)<3
     option(3) = 0;   
  end

% Nombre d'evaluations de la fonction
  nf = 0;
% Pas pour l'approximation de la derivee
  delta = 1e-6;
% lambda minimum pour le bracketing
  lambdamin = 1e-8;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Bracketing %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recherche A, Beta et B tel que et f(A) > f(Beta) < f(B)

% Initialisation de A et Beta
  A = P_k; fA = feval(fun,A); lA = 0; nf=nf+1;
  Beta = P_k+lambda*D_k; fBeta = feval(fun,Beta); lBeta = lambda; nf=nf+1;

% On suppose que l'on peut descendre dans la direction donnee : lambda > 0 !
if (fBeta > fA)        % On remonte => on a B et on recherche Beta
   B = Beta; fB = fBeta; lB = lBeta;
   while ((fBeta >= fA)|(fBeta >= fB))&(lambda>lambdamin)
      lambda = lambda/2;
      Beta = P_k+lambda*D_k; fBeta = feval(fun,Beta); lBeta = lambda; nf=nf+1;
		if (fBeta > fA)&(fBeta < fB)
			B = Beta; fB = fBeta; lB = lBeta;
      end
   end
else                                    % On descend => on a Beta on cherche B
   B = Beta; fB = fBeta; lB = lBeta;
   while (fB <= fBeta)&(lambda>lambdamin)
      lambda = lambda*2.5;
      B = P_k+lambda*D_k; fB = feval(fun,B); lB = lambda; nf=nf+1;
		if (fB < fBeta)
			Beta = B; fBeta=fB; lBeta = lB;
      end
   end
end

if option(1)==1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Par dichotomie %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if option(3)
   fprintf('Minimisation 1D par dichotomie\n');
   fprintf('Précision requise : %.3e\n',option(2));
   plot([lA, lBeta, lB],[fA,fBeta,fB],'*'); hold on
end

it=0;
while (norm(B-A)>option(2))
    % Approximation de la derivee en Beta
      dfBeta = (feval(fun,Beta+delta*D_k)-fBeta)/delta; nf = nf+1;
      if (dfBeta>0)                     % Derivee positive : A <- A, B <- Beta
         B = Beta; lB = lBeta;
         lBeta = (lA+lB)/2; Beta =  P_k + lBeta*D_k; 
         fBeta = feval(fun,Beta); nf=nf+1;
      else                              % Derivee négative : A <- Beta, B <- B
         A = Beta; lA = lBeta;
         lBeta = (lA+lB)/2; Beta =  P_k + lBeta*D_k; 
         fBeta = feval(fun,Beta); nf=nf+1;
      end
      if option(3)
         plot(lBeta,fBeta,'*g'); 
      end
end

elseif option(1)==2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Par Interpolation parabolique %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recherche de lambda_k dans [A,B] tel que 
%      f(P_k+lambda_k*D_k) = min_lambda f(P_k+lambda*D_k)
% On dispose de Beta dans [A,B] tel que f(A) > f(Beta) < f(B)
% et de lA, lB et lBeta tels que 
% A = P_k + lA*D_k; B = P_k + lB*D_k; Beta = P_k + lBeta*D_k; 
% On procède par interpolation parabolique en s'arrangeant pour avoir 
% toujours la configuration ci dessus
if option(3)
   fprintf('Minimisation 1D par interpolation parabolique\n');
   fprintf('Nombre maximum d''itérations : %d\n',option(2));
   lbdlist = linspace(lA,lB,20);
   plot([lA, lBeta, lB],[fA,fBeta,fB],'*'); hold on
end

it=0;
parab = 1;
while (it<option(2))&(abs(lB-lA)>10*eps)
      it = it+1;
      if parab                   % Interpolation parabolique
         num = (lBeta-lA)^2*(fBeta-fB) - (lBeta-lB)^2*(fBeta-fA);
         den = (lBeta-lA)*(fBeta-fB) - (lBeta-lB)*(fBeta-fA);
         if abs(den)<10*eps,        % Test de colinéarité
            lambda = lBeta + 10*eps;
         else, 
            lambda = lBeta - num/den/2;
         end
      end
      if option(3);fprintf('%d ',parab);end
      parab = 1;
      Gamma = P_k + lambda*D_k; fGamma = feval(fun,Gamma); lGamma = lambda; nf=nf+1;
      if option(3)               % Calcul et affichage de la parabole
         pol=vander([lA lB lBeta])\[fA;fB;fBeta];   
         plot(lbdlist,polyval(pol,lbdlist),'g')
         plot(lGamma,fGamma,'*g')
      end
      if (lGamma>lBeta)				% Gamma dans [Beta,B]
         if (fGamma<fBeta)
            A = Beta; fA = fBeta; lA = lBeta;
            Beta = Gamma; fBeta = fGamma; lBeta = lGamma;
            if option(3), fprintf('A<-Beta,  Beta<-Gamma, B<-B,    ');end
         else 
          % parab = 0; lambda = lBeta - .9*(lGamma - lBeta);
          % if option(3), fprintf('A<-A,       Beta<-Beta,  B<-B,  ');end
            B = Gamma; fB = fGamma; lB = lGamma;
            if option(3), fprintf('A<-A,     Beta<-Beta,  B<-Gamma,');end
         end
      else				            % Gamma dans [A,Beta]
         if (fGamma<fBeta)
            B = Beta; fB = fBeta; lB = lBeta;
            Beta = Gamma; fBeta = fGamma; lBeta = lGamma;
            if option(3), fprintf('A<-A,     Beta<-Gamma, B<-Beta, ');end
         else
          %  parab = 0; lambda = lBeta + .9*(lBeta - lGamma);
          %  if option(3), fprintf('A<-A,       Beta<-Beta,  B<-B,  ');end
             A = Gamma; fA = fGamma; lA = lGamma;
             if option(3), fprintf('A<-Gamma, Beta<-Beta,  B<-B,    ');end
         end
      end
      if option(3), fprintf(' %.3e dans [%.3e,%.3e]\n',lBeta,lA,lB);end
end



end
if option(3); 
   hold off;
   fprintf('Nombre d''évaluation de la fonction : %d\n',nf);
end

% Parametres de sortie
  lambda = lBeta;
  P = Beta;
  fP = fBeta;
