function [P,FVAL,COMP_COST,ALL_P,ALL_J,EXITFLAG] = minimize(FUN,GRADFUN,P_INIT,DIRECTION,STOP,OPTION1D); 
%  Fonction minimize
%      [P,FVAL,COMP_COST,ALL_P,ALL_J,EXITFLAG]
%                    = minimize(FUN,GRADFUN,P_INIT,DIRECTION,STOP,OPTION1D); 
%                    
%  Minimisation d'un critère par un algorithme de minimisation de type 
%  algorithme de descente exploitant le gradient du critère.
%  La recherche unidimensionnelle est effectuée par la fonction min1D.  
% 
%  Paramètres d'entrée :
%  --------------------
%    FUN        : fonction calculant le critère à minimiser 
%                 (nom de la fonction ou pointeur vers la fonction)
%    GRADFUN    : fonction calculant le gradient du critère à minimiser
%    P_INIT     : valeur initiale des paramètres
%    DIRECTION  : choix de la direction de descente
%        1 - gradient
%        2 - gradients avec correction de Vigne
%        3 - gradients conjugués
%    STOP       : paramètres du test d'arrêt STOP = [ K_MAX, EPS_P, EPS_J ]     
%        K_MAX  : nombre maximal d'itération
%        EPS_P  : seuil d'arrêt sur les paramètres
%        EPS_J  : seuil d'arrêt sur le critère
%    OPTION1D   : vecteur d'option de la fonction min1D
% 
%  Paramètres de sortie : 
%  ---------------------
%    P         : valeur des paramètres optimaux
%    FVAL      : valeur du critère en les paramètres optimaux
%    COMP_COST : Coût de calcul de l'algorithme 
%                COMP_COST = [ K, T0, NB_J, NB_G ] avec :
%        K     : nombre d'itérations
%        T0    : temps de calcul CPU
%        NB_J  :  nombre d'appel à la fonction FUN
%        NB _G : nombre d'appel à la fonction GRADFUN
%    ALL_P     : ensemble des paramètres calculés au cours des itérations;
%    ALL_J     : ensemble des valeurs du critère calculés au cours des itérations;
%    EXITFLAG  : résultat des tests d'arrêt 
%          EXITFLAG = [ FLAG_K_MAX, FLAG_EPS_P, FLAG_EPS_J ]  
%          Les flags des tests d'arrêt activé sont à 1 (les autres à 0)

% H. Carfantan, IRAP, janvier 2020

% Initialisations
%%%%%%%%%%%%%%%%%
  T0 = cputime;             % Temps de calcul
  Pk = P_INIT;              % Valeur initiale des paramètres
  JPk  = feval(FUN,Pk);     % Valeur du critère en Pk 
  NB_J   = 1;               % Nombre de fois ou le critère est calculé
  Gk   = feval(GRADFUN,Pk); % Gradient en Pk
  NB_G   =  1;              % Nombre de fois où le gradient est calculé
  % Pour le test d'arrêt
  K_MAX = STOP(1); EPS_P = STOP(2); EPS_J = STOP(3);
  PkOld  = 100*(abs(Pk)+1);     % Valeur des paramètres à l'itération précédente
  JPkOld = 100*(abs(JPk)+1);    % Valeur du critère à l'itération précédente
  if nargout>3  % Stockage de l'historique dess paramètres et du critère 
    ALL_P = zeros(length(Pk),K_MAX+1);   % Stokage des paramètres
    ALL_P(:,1) = Pk; 
    ALL_J = zeros(1,K_MAX+1);     % Stockage du critère
    ALL_J(1) = JPk;
  end
  K=0;                           % Nombre d'itérations

% Iterations
%%%%%%%%%%%%
  % Test d'arret
  %-------------
  % Arrêt lorsque le nombre maximal kmax d'itération est atteint 
  % ou quand la solution évolue peu ||PkOld-Pk||< eps_P 
  % et le critère évolue peu J(PkOld) -J(Pk) < eps_J
  while ( K<K_MAX ) && ( ( norm(PkOld-Pk)> EPS_P )||( abs(JPkOld-JPk)> EPS_J ) ) 
  K = K+1;

  % Stockage des anciens paramètres
  %--------------------------------
    PkOld  = Pk;
    JPkOld = JPk;

  % Calcul de la direction de descente
  %-----------------------------------
    Dk = - Gk ; % Direction de descente opposée au gradient                
    if (DIRECTION==2)&&(K>1)     % Algorithme des gradient avec correction de Vigne
       if (Dk' *DkOld<0)         % Teste si l'angle est supérieur à 90° : correction de Vigne
          Dk = (Dk/norm(Dk) + DkOld/norm(DkOld))/2;
       end
    elseif (DIRECTION==3)&&(K>1) % Algorithme des gradient conjugués
            Dk = - Gk + (Gk'*Gk)/(GkOld'*GkOld)*DkOld;            % Fletcher Reeves
    end
    % Stockage des anciennes directions de descentes et gradients
    GkOld  = Gk;
    DkOld  = Dk;

  % Minimisation 1D
  %----------------
    [lambda, Pk, JPk, nJ1D] = min1D(FUN,Pk,Dk,OPTION1D); 
    NB_J = NB_J + nJ1D;     % Nombre de fois où le critère est calculé
    Gk = feval(GRADFUN,Pk); % Gradient en Pk
    NB_G =  NB_G + 1;       % Nombre de fois où le gradient est calculé
    if nargout>3  % Stockage de l'historique dess paramètres et du critère 
        ALL_P(:,K+1) = Pk;        % Stockage des valeurs des paramètres 
        ALL_J(K+1) = JPk;        % Stockage des valeurs du critère
    end
  end                       % Fin des itérations
  P = Pk;
  FVAL = JPk;

% Affichage de la convergence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  T0 = cputime-T0 ;
  if K==K_MAX
     fprintf('L''algorithme n''a pas convergé en %d itérations (%.2f secondes)\n',K_MAX,T0);
  else
     fprintf('L''algorithme a convergé en %d itérations (%.2e secondes)\n',K,T0);
     fprintf('avec %d calculs du critère et %d calculs du gradient\n',NB_J,NB_G);
     if nargout>3  % Stockage de l'historique dess paramètres et du critère 
        ALL_P = ALL_P(:,1:K+1);
        ALL_J = ALL_J(1:K+1);
     end
  end
  COMP_COST = [ K, T0, NB_J, NB_G ] ;
% EXITFLAG = [ FLAG_K_MAX, FLAG_EPS_P,              FLAG_EPS_J ]  
  EXITFLAG = [ (K>K_MAX),  (norm(PkOld-Pk)< EPS_P), (abs(JPkOld-JPk)<EPS_J) ]; 
  
