function J = crit(x,y,H,alpha,phi,p)
% Fonction crit
%   J = crit(x,y,H,alpha,phi,p);
%
%   Calcul d'un critere de la forme 
%       J(x) = ||y-H*x||^2 + alpha \sum phi(Dx)
%   avec la fonction phi correspondant à une norme Lp \phi(x) = |x|^p
%   ou une fonction hyperbolique \phi(x) = \sqrt{p^2 + x^2}
%   et Dx correspondant aux différences de valeurs entre les pixels voisins 
%   horizontalement et verticalement d'une image carrée donnée dans le vecteur x
%
%   Variables d'entrées :
%   x, y, H, alpha, p : variables du critère
%   paramètre phi : 
%       si phi=1 : norme Lp ; \phi(x) = |x|^p
%       si phi=2 : fonction hyperbolique ; \phi(x) = \sqrt{p^2 + x^2}

% H. Carfantan, IRAP, janvier 2020

if isempty(p), p=2; end
if isempty(phi), phi=1; end

% Calcul de l'erreur entre les données et le modèle
% et du terme quadratique de fidélité aux données du critère
    res = y-H*x;
    J = res'*res;
    
% Calcul du terme de pénalisation sur les différences horizontales 
% et verticales entre pixels voisins
  N = sqrt(length(x));  % taille de l'image carrée x
  img = reshape(x,N,N); % image
  dx_vert =  diff([zeros(1,N); img; zeros(1,N)],1,1);
  dx_horiz = diff([zeros(N,1), img, zeros(N,1)],1,2);   
  if phi==1    % Norme Lp
    J = J + alpha*( sum(abs(dx_vert(:)).^p + abs(dx_horiz(:)).^p) );
  else          % Fonction hyperbolique
    J = J + alpha*sum( sqrt(p^2 + dx_vert(:).^2) + sqrt(p^2 + dx_horiz(:).^2) );
  end
