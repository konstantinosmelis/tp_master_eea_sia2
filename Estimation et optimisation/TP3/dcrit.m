function Grad = dcrit(x,y,H,alpha,phi,p)
% Fonction dcrit
%   Grad = dcrit(x,y,H,alpha,phi,p);
%
%   Calcul du gradient d'un critere de la forme 
%       J(x) = ||y-H*x||^2 + alpha \sum phi(Dx)
%   avec la fonction phi correspondant à une norme Lp \phi(x) = |x|^p
%   ou une fonction hyperbolique \phi(x) = \sqrt{p^2 + x^2}
%   et Dx correspondant aux différences de valeurs entre les pixels voisins 
%   horizontalement et verticalement d'une image carrée donnée dans le vecteur x
%
%   Variables d'entrées :
%   x, y, H, alpha, p : variables du critère
%   paramètre phi : 
%       si phi=1 : norme Lp ; \phi(x) = |x|^p
%       si phi=2 : fonction hyperbolique ; \phi(x) = \sqrt{p^2 + x^2}

% H. Carfantan, IRAP, janvier 2020

if isempty(p), p=2; end
if isempty(phi), phi=1; end

% Calcul de l'erreur entre les données et le modèle et du gradient 
% correspondant au terme quadratique de fidélité aux données 
% du critère
  res = y - H*x;
  Grad = -2*H'*res ;
% Calcul du gradient du terme de pénalisation sur les différences 
% horizontales et verticales entre pixels voisins
  N = sqrt(length(x));  % taille de l'image carrée x
  img = reshape(x,N,N); % image
  dx_vert  = diff([zeros(1,N); img; zeros(1,N)],1,1);
  if phi==1 % Norme Lp
      dx_vert  = p*sign(dx_vert).*abs(dx_vert).^(p-1);
  else      % Fonction hyperbolique
      dx_vert  = (dx_vert)./sqrt( p^2 + (dx_vert).^2 );
  end
  dx_vert  = diff(dx_vert,1,1);
  dx_horiz = diff([zeros(N,1), img, zeros(N,1)],1,2);   
  if phi==1 % Norme Lp
     dx_horiz  = p*sign(dx_horiz).*abs(dx_horiz).^(p-1);
  else      % Fonction hyperbolique
     dx_horiz  = (dx_horiz)./sqrt( p^2 + (dx_horiz).^2 );
  end
  dx_horiz = diff(dx_horiz,1,2);   
  Grad = Grad - alpha*(dx_vert(:) + dx_horiz(:));
