function [w, kurtY, y] = FastICA(z)
    randn('seed', 2);
    w1 = randn(2, 1);
    w1 = w1 / norm(w1);

    N = 1;
    while true
        y = w1' * z;
        kurtY(N) = abs(kurtosis(y));

        w(:, N) = w1;

        w1 = mean(z * y'.^3) - (3 * w1);
        w1 = w1 / norm(w1);

        if norm(w1 - w(:, N)) < 1e-6
            break;
        end
        N = N + 1;
    end
end

