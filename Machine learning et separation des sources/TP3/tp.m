close all, clear all, clc;

%% Generation d'un melange artificiel de deux sources artificielles
rand('seed', 1);

% q1
s = rand(2, 1000);

% q2
s1 = (s(1, :) - mean(s(1, :))) / std(s(1, :));
s2 = (s(2, :) - mean(s(2, :))) / std(s(2, :));

figure, plot(s1);
title("Signal s_1(t)");

figure, plot(s2);
title("Signal s_2(t)");

% q3
corr_s = corrcoef(s1, s2);
fprintf("Matrice de correlation de s1 et s2:");
display(corr_s);

% q4
figure, scatter(s1, s2);
xlabel("s_1(t)"), ylabel("s_2(t)");
title("Nuage des points des deux sources");

% q5
A = [1 .7; .8 1];
x = A * [s1; s2];

% q6
corr_x = corrcoef(x(1, :), x(2, :));
fprintf("Matrice de correlation de x1 et x2:");
display(corr_x);

figure, scatter(x(1, :), x(2, :));
xlabel("x_1(t)"), ylabel("x_2(t)");
title("Nuage des points des signaux mélanges");

%% Blanchiment
% q1
[H, D] = eig(cov(x'));

M = D^(-1/2) * H';
fprintf("Matrice de blanchiment:");
display(M);

% q2
z = M * x;

corr_z = corrcoef(z(1, :), z(2, :));
fprintf("Matrice de correlation de z1 et z2:");
display(corr_z);

figure, scatter(z(1, :), z(2, :));
xlabel("z_1(t)"), ylabel("z_2(t)");
title("Nuage des points des signaux z_1 et z_2");

%% Estimation des sources
% q2
[w1s, kurt_y1, y1] = FastICA(z);
N = length(w1s);

% q3
figure; plot(w1s');
legend("w_1^{(1)}", "w_1^{(2)}");
title("Evolution des composantes de w_1");

figure; plot(kurt_y1);
title("Evolution du kurtosis de y(t)");

% q4
n = 100:200;
figure, plot(y1(n));
title("Signal y_1(t) compare a s_1(t)");
hold on, plot(s1(n)), hold off;

figure, plot(y1(n));
title("Signal y_1(t) compare a s_2(t)");
hold on, plot(s2(n)), hold off;

% q5: Le fait que les vecteurs soient orthogonaux simule une independance.
% Ainsi ils sont orthogonaux pour maximiser l'independance entre les deux
% sources

% q7
alpha = atan(-w1s(1, N) / w1s(2, N));
w2 = [cos(alpha); sin(alpha)];
y2 = w2' * z;

figure, plot(y2(n));
title("Signal y_2(t) compare a s_1(t)");
hold on, plot(s1(n)), hold off;

figure, plot(y2(n));
title("Signal y_2(t) compare a s_2(t)");
hold on, plot(s2(n)), hold off;

% q8
corr_y = corrcoef(y1, y2);
fprintf("Matrice de correlation de y1 et y2:");
display(corr_y);

figure, scatter(y1, y2);
xlabel("y_1(t)"), ylabel("y_2(t)");
title("Nuage des points des signaux y_1 et y_2");

%% Mesure des performances de l'algorithme
s1_norm = s1 / std(s1);
s2_norm = s2 / std(s2);
y1_norm = y1 / std(y1);
y2_norm = y2 / std(y2);

RSI1 = .5 * (10 * log10(mean(s1_norm.^2) / mean((s1_norm + y2_norm).^2)));
RSI2 = .5 * (10 * log10(mean(s2_norm.^2) / mean((s2_norm + y1_norm).^2)));
RSI = RSI1 + RSI2;

%% Premiers tests avec des signaux de teledetection
load("bss_remote_sensing_mix1.mat"), load("bss_remote_sensing_mix2.mat");

% q1
figure, plot(x1);
title("Signal x_1");

figure, plot(x2);
title("Signal x_2");

x1 = (x1 - mean(x1)) / std(x1);
x2 = (x2 - mean(x2)) / std(x2);
x = [x1; x2];

[H, D] = eig(cov([x1' x2']));
M = D^(-1/2) * H';
z = M * x;

[w1s, kurt_y1, y1] = FastICA(z);
N = length(w1s);

alpha = atan(-w1s(1, N) / w1s(2, N));
w2 = [cos(alpha); sin(alpha)];
y2 = w2' * z;

figure, plot(y1);
title("Signal y_1");

figure, plot(-y1);
title("Signal -y_1");

figure, plot(y2);
title("Signal y_2");

figure, plot(-y2);
title("Signal -y_2");

% q2
load("data.m");
% load_ign_crs;
% load_ign_fn;

K = length(ign_crs(:, 1));
coef_corr_y1_crs = zeros(1, K);
coef_corr_ny1_crs = zeros(1, K);
coef_corr_y2_crs = zeros(1, K);
coef_corr_ny2_crs = zeros(1, K);
for n = 1:K
    coef_corr_y1_crs(n) = corrcoef(y1, ign_crs(n, :))(1, 2);
    coef_corr_ny1_crs(n) = corrcoef(-y1, ign_crs(n, :))(1, 2);
    coef_corr_y2_crs(n) = corrcoef(y2, ign_crs(n, :))(1, 2);
    coef_corr_ny2_crs(n) = corrcoef(-y2, ign_crs(n, :))(1, 2);
end

fprintf("Coefficient de correlation max: %f entre y1 et ign_crs\n", max(coef_corr_y1_crs));
fprintf("Coefficient de correlation max: %f entre -y1 et ign_crs\n", max(coef_corr_ny1_crs));
fprintf("Coefficient de correlation max: %f entre y2 et ign_crs\n", max(coef_corr_y2_crs));
fprintf("Coefficient de correlation max: %f entre -y2 et ign_crs\n", max(coef_corr_ny2_crs));

K = length(ign_fn(:, 1));
coef_corr_y1_fn = zeros(1, K);
coef_corr_ny1_fn = zeros(1, K);
coef_corr_y2_fn = zeros(1, K);
coef_corr_ny2_fn = zeros(1, K);
for n = 1:K
    coef_corr_y1_fn(n) = corrcoef(y1, ign_fn(n, :))(1, 2);
    coef_corr_ny1_fn(n) = corrcoef(-y1, ign_fn(n, :))(1, 2);
    coef_corr_y2_fn(n) = corrcoef(y2, ign_fn(n, :))(1, 2);
    coef_corr_ny2_fn(n) = corrcoef(-y2, ign_fn(n, :))(1, 2);
end

fprintf("Coefficient de correlation max: %f entre y1 et ign_fn\n", max(coef_corr_y1_fn));
fprintf("Coefficient de correlation max: %f entre -y1 et ign_fn\n", max(coef_corr_ny1_fn));
fprintf("Coefficient de correlation max: %f entre y2 et ign_fn\n", max(coef_corr_y2_fn));
fprintf("Coefficient de correlation max: %f entre -y2 et ign_fn\n", max(coef_corr_ny2_fn));

figure, plot(-y1);
hold on, plot(ign_crs(24, :)), hold off;
title("Spectre -y_1 compare a ign\\_crs(24)");
legend("-y_1", "ign\\_crs");

figure, plot(-y2);
hold on, plot(ign_crs(29, :)), hold off;
title("Spectre -y_2 compare a ign\\_crs(29)");
legend("-y_2", "ign\\_crs");

figure, plot(-y2);
hold on, plot(ign_fn(29, :)), hold off;
title("Spectre -y_2 compare a ign\\_fn(29)");
legend("-y_2", "ign\\_fn");

%% Deuxiemes test avec des signaux de teledetection
load("bss_remote_sensing_sources_1.mat");

figure, plot(spectrum1);
title("Spectre spectrum_1");

figure, plot(spectrum2);
title("Spectre spectrum_2");

% q1
A = [.4, .1; .1, .4];
x = A * [spectrum1; spectrum2];

[H, D] = eig(cov(x'));
M = D^(-1/2) * H';
z = M * x;

[w1s, kurt_y1, y1] = FastICA(z);
N = length(w1s);

alpha = atan(-w1s(1, N) / w1s(2, N));
w2 = [cos(alpha); sin(alpha)];
y2 = w2' * z;

figure, plot(-y1);
title("Spectre -y_1 estime");

figure, plot(-y2);
title("Spectre -y_2 estime")

s1_norm = spectrum1 / std(spectrum1);
s2_norm = spectrum2 / std(spectrum2);
y1_norm = y1 / std(y1);
y2_norm = y2 / std(y2);

RSI1 = .5 * (10 * log10(mean(s1_norm.^2) / mean((s1_norm + y2_norm).^2)));
RSI2 = .5 * (10 * log10(mean(s2_norm.^2) / mean((s2_norm + y1_norm).^2)));
RSI = RSI1 + RSI2;

