% GOAL: this script file loads the data matrix ign_fn
%
% DETAILED DESCRIPTION:
% this script file loads the data file ign_fn.sli into matrix ign_fn .
% It is derived from file load_igcp_1.m , where it is stated:
% """
% Les informations (nombre colonnes, lignes, etc) sont extraites a partir
% du fichier igcp_1.hdr
% nombre de lignes=nombre de materiaux
% nombre de colonnes= nombre de points spectraux
% nombre de bandes: toujours egale a 1
% """
%
% AUTHORS: Yannick Deville
%          Institut de Recherche en Astrophysique et Planetologie
%          Observatoire Midi Pyrenees - Universite Paul Sabatier Toulouse 3
%          14 Av. Ed. Belin
%          31400 Toulouse
%          France
%          ydeville@ast.obs-mip.fr
%
% HISTORY: - Original file:
%            created on Oct. 31, 2011 and checked right after (by comparing it
%            to load_igcp_1.m , which is assumed to be correct, except that the last
%            argument of multibandread, i.e. BYTEORDER, had to be changed here:
%            see explanations in load_ign_crs.m , and ign_fn.hdr also
%            contains "byte order = 1".
%            plot(ign_fn(1,:)) is OK).
%          - Updates:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ign_fn = multibandread('ign_fn.sli', [33 2844 1], 'float', 0, 'bsq', 'b');

