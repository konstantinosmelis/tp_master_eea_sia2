close all, clear all, clc;

load("bss_remote_sensing_3sources_1.mat");

%% Generation d'une image artificielle a partir de 3 spectres
% q1
N = 3;
k = 5;
P = k^2;

S = [spectrum1; spectrum2; spectrum3];

A = rand(P, N);
A = A ./ sum(A, 2);

b = 0.03;
t = sort(randi(length(A), 1, N));
A(t, :) = [1-2*b, b, b; b, 1-2*b, b; b, b, 1-2*b];

X = A * S;

% q2
figure, scatter(X(:, 5), X(:, 2500), '*');
hold on, scatter(S(1, 5), S(1, 2500));
scatter(S(2, 5), S(2, 2500));
scatter(S(3, 5), S(3, 2500));
hold off;
title(sprintf("Nuage des points des observations et des sources pour P=%d", P));
legend("Observations", "spectrum_1", "spectrum_2", "spectum_3", 'Location', 'northwest');

% q3
fprintf("Matrice de correlation des sources:\n");
display(corrcoef(S'));

%% Programmation de la methode N-FINDR
% q1
%%% Pour effectuer une reduction de données sans perte d'informations il
%%% est prudent d'effectuer une Analyse a Composantes Principales (ACP).
%%% L'ACP permet de reduire le nombre de variables à considérer tout en
%%% gardant les informations importantes sur les données

C = cov(X);
[vec_propres, val_propres] = eig(C);
val_propres = abs(diag(val_propres));
[val_propres, index] = sort(val_propres, 'descend');
vec_propres = vec_propres(:, index);

img = X * vec_propres(:, 1:(N-1));

% q2
[Y, V, ~] = n_findr(img, P, N);

figure, plot(V);
title("Evolution du volume au fil des iterations");

figure, scatter(img(:, 1), img(:, 2), '*');
hold on, scatter(img(Y(1), 1), img(Y(1), 2));
scatter(img(Y(2), 1), img(Y(2), 2));
scatter(img(Y(3), 1), img(Y(3), 2));
hold off;
title("Nuage des points des observations et des endmembers");
legend("Observations", "endmember_1", "endmember_2", "endmember_3", 'Location', 'northwest');

%%% Nous pouvons bien voir que les endmembers obtenus forment bien un
%%% triangle autour des observations.

%% Analyse des donnees
% q2
for b = [0 .1]
    A = rand(P, N);
    A = A ./ sum(A, 2);
    t = sort(randi(length(A), 1, N));
    A(t, :) = [1-2*b, b, b; b, 1-2*b, b; b, b, 1-2*b];
    X = A * S;

    C = cov(X);
    [vec_propres, val_propres] = eig(C);
    val_propres = abs(diag(val_propres));
    [val_propres, index] = sort(val_propres, 'descend');
    vec_propres = vec_propres(:, index);

    img = X * vec_propres(:, 1:(N-1));
    [Y, V, ~] = n_findr(img, P, N);

    figure, plot(V);
    title(sprintf("Evolution du volume au fil des iterations pour b=%2f", b));

    figure, scatter(img(:, 1), img(:, 2), '*');
    hold on, scatter(img(Y(1), 1), img(Y(1), 2));
    scatter(img(Y(2), 1), img(Y(2), 2));
    scatter(img(Y(3), 1), img(Y(3), 2));
    hold off;
    title(sprintf("Nuage des points des observations et des endmembers pour b=%2f", b));
    legend("Observations", "endmember_1", "endmember_2", "endmember_3", 'Location', 'northwest');

    figure, plot(spectrum1);
    hold on, plot(spectrum2), plot(spectrum3);
    plot(X(Y(1), :)), plot(X(Y(2), :)), plot(X(Y(3), :));
    hold off;
    title(sprintf("Spectres theoriques et sources estimees pour b=%2f", b));
    legend("spectrum_1", "spectrum_2", "spectrum_3", "source_1", "source_2", "source_3");

    RSI = .5 * sum(log10(mean(S.^2, 2) ./ mean((S - [X(Y(1), :); X(Y(2), :); X(Y(3), :)]).^2, 2)));
    fprintf("Pour b=%2f => RSI=%f dB\n", b, RSI);
end

%% Test sur d'autres donnees et comparaison avec FastICA
load("bss_remote_sensing_sources_1.mat");

% q1
N = 2;

S = [spectrum1; spectrum2];

a = linspace(0, 1, P);
A = [1 - a; a]';
X = A * S;

C = cov(X);
[vec_propres, val_propres] = eig(C);
val_propres = abs(diag(val_propres));
[val_propres, index] = sort(val_propres, 'descend');
vec_propres = vec_propres(:, index);

% q2
img = X * vec_propres(:, 1:(N-1));
[Y, V, ~] = n_findr(img, P, N);

figure, plot(spectrum1);
hold on, plot(spectrum2), hold off;
title("Spectres theoriques");
legend("spectrum_1", "spectrum_2");

figure, plot(X(Y(1), :));
hold on, plot(X(Y(2), :)), hold off;
title("Sources estimees");
legend("source_1", "source_2");

RSI = .5 * sum(log10(mean(S.^2, 2) ./ mean((S - [X(Y(1), :); X(Y(2), :)]).^2, 2)));
fprintf("RSI = %f dB\n", RSI);

% q3
a = linspace(0, 9, P);
A = [1 - a; a]';
X = A * S;

C = cov(X);
[vec_propres, val_propres] = eig(C);
val_propres = abs(diag(val_propres));
[val_propres, index] = sort(val_propres, 'descend');
vec_propres = vec_propres(:, index);

img = X * vec_propres(:, 1:(N-1));
[Y, V, ~] = n_findr(img, P, N);

figure;
subplot(121), plot(X(Y(1), :)), title("Source 1");
subplot(122), plot(X(Y(2), :)), title("Source 2");

RSI = .5 * sum(log10(mean(S.^2, 2) ./ mean((S - [X(Y(1), :); X(Y(2), :)]).^2, 2)));
fprintf("RSI = %f dB\n", RSI);

%% Estimation des coefficients de melange
load("bss_remote_sensing_3sources_1.mat");

S = [spectrum1; spectrum2; spectrum3];

