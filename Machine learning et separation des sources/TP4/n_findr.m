function [Y, V, it] = n_findr(img, P, N)
    rand('seed', 1);
    endmembers = img(randi(P, N, 1), :)';

    Y = ones(N, 1);
    it = 1;
    V(it) = abs(det([ones(1, N); endmembers])) / factorial(N-1);
    while true
        it = it + 1;
        volume = V(it - 1);
        for p = 1:P
            vols = zeros(1, N);
            for n = 1:N
                e = endmembers;
                e(:, n) = img(p, :)';
                vols(n) = abs(det([ones(1, N); e])) / factorial(N-1);
            end

            [maxVol, imaxVol] = max(vols);
            if maxVol > volume
                endmembers(:, imaxVol) = img(p, :)';
                Y(imaxVol) = p;
                volume = maxVol;
            end
        end
        V(it) = volume;

        if V(it) <= V(it - 1)
            break;
        end
    end
end

