% Exercices Simples

% Part 1
sqrt(10);
sqrt(10)^2;
1i;
1i^2;
x = (1 + 3i)*(2 + 2i)*(3 + 1i);
module = abs(x);
argument = angle(x);
cos(pi/2);
sin(pi/2);
exp(1i*pi/2);

% Part 2
vl = 1:2:20;
vc = vl';
vcvl = vc*vl;
vlvc = vl*vc;
vl = 2:2:24;
A = reshape(vl, [4, 3])';
N = 6;
v3 = [(0:N),(N-1:-1:1)];
N = 9;
v3 = [(0:N),(N-1:-1:1)];

% Part 3
A = [A; 26:2:32];
A(3,:) % 3e ligne
A(:,2) % 2e colone
A(1,2)
A(3,4)
B = A; B(:,4) = []; B(2,:) = [];
D = (A.*A) + 1;
P = 3;
C = mod(reshape([1:P*P], [P, P]), 2);
B*C;
B.*C;
D = [10, 7, 8, 7 ; 7, 5, 6, 5 ; 8, 6, 10, 9 ; 7, 5, 9, 10];
iD = inv(D);