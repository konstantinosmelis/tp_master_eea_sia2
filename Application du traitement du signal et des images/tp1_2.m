% Afficahge graphique et alphanumerique

% 1D
t = 0:0.01:1;
f = 5;
sinu = sin(2*pi*f*t);
plot(t, sinu);

%%
exp_compl = exp(1i*2*pi*f*t);
subplot(2, 2, 1);
plot(t, real(exp_compl));
title('Partie réelle');
subplot(2, 2, 2);
plot(t, imag(exp_compl));
title('Partie imaginaire');
subplot(2, 2, 3);
plot(t, abs(exp_compl));
title('Module');
subplot(2, 2, 4);
plot(t, angle(exp_compl));
title('Argument');

%%
exp_compl_am = exp(-t') * exp_compl;
plot(exp_compl_am);
subplot(2, 2, 1);
plot(t, real(exp_compl_am));
title('Partie réelle');
subplot(2, 2, 2);
plot(t, imag(exp_compl_am));
title('Partie imaginaire');
subplot(2, 2, 3);
plot(t, abs(exp_compl_am));
title('Module');
subplot(2, 2, 4);
plot(t, angle(exp_compl_am));
title('Argument');

%%
x = -5:0.01:5;
gaussian = (1 / sqrt(2*pi)) * exp(-(x.^2)/2);
plot(x, gaussian);