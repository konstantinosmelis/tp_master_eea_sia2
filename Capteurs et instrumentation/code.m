close all, clear, clc;
format long;

% load('data.mat');

img = readfits("image10s.fit");
dark = readfits("dark10s.fit");

% img = img - dark;

figure, imagesc(img);
colormap gray, axis image;
title('Image capturee avec 10s de temps de pose');

% Sur une ligne
k = 400;
N = size(img, 2);
ligne = img(k, :);
f = polyfit(1:N, ligne, 1);
f = f(1) * (1:N) + f(2);

figure, plot(ligne);
hold on, plot(f), hold off;
title(sprintf('Profil de la ligne %d', k));
legend('Profil de ligne', 'Droite de variation');

CTE_x = (f(N) / f(1)) ^ (1 / N);
display(CTE_x);

% Sur la moyenne d'un nombre M de lignes
k = 300:500;
ligne = mean(img(k, :));
f = polyfit(1:N, ligne, 1);
f = f(1) * (1:N) + f(2);

figure, plot(ligne);
hold on, plot(f), hold off;
title(sprintf('Moyenne des profils des lignes %d a %d', k(1), k(end)));
legend('Moyenne des profils de lignes', 'Droite de variation');

CTE_x_m = (f(N) / f(1)) ^ (1 / N);
display(CTE_x_m);

% Sur une colone
k = 400;
N = size(img, 1);
ligne = img(:, k);
f = polyfit(1:N, ligne, 1);
f = f(1) * (1:N) + f(2);

figure, plot(ligne);
hold on, plot(f), hold off;
title(sprintf('Profil de la colone %d', k));
legend('Profil de colone', 'Droite de variation');

CTE_y = (f(N) / f(1)) ^ (1 / N);
display(CTE_y);

% Sur la moyenne d'un nombre M de colones
k = 520:700;
ligne = mean(img(:, k), 2);
f = polyfit(1:N, ligne, 1);
f = f(1) * (1:N) + f(2);

figure, plot(ligne);
hold on, plot(f), hold off;
title(sprintf('Moyenne des profils des colones %d a %d', k(1), k(end)));
legend('Moyenne des profils de colones', 'Droite de variation');

CTE_y_m = (f(N) / f(1)) ^ (1 / N);
display(CTE_y_m);

