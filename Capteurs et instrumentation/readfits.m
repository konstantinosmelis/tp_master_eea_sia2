function b=readfits(filename)

fid=fopen(filename,'r','b');
sortie=0;
nblignes=0;
bzero=0;
bscale=1;
while(1==1)
   [a,count]=fread(fid,80,'char');
   nblignes=nblignes+1;
   aa=setstr(a');
   keyword=aa(1:6);
   if (strcmp(keyword,'NAXIS1')==1)
      naxis1=str2num(aa(27:30));
   end
   if (strcmp(keyword,'NAXIS2')==1)
      naxis2=str2num(aa(27:30));
   end
   if (strcmp(keyword,'BZERO ')==1)
      bzero=str2num(aa(20:30));
   end
   if (strcmp(keyword,'BSCALE')==1)
      bscale=str2num(aa(20:30));
   end
   if (strcmp(keyword,'BITPIX')==1)
      bitpix=str2num(aa(27:30));
   end
   if (strcmp(keyword,'END   ')==1)
      sortie=1;
   end
   if (nblignes==36)
      if (sortie==1)
         break
      else
         nblignes=0;
      end
   end
end

if (bitpix==16)
   [b,count]=fread(fid,[naxis1,naxis2],'ushort');
   b=b'-bzero;
end

if (bitpix==-32)
   [b,count]=fread(fid,[naxis1,naxis2],'float');
   b=b';
end

fclose(fid);
