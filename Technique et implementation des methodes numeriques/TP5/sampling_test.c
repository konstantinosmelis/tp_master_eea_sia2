#include <stdio.h>
#include <stdlib.h>
#include "lib/sampling.h"

int main(int argc, char *argv[]) {
    FILE *fd;
    double min, max;
    int n;
    struct sampling *sampling = NULL;
    struct quadrature *quad = NULL;
    struct subdivision *subdiv;
    struct interval *inter;

    if(argc != 5) {
        fprintf(stderr, "Usage: %s qudratureFile numberOfSubdivisions min max\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    n = atoi(argv[2]);
    min = atof(argv[3]);
    max = atof(argv[4]);

    inter = malloc(sizeof(struct interval));
    subdiv = malloc(sizeof(struct subdivision));

    inter->min = min;
    inter->max = max;
    subdiv->number = n;

    create_subdivision(inter, subdiv);
    
    fd = fopen(argv[1], "r");
    create_quadrature(fd, &quad);
    fclose(fd);
    
    sampling = malloc(sizeof(struct sampling));
    calculate_sampling(sampling, quad, subdiv);
    
    printf("I= %f\n", calculate_integral(sampling));
    
    gnuplot(*sampling);

    destroy_interval(inter);
    destroy_sampling(sampling);

    return 0;
}
