struct interval {
    double min;
    double max;
};

struct subdivision {
    int number;
    struct interval *intervals;
};

void display_interval(struct interval*);

void display_subdivision(struct subdivision*);

void create_subdivision(struct interval*, struct subdivision*);

void destroy_interval(struct interval*);

void destroy_subdivision(struct subdivision*);
