#include "quadrature.h"
#include "interval.h"

struct sampling {
    struct quadrature *quadrature;
    struct subdivision *subdivision;
    float **sample_matrix;
};

float f(float);

void calculate_sampling(struct sampling*, struct quadrature*, struct subdivision*);

void destroy_sampling(struct sampling*);

float calculate_integral(struct sampling*);

void gnuplot(struct sampling);

void save(char*, float*, float*, int);
