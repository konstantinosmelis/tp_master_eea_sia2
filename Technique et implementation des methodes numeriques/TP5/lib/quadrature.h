#include "stdio.h"

struct quadrature {
    int size;
    float *zeta;
    float *omega;
};

void create_quadrature(FILE*, struct quadrature**);

int read_quadrature(FILE*, struct quadrature**);

void write_quadrature(FILE*, struct quadrature*);

void display_quadrature(struct quadrature*);

void destroy_quadrature(struct quadrature*);
