#include <stdio.h>
#include <stdlib.h>
#include "quadrature.h"

void create_quadrature(FILE *fd, struct quadrature **quadrature) {
    int size;
    size = read_quadrature(fd, quadrature);

    if(size == 0) {
        fprintf(stderr, "File has 0 values for zeta and omega\n");
        destroy_quadrature(*quadrature);
        exit(EXIT_FAILURE);
    }
}

int read_quadrature(FILE *fd, struct quadrature **quadrature) {
    int i, n = 0;

    fscanf(fd, "%d", &n);

    *quadrature = malloc(n * sizeof(struct quadrature));
    (*quadrature)->size = n;
    (*quadrature)->zeta = malloc(n * sizeof(float));
    (*quadrature)->omega = malloc(n * sizeof(float));

    for(i = 0; i < n; i++) {
        if(fscanf(fd, "%f %f", &(*quadrature)->zeta[i], &(*quadrature)->omega[i]) == 2) {
            if((*quadrature)->zeta[i] < 0 || (*quadrature)->zeta[i] > 1) {
                fprintf(stderr, "The value of zeta[%d] is not between 0 and 1\n", i);
                destroy_quadrature(*quadrature);
                exit(EXIT_FAILURE);
            }
            else
                continue;
        }
    }
    return n;
}

void write_quadrature(FILE *fd, struct quadrature *quadrature) {
    int i;

    fprintf(fd, "%d\n", quadrature->size);
    for(i = 0; i < quadrature->size; i++)
        fprintf(fd, "%f %f\n", quadrature->zeta[i], quadrature->omega[i]);
}

void display_quadrature(struct quadrature *quadrature) {
    int i;

    printf("Data read:\n");
    for(i = 0; i < quadrature->size; i++)
        printf("\tzeta[%d] = %f, omega[%d] = %f\n", i, quadrature->zeta[i], i, quadrature->omega[i]);
}

void destroy_quadrature(struct quadrature *quadrature) {
    free(quadrature->zeta);
    free(quadrature->omega);
    free(quadrature);
}
