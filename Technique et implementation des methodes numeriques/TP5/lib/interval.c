#include <stdio.h>
#include <stdlib.h>
#include "interval.h"

void display_interval(struct interval *interval) {
    printf("[%.3f; %.3f]\n", interval->min, interval->max);
}

void display_subdivision(struct subdivision *subdivision) {
    int i;

    printf("Subdivisions: \n");
    for(i = 0; i < subdivision->number; i++) {
        printf("\t");
        display_interval(subdivision->intervals + i);
    }
}

void create_subdivision(struct interval *interval, struct subdivision *subdivision) {
    int i;
    double min, step;
    struct interval temp_interval;

    subdivision->intervals = malloc(subdivision->number * sizeof(struct interval));
    step = (interval->max - interval->min) / subdivision->number;
    min = interval->min;

    for(i = 0; i < subdivision->number; i++) {
        temp_interval.min = min;
        temp_interval.max = min + step;
        subdivision->intervals[i] = temp_interval;
        min += step;
    }
}

void destroy_interval(struct interval *interval) {
    free(interval);
}

void destroy_subdivision(struct subdivision *subdivision) {
    free(subdivision->intervals);
    free(subdivision);
}
