#include <stdlib.h>
#include <math.h>
#include "sampling.h"

float f(float x) {
    return (1 / sqrt(2 * M_PI)) * exp(- (x * x) / 2);
    //return 2;
}

void calculate_sampling(struct sampling *sampling, struct quadrature *quadrature, struct subdivision *subdivision) {
    int i,j;
    sampling->quadrature = quadrature;
    sampling->subdivision = subdivision;
    sampling->sample_matrix = malloc(subdivision->number * sizeof(float*));
    
    for(i = 0; i < subdivision->number; i++)
        sampling->sample_matrix[i] = malloc(quadrature->size * sizeof(float));

    for(i = 0; i < subdivision->number; i++) {
        for(j = 0; j < quadrature->size; j++) {
            sampling->quadrature->zeta[j] = (1 + cos(((2 * j + 1) * M_PI) / (2 * quadrature->size))) / 2;
            sampling->quadrature->omega[j] = (M_PI * sin(((2 * j + 1) * M_PI) / (2 * quadrature->size))) / (2 * quadrature->size);
            sampling->sample_matrix[i][j] = f((quadrature->zeta[j] * subdivision->intervals[i].max) + (subdivision->intervals[i].min * (1 - quadrature->zeta[j])));
        }
    }
}

void destroy_sampling(struct sampling *sampling) {
    int i;
    
    for(i = 0; i < sampling->subdivision->number; i++)
        free(sampling->sample_matrix[i]);
        
    destroy_subdivision(sampling->subdivision);
    destroy_quadrature(sampling->quadrature);
    free(sampling);
}

float calculate_integral(struct sampling *sampling) {
    int i, j;
    float min, max, omega, result = 0;

    for(i = 0; i < sampling->subdivision->number; i++) {
        min = sampling->subdivision->intervals[i].min;
        max = sampling->subdivision->intervals[i].max;
        
        for(j = 0; j < sampling->quadrature->size; j++) {
            omega = sampling->quadrature->omega[j];
            result += (max - min) * (omega * sampling->sample_matrix[i][j]);
        }
    }

    return result;
}

void gnuplot(struct sampling sampling) {
    int n = (sampling.subdivision)->number;
    int p = (sampling.quadrature)->size;
    int m = 1000;
    int j, k;
    float min, max, zeta_sum;
    float *Ordonnees_f_x, *Abscisses_x, *Ordonnees_fboxes_x, *Abscisses_boxes;

    Abscisses_x = (float*) malloc(m * sizeof(float));
    Ordonnees_f_x = (float*) malloc(m * sizeof(float));
    Abscisses_boxes = (float*) malloc((2 * p * n + 1) * sizeof(float));
    Ordonnees_fboxes_x = (float*) malloc(2 * p * n * sizeof(float));
    Abscisses_boxes[0] = (sampling.subdivision)->intervals[0].min;
    
    for(j = 0; j < n; j++) {
        min = ((sampling.subdivision)->intervals)[j].min;
        max = ((sampling.subdivision)->intervals)[j].max;
        zeta_sum = 0;

        for(k = 0; k < p; k++) {
            zeta_sum += ((sampling.quadrature)->omega)[k];
            Ordonnees_fboxes_x[2 * (p * j + k)] = (sampling.sample_matrix)[j][k];
            Ordonnees_fboxes_x[2 * (p * j + k) + 1] = (sampling.sample_matrix)[j][k];
            Abscisses_boxes[2 * (p * j + k) + 1] = (1 - zeta_sum) * min + zeta_sum * max;
            Abscisses_boxes[2 * (p * j + k) + 2] = (1 - zeta_sum) * min + zeta_sum * max;
        }
    }

    min = ((sampling.subdivision)->intervals)[0].min;
    max = ((sampling.subdivision)->intervals)[n-1].max;
    for(j = 0; j < m; j++) {
        Abscisses_x[j] = min * (m-j-0.5) / m + max * (j + 0.5) / m;
        Ordonnees_f_x[j] = f(Abscisses_x[j]);
    }

    save("courbe_ligne.dat", Abscisses_x, Ordonnees_f_x, m);
    save("courbe_integration_boxes.dat", Abscisses_boxes, Ordonnees_fboxes_x, 2 * p * n);

    free(Ordonnees_fboxes_x);
    free(Abscisses_x);
    free(Ordonnees_f_x);
    free(Abscisses_boxes);
}

void save(char *filename, float *Abscisses, float *Ordonnees, int n)
{	FILE* fd = NULL;
    int k;
    fd = fopen(filename, "w");
    if(fd != NULL) {
        for(k=0;k<n;k++)
               fprintf(fd, "%f\t%f\n", Abscisses[k], Ordonnees[k]);
        fclose(fd);
    }
}
