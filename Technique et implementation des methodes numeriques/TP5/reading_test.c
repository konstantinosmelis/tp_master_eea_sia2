#include <stdio.h>
#include <stdlib.h>
#include "lib/quadrature.h"
#include "lib/interval.h"

int main(int argc, char *argv[]) {
    FILE *fd;
    struct quadrature *quadrature = NULL;

    if(argc != 2) {
        fprintf(stderr, "Usage: %s fileName", argv[0]);
        exit(EXIT_FAILURE);
    }

    fd = fopen(argv[1], "r");
    if(!fd) {
        perror("Error while opening file");
        exit(EXIT_FAILURE);
    }

    create_quadrature(fd, &quadrature);
    fclose(fd);

    display_quadrature(quadrature);

    destroy_quadrature(quadrature);

    return 0;
}
