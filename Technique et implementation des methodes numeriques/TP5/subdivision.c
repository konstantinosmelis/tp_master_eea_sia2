#include <stdio.h>
#include <stdlib.h>
#include "lib/interval.h"

int main(int argc, char *argv[]) {
    double min, max;
    int n;
    struct interval *inter;
    struct subdivision *subdiv;

    if(argc != 4) {
        fprintf(stderr, "Usage: %s numberOfSubdivisions min max\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    n = atoi(argv[1]);
    min = atof(argv[2]);
    max = atof(argv[3]);

    inter = malloc(sizeof(struct interval));
    subdiv = malloc(sizeof(struct subdivision));

    inter->min = min;
    inter->max = max;
    subdiv->number = n;

    create_subdivision(inter, subdiv);

    display_subdivision(subdiv);

    destroy_subdivision(subdiv);
    destroy_interval(inter);

    return 0;
}
