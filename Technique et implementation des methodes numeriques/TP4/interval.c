#include <stdlib.h>
#include <math.h>
#include "interval.h"

void regular_sampling(float min, float max, int n, float *samples) {
    int i;
    float step, k;

    step = (max - min) / n;
    k = min + (step / 2);
    for(i = 0; i < n; i++) {
        samples[i] = k;
        k += step;
    }
}

void tchebychev_roots(int m, float min, float max, float *roots) {
    int i;

    for(i = 0; i <= m; i++) {
        roots[i] = (((max - min) / 2) * cos((M_PI / 2) * (((2.0 * i) + 1) / (m + 1)))) + ((min + max) / 2);
    }
}
