#include <stdio.h>
#include <stdlib.h>
#include "polynomial_lib.h"
#include "interval.h"

int main(int argc, char *argv[]) {
    FILE *fd;
    struct polynomial *poly;
    float *samples, *images;

    if(argc != 6) {
        fprintf(stderr, "Usage: %s degree numSamples min max outfile\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    poly = malloc(sizeof(struct polynomial));
    create(poly, atoi(argv[1]));
    write(poly);

    samples = malloc(atoi(argv[2]) * sizeof(float));
    regular_sampling(atof(argv[3]), atof(argv[4]), atoi(argv[2]), samples);

    images = malloc(atoi(argv[2]) * sizeof(float));
    sample_image(poly, atoi(argv[2]), samples, images);

    fd = fopen(argv[5], "w");
    if(!fd) {
        perror("Error while opening file");
        exit(EXIT_FAILURE);
    }
    fprint(fd, atoi(argv[2]), samples, images);
    fclose(fd);

    free(poly->coefficients);
    free(poly);
    free(samples);

    return 0;
}
