#include <stdio.h>
#include <stdlib.h>
#include "polynomial_lib.h"
#include "interval.h"

int main(int argc, char *argv[]) {
    FILE *fd;
    int i, m;
    float *samples, *X, *Y, *lag_im;
    char filename[25];

    if(argc != 2) {
        fprintf(stderr, "Usage: %s numPoints\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    m = atoi(argv[1]);
    samples = malloc(1000 * sizeof(float));
    lag_im = malloc(1000 * sizeof(float));
    X = malloc((m + 1) * sizeof(float));
    Y = malloc((m + 1) * sizeof(float));
    
    regular_sampling(-1, 1, 1000, samples);
    regular_sampling(-1, 1, m, X);
    
    for(i = 0; i <= m; i++)
        Y[i] = (X[i] * X[i]) - X[i] + 1;

    lagrange_sample_image(m, 1000, samples, X, Y, lag_im);

    snprintf(filename, sizeof(filename), "m_%d.dat", m);
    fd = fopen(filename, "w");
    fprint(fd, 1000, samples, lag_im);
    fclose(fd);

    free(samples);
    free(X);
    free(Y);
    free(lag_im);

    return 0;
}
