#include <stdio.h>
#include <stdlib.h>
#include "polynomial_lib.h"
#include "interval.h"

int main(int argc, char *argv[]) {
    FILE *fd;
    char filename[25];
    int i, j, m, num_samples;
    float *X, *Y, *samples, *lagrange_images;

    if(argc != 5) {
        fprintf(stderr, "Usage: %s numData numSamples min max\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    m = atoi(argv[1]);
    
    X = malloc((m + 1) * sizeof(float));
    Y = malloc((m + 1) * sizeof(float));
    printf("Values X and Y:\n");
    for(i = 0; i <= m; i++) {
        scanf("%f %f", &X[i], &Y[i]);
    }

    // One file for each li(x) polynomial
    num_samples = atoi(argv[2]);
    samples = malloc(num_samples * sizeof(float));
    regular_sampling(atof(argv[3]), atof(argv[4]), num_samples, samples);
    
    for(i = 0; i <= m; i++) {
        snprintf(filename, sizeof(filename), "l_%d.dat", i);
        fd = fopen(filename, "w");
        for(j = 0; j < num_samples; j++) {
            fprintf(fd, "%f %f\n", samples[j], lagrange_polynomial(i, m, samples[j], X));
        }
        fclose(fd);
    }

    // One file for the interpolating polynomial
    lagrange_images = malloc(num_samples * sizeof(float));
    lagrange_sample_image(m, num_samples, samples, X, Y, lagrange_images);

    fd = fopen("poly_lagrange.dat", "w");
    for(i = 0; i < num_samples; i++) {
        fprintf(fd, "%f %f\n", samples[i], lagrange_images[i]);
    }
    fclose(fd);

    free(X);
    free(Y);
    free(samples);
    free(lagrange_images);

    return 0;
}
