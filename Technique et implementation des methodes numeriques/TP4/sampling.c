#include <stdio.h>
#include <stdlib.h>
#include "polynomial_lib.h"
#include "interval.h"

int main(int argc, char *argv[]) {
    FILE* fd;
    int i, num_samples;
    float *samples;

    if(argc != 3) {
        fprintf(stderr, "Usage: %s numberOfSamples outputFile\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    num_samples = atoi(argv[1]);
    samples = malloc(num_samples * sizeof(float));
    regular_sampling(-1, 1, num_samples, samples);

    fd = fopen(argv[2], "w");
    if(!fd) {
        perror("Error while opening file");
        exit(EXIT_FAILURE);
    }
    for(i = 0; i < num_samples; i++)
        fprintf(fd, "%f\t%f\n", samples[i], f(samples[i]));
    fclose(fd);

    free(samples);

    return 0;
}
