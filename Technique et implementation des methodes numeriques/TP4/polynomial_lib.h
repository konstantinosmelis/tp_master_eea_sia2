#include <stdio.h>

struct polynomial {
    int degree;
    float *coefficients;
};

void create(struct polynomial*, int);

void write(struct polynomial*);

void display(struct polynomial*);

float image(struct polynomial*, float);

void derivative(struct polynomial*, struct polynomial*);

void sum(struct polynomial*, struct polynomial*, struct polynomial*);

void product(struct polynomial*, struct polynomial*, struct polynomial*);

void fprint(FILE*, int, float*, float*);

void sample_image(struct polynomial*, int, float*, float*);

float lagrange_polynomial(int, int, float, float*);

void lagrange_sample_image(int, int, float*, float*, float*, float*);

double f(double x);

void tchebychev_roots(int, float, float, float*);
