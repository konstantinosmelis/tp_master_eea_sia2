h = [0.1 0.01 0.001];
theta = [0.1 1 3 3.13];
l = 1;
omega = sqrt(9.81 / l);

f = @(t, y) [y(2); -(omega^2) * sin(y(1))];

[T, Y] = RK4(f, 0, 30, h(3), theta(4));
[To, Yo] = ode45(f, [0 30], [theta(4) 0]);

subplot(2, 1, 1);
plot(T, Y);

subplot(2, 1, 2);
plot(To, Yo);
