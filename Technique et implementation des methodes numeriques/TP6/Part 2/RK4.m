function [Tout, Yout] = RK4(odefun, T0, Tf, h, y0)
    Tout = (T0:h:Tf);
    Yout = zeros(2, length(Tout));
    Yout(:, 1) = [y0, 0];

    for i = 1:(length(Tout)-1)
        k1 = feval(odefun, Tout(i), Yout(:, i));
        k2 = feval(odefun, Tout(i) + (1/2) * h, Yout(:, i) + (1/2) * h * k1);
        k3 = feval(odefun, Tout(i) + (1/2) * h, Yout(:, i) + (1/2) * h * k2);
        k4 = feval(odefun, Tout(i) + h, Yout(:, i) + h * k3);

        Yout(:, i+1) = Yout(:, i) + (h/6) * (k1 + 2 * k2 + 2 * k3 + k4);
    end
end
