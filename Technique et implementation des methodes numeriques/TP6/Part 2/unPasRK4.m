function [tnext, Ynext] = unPasRK4(odefun, tk, Yk, h)
    pk1 = Yk;
    pk2 = Yk + (h / 2) * feval(odefun, tk, pk1);
    pk3 = Yk + (h / 2) * feval(odefun, tk + (h / 2), pk2);
    pk4 = Yk + h * feval(odefun, tk + h, (pk2 / 2) + (pk3 / 2));

    k1 = feval(odefun, tk, pk1);
    k2 = feval(odefun, tk + (h / 2), pk2);
    k3 = feval(odefun, tk + (h / 2), pk3);
    k4 = feval(odefun, tk + h, pk4);

    tnext = tk + h;
    Ynext = Yk + h * ((1/6) * k1 + (1/3) * k2 + (1/3) * k3 + (1/6) * k4);
end
