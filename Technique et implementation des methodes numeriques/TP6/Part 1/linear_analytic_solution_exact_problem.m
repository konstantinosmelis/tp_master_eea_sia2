t = 0:0.05:30;
l = 1;
theta = [0.1 1 3 3.13];

figure(2);
for i = theta
    plot(analytic_linear_solution(l, t, i));
    hold on;
end

title('Linearized analytic solution'); ylabel('\theta(t) (rad)'); xlabel('t (s)');
legend(['\theta_0 = ' num2str(theta(1)) 'rad'], ['\theta_0 = ' num2str(theta(2)) 'rad'], ['\theta_0 = ' num2str(theta(3)) 'rad'],['\theta_0 = ' num2str(theta(4)) 'rad']);

function res = analytic_linear_solution(l, t, theta)
    omega = sqrt(9.81 / l);
    res = theta * cos(t .* omega);
end