t = 0:0.05:30;
l = 1;
theta = [0.1 1 3 3.13];
omega = sqrt(9.81 / l);

f = @(t, y) [y(2); -(omega^2) * sin(y(1))];

figure(3);
for i = 1:length(theta)
    [Tout,Yout] = ode45(f, t, [theta(i) 0], odeset('RelTol', 1e-6));
    plot(Tout, Yout(:, 1));
    hold on;
end

title('Approched analytic solution'); ylabel('\theta(t) (rad)'); xlabel('t (s)');
legend(['\theta_0 = ' num2str(theta(1)) 'rad'], ['\theta_0 = ' num2str(theta(2)) 'rad'], ['\theta_0 = ' num2str(theta(3)) 'rad'], ['\theta_0 = ' num2str(theta(4)) 'rad']);
