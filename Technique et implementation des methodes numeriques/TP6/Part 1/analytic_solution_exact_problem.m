t = 0:0.05:30;
l = 1;
theta = [0.1 1 3 3.13 pi];

figure(1);
for i = theta
    plot(analytic_solution(l, t, i));
    hold on;
end

title('Analytic solution'); ylabel('\theta(t) (rad)'); xlabel('t (s)');
legend(['\theta_0 = ' num2str(theta(1)) 'rad'], ['\theta_0 = ' num2str(theta(2)) 'rad'], ['\theta_0 = ' num2str(theta(3)) 'rad'],['\theta_0 = ' num2str(theta(4)) 'rad']);

function res = analytic_solution(l, t, theta)
    k = sin(theta / 2);
    omega = sqrt(9.81 / l);
    res = 2 * asin(k * ellipj((t .* omega) + ellipke(k^2), k^2));
end