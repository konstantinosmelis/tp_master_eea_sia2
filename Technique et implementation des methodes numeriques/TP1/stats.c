#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int sum(int*, int);
float avg(int*, int);
float std_deviation(int*, int);
int minimum(int*, int);
int maximum(int*, int);

int main(int argc, char *argv[]) {
	int *numbers;
	int size, i, sum_, max, min;
	float avg_, stddeviation;
	
	printf("Insert the number of values: ");
	scanf("%d", &size);
	
	numbers = malloc(size * sizeof(int));

	for(i = 0; i < size; i++) {
		printf("Insert the number %d: ", i+1);
		scanf("%d", &numbers[i]);
	}
	
	sum_ = sum(numbers, size);
	avg_ = avg(numbers, size);
	stddeviation = std_deviation(numbers, size);
	max = maximum(numbers, size);
	min = minimum(numbers, size);
	
	printf("sum=%d\n", sum_);
	printf("average=%.2f\n",avg_);
	printf("standard deviation=%.2f\n", stddeviation);
	printf("max=%d\n", max);
	printf("min=%d\n", min);

	free(numbers);
	
	return 0;
}

int sum(int *list, int n) {
	int i, sum = 0;
	for(i = 0; i < n; i++)
		sum += list[i];
		
	return sum;
}

float avg(int *list, int n) {
	return ((float) sum(list, n)) / n;
}

float std_deviation(int *list, int n) {	
	float squared_gap = 0, variance;
	float average = avg(list, n);
	int i;
	for(i = 0; i < n; i++)
		squared_gap += pow((list[i] - average), 2);
	variance = ((float) squared_gap) / n;
	return sqrt(variance);
}

int minimum(int *list, int n) {
	int i, min = list[0];
	for(i = 0; i < n; i++) {
		if(min > list[i])
			min = list[i];
	}
	return min;
}

int maximum(int *list, int n) {
	int i, max = list[0];
	for(i = 0; i < n; i++) {
		if(max < list[i])
			max = list[i];
	}
	return max;
}
