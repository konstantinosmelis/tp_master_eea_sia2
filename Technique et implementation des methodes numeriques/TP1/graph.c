#include <stdio.h>

#define N 100

void afficher_tableau(float*, int);
void result(int, float*, float*, float, float, float);
void graph(FILE*, float*, float*, int);
void minimum(float*, float*, int);
void maximum(float*, float*, int);

int main(int argc, char *argv[]) {
	FILE *file;
	float a, b, c, xmin, xmax, step;
	int i = 0;
	float x[N] = {0}, y[N] = {0};
	
	printf("Insert the values of a, b and c of the equation y=ax^2+bx+c: ");
	scanf("%f %f %f", &a, &b, &c);
	printf("Insert the values of Xmin and Xmax as well as the step: ");
	scanf("%f %f %f", &xmin, &xmax, &step);

	do {
		if(xmin > xmax) {
			printf("xmin cannot be greater than xmax. Please try again: ");
			scanf("%f %f %f", &xmin, &xmax, &step);
		}
		else if((xmax-xmin)/step > 100) {
			printf("The step is too small. Please try again: ");
			scanf("%f %f %f", &xmin, &xmax, &step);
		}
	} while(xmin > xmax || (xmax-xmin)/step > 100);

	while(xmin <= xmax) {
		x[i] = xmin;
		xmin += step;
		i++;
	}
	
	result(i, x, y, a, b, c);
	
	minimum(x, y, i);
	maximum(x, y, i);
	
	file = fopen("points.dat", "w");
	graph(file, x, y, i);
	fclose(file);
	
	return 0;
}

void display_array(float *tab, int size) {
	int i;
	for(i = 0; i < size; i++)
		printf("%f  ", tab[i]);
	printf("\n");
}

void result(int k, float *x, float *y, float a, float b, float c) {
	int i;
	for(i = 0; i < k; i++) 
		y[i] = (a * x[i] * x[i]) + (b * x[i]) + c;
}

void graph(FILE *file, float *x, float *y, int k) {
	int i;
	for(i = 0; i < k; i++)
		fprintf(file, "%f %f\n", x[i], y[i]);
}

void minimum(float *x, float *y, int k) {
	int i, imin = 0;
	float ymin = y[0];
	
	for(i = 0; i < k; i++) {
		if(ymin > y[i]) {
			ymin = y[i];
			imin = i;		
		}
	}

	printf("Minimum: x=%.2f y=%.2f\n", x[imin], y[imin]);
}

void maximum(float *x, float *y, int k) {
	int i, imax = 0;
	float ymax = y[0];
	
	for(i = 0; i < k; i++) {
		if(ymax < y[i]) {
			ymax = y[i];
			imax = i;
		}
	}
	
	printf("Maximum: x=%.2f y=%.2f\n", x[imax], y[imax]);
}
