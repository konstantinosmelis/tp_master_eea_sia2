#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
	float a = 0, b, c, delta;
	float x0, x1, ima0, ima1;
	
	printf("Solve the equation: ax^2+bx+c=0\n");
	printf("Insert values for a, b and c: ");
	do {
		scanf("%f %f %f", &a, &b, &c);
		if(a == 0)
			printf("`a` can't be null. Please try again: ");
	} while(a == 0);

	delta = (b * b) - (4 * a * c);
	printf("Delta equals: %.2f\n", delta);

	if(delta == 0) {
		x0 = -b / (2 * a);
		printf("The equation has a single solution: x=%f\n",x0);
	}
	else if(delta > 0) {
		x0=(-b + sqrt(delta)) / (2 * a);
		x1=(-b - sqrt(delta)) / (2 * a);
		printf("The equation has two solutions: x=%f or x=%f\n",x0,x1);
	}
	else {
		delta = -delta;
		x0 = -b / (2 * a);
		ima0 = -sqrt(delta) / (2 * a);
		ima1 = sqrt(delta) / (2 * a);
		printf("The equation has two complexe solutions: x=%f+%fi or x=%f+%fi\n", x0, ima0, x0, ima1);
		
	}
	return 0;
}
