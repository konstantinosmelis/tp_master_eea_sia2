#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "polynomial_lib.h"

void create(struct polynomial *poly, int degree) {
    poly->degree = degree;
    poly->coefficients = malloc((degree + 1) * sizeof(float));
}

void write(struct polynomial *poly) {
    int i;

    for(i = 0; i < (poly->degree + 1); i++) {
        printf("\tcoef.%d: ", i);
        scanf("%f", &poly->coefficients[i]);
    }
}

void display(struct polynomial *poly) {
    int i, degree = poly->degree;
    
    for(i = 0; i < (poly->degree + 1); i++)
        printf("%.2fx^%d ", poly->coefficients[i], degree--);
    printf("\n");
}

float image(struct polynomial *poly, float x) {
    int i, degree = poly->degree;
    float image = 0;
    
    for(i = 0; i < (poly->degree + 1); i++)
        image += (poly->coefficients[i] * pow(x, degree--));
    return image;
}

void derivative(struct polynomial *poly, struct polynomial *derivative) {
    int i, degree = poly->degree;
    create(derivative, degree - 1);
    
    for(i = 0; i < (derivative->degree + 1); i++)
        derivative->coefficients[i] = poly->coefficients[i] * degree--;
}

void sum(struct polynomial *poly1, struct polynomial *poly2, struct polynomial *sum) {
    int i, degree, deg1 = poly1->degree, deg2 = poly2->degree;
    degree = deg1 > deg2 ? deg1 : deg2;
    create(sum, degree);

    for(i = degree; i >= 0; i--) {
        if(deg1 >= 0 && deg2 >= 0)
            sum->coefficients[i] = poly1->coefficients[deg1--] + poly2->coefficients[deg2--];
        else if(deg1 == -1)
            sum->coefficients[i] = poly2->coefficients[deg2--];
        else
            sum->coefficients[i] = poly1->coefficients[deg1--];
    }
}

void product(struct polynomial *poly1, struct polynomial *poly2, struct polynomial *product) {
    int a, b, i, k, degree = poly1->degree + poly2->degree;
    create(product, degree);

    k = 0;
    for(a = 0; a <= poly1->degree; a++) {
        i = k++;
        for(b = 0; b <= poly2->degree; b++) {
            product->coefficients[i] += poly1->coefficients[a] * poly2->coefficients[b];
            i++;
        }
    }
}
