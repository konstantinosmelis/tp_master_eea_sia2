#include <stdio.h>
#include <stdlib.h>
#include "polynomial_lib.h"

int main(int argc, char *argv[]) {
    int degree;
    float x, image_;
    struct polynomial *poly, *der, *sum_;

    poly = malloc(sizeof(struct polynomial));
    printf("Choose the degree of the equation: ");
    scanf("%d", &degree);
    create(poly, degree);

    printf("Insert the values of the coefficients: \n");
    write(poly);

    printf("The equation you wrote is: y=");
    display(poly);

    printf("Choose the number which you want the image: ");
    scanf("%f", &x);
    image_ = image(poly, x);
    printf("f(%.2f)=%f\n", x, image_);

    der = malloc(sizeof(struct polynomial));
    derivative(poly, der);
    printf("Derivative: ");
    display(der);

    sum_ = malloc(sizeof(struct polynomial));
    sum(poly, poly, sum_);
    printf("Sum of poly+poly: ");
    display(sum_);

    free(poly);
    free(der);
    free(sum_);

    return 0;
}
