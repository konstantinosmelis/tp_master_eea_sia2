struct polynomial {
    int degree;
    float *coefficients;
};

void create(struct polynomial*, int);

void write(struct polynomial*);

void display(struct polynomial*);

float image(struct polynomial*, float);

void derivative(struct polynomial*, struct polynomial*);

void sum(struct polynomial*, struct polynomial*, struct polynomial*);

void product(struct polynomial*, struct polynomial*, struct polynomial*);
