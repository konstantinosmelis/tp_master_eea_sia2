#include <stdio.h>
#include <stdlib.h>

#define N 100

int read(FILE*, float*, float*);
float avg(float*, int);
float prod_avg(float*, float*, int);
void coefficients(float*, float*, int, float*, float*);

int main(int argc, char *argv[]) {
    FILE *fd;
    char filename[25];
    int pairs;
    float X[N], Y[N], a, b, x;

    if(argc != 2) {
        printf("Usage: ./bin/lsq <filename>\n");
        exit(1);
    }

    sscanf(argv[1], "%s", filename);
    fd = fopen(filename, "r");
    if(!fd) {
        perror("Error while opening file");
        exit(EXIT_FAILURE);
    }
    pairs = read(fd, X, Y);
    fclose(fd);

    coefficients(X, Y, pairs, &a, &b);
    printf("y=%.2fx+%.2f\n", a, b);

    printf("Insert a value for x: ");
    scanf("%f", &x);
    printf("For x=%f, y=%f\n", x, ((a * x) + b));

    return 0;
}

int read(FILE *file, float *X, float *Y) {
    int pairs = 0;
    
    while(fscanf(file, "%f %f", &X[pairs], &Y[pairs]) == 2)
        pairs++;

    return pairs;
}

float avg(float *list, int size) {
    int i, sum = 0;

    for(i = 0; i < size; i++)
        sum += list[i];

    return (float) sum / size;
}

float prod_avg(float *X, float *Y, int size) {
    float prod[size];
    int i;
    
    for(i = 0; i < size; i++) {
        prod[i] = X[i] * Y[i];
    }

    return avg(prod, size);
}

void coefficients(float *X, float *Y, int size, float *a, float *b) {
    float avgx, avgy, avgxy, avgxx;
    
    avgx = avg(X, size);
    avgy = avg(Y, size);
    avgxy = prod_avg(X, Y, size);
    avgxx = prod_avg(X, X, size);

    *a = (avgxy - (avgx * avgy)) / (avgxx - (avgx * avgx));    
    *b = ((avgy * avgxx) - (avgx * avgxy)) / (avgxx - (avgx * avgx));
}
