#include <stdio.h>
#include <stdlib.h>
#include "lib/list_lib.h"

int main(int argc, char *argv[]) {
    FILE *file;
    char filename[25];
    struct linked_list *list = NULL;

    if(argc != 2) {
        printf("Usage: %s <filename>", argv[0]);
        exit(EXIT_FAILURE);
    }

    sscanf(argv[1], "%s", filename);
    list = read(filename);

    file = fopen("file.txt", "w");
    // write(stdout, list);
    write(file, list);
    fclose(file);

    destroy(list);

    return 0;
}
