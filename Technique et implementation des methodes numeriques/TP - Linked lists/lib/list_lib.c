#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "list_lib.h"

struct linked_list *read(char *filename) {
    FILE *fd;
    struct stat st;
    struct linked_list *list;
    float a, b, tmp[2];

    if(stat(filename, &st) != 0) {
        perror("Error with stat()");
        exit(EXIT_FAILURE);
    }
    if(st.st_size == 0) {
        fprintf(stderr, "File %s is empty. Cannot read chained list\n", filename);
        exit(EXIT_FAILURE);
    }

    fd = fopen(filename, "r");
    if(!fd) {
        perror("Error while opening file");
        exit(EXIT_FAILURE);
    }

    list = malloc(sizeof(struct linked_list));
    while(fscanf(fd, "%f %f", &a, &b) == 2) {
        tmp[0] = a;
        tmp[1] = b;
        insert_first(tmp, &list);
    }
    fclose(fd);

    return list;
}

void write(FILE *file, struct linked_list *list) {
    struct linked_list tmp = *list;

    while(tmp.next != NULL) {
        if(file == stdout)
            fprintf(stdout, "[%f, %f] -> ", tmp.data[0], tmp.data[1]);
        else
            fprintf(file, "%f %f\n", tmp.data[0], tmp.data[1]);
        tmp = *tmp.next;
    }
    if(file == stdout)
        printf("NULL\n");
}

void insert_first(float data[2], struct linked_list **list) {
    struct linked_list *new = malloc(sizeof(struct linked_list));

    new->data[0] = data[0];
    new->data[1] = data[1];
    new->next = *list;
    *list = new;
}

void destroy(struct linked_list *list) {
    if(list->next == NULL)
        free(list);
    else
        destroy(list->next);
}
