#include <stdio.h>

struct linked_list {
    float data[2];
    struct linked_list *next;
};

struct linked_list *read(char*);

void write(FILE*, struct linked_list*);

void insert_first(float[2], struct linked_list**);

void destroy(struct linked_list*);
