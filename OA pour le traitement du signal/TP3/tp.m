close all, clear, clc;

load("data.mat");
%% Presentation du TP
% q1
Py = mean(y.^2);
var_b = Py / 100;

% q2
x_0 = A_th(1) * cos(2 * pi * f_th(1) * t + phi_th(1));
Px_0 = mean(x_0.^2);
x_0b = x_0 + (rand(length(x_0), 1) * sqrt(Px_0 / 100));

figure, plot(t, x_0);
title("Signal x_0(t)");

figure, plot(t, x_0b);
title("Signal x_0(t) bruite");

x = 0;
for n = 1:5
    x = x + A_th(n) * cos(2 * pi * f_th(n) * t + phi_th(n));
end

Px = mean(x.^2);
xb = x + (rand(length(x), 1) * sqrt(Px / 100));

figure, plot(t, x);
title("Signal x(t)");

figure, plot(t, xb);
title("Signal x(t) bruite");

%% Analyse spectrale par Transformee de Fourier
% Cas de l'echantillonnage irregulier
% q2
fmax = 60;
M = 500;
N = length(x);
freq = (-M:M) / M * fmax;
G = exp(1j * 2 * pi .* t .* freq);

% q3
tf_x_0 = abs(G' * x_0) / N;
figure, plot(freq, tf_x_0);
hold on, stem(f_th(1), A_th(1)), hold off;
title("Spectre du signal x_0(t)");

tf_x_0b = abs(G' * x_0b) / N;
figure, plot(freq, tf_x_0b);
hold on, stem(f_th(1), A_th(1)), hold off;
title("Spectre du signal x_0(t) bruite");

% q4
tf_x = abs(G' * x) / N;
figure, plot(freq, tf_x);
hold on, stem(f_th, A_th), hold off;
title("Spectre du signal x(t)");

tf_xb = abs(G' * xb) / N;
figure, plot(freq, tf_xb);
hold on, stem(f_th, A_th), hold off;
title("Spectre du signal x(t) bruite");

% q5
win = G' * ones(N, 1) / N;
figure, plot(freq, abs(win));
title("Fenetre spectrale");

% Cas ou les fréquences sont connues
% q4
f_x_0 = [-f_th(1) f_th(1)];
f_x = [-f_th f_th];

Rx_0 = exp(1j * 2 * pi * f_x_0 .* t);
Rx = exp(1j * 2 * pi * f_x .* t);

ax_0 = inv(Rx_0' * Rx_0) * Rx_0' * x_0;
ax = inv(Rx' * Rx) * Rx' * x;

Ax_0 = abs(ax_0);
Ax = abs(ax);
phi_x_0 = angle(ax_0);
phi_x = angle(ax);

%% Parcimonie par approches gloutonnes
% Methodes de pre-whitenning ou Matching Pursuit (MP)
% q1
M = 5000;
freq = (-M:M) / M * fmax;
G = exp(1j * 2 * pi .* t .* freq);

r = x;
c = zeros(size(G,2), 1);
Gamma = [];
tau = 0;
T=inf;
tau = chisqq(0.95,N);

while T > tau
    [~, k] = max(abs(G' * r));
    Gamma = [Gamma k];
    c(k)= c(k) + (1 / N) * G(:, k)' * r;
    r = r - (1 / N) * G(:, k)' * r * G(:, k);
    T = norm(r)^2 / Px;
end

figure, plot(freq, abs(G' * x) / N);
hold on, stem(freq(Gamma), abs(c(Gamma))), hold off;
title("Rerpresentation frequentielle x_0");
legend("Spectre du signal x_0(T) bruite", "Frequences obtenues par MP");

%% Parcimonie par relaxation convexe
tmp = mean(xb.^2) * tau;
[c, ind] = ols(G, xb, Inf, tmp);

figure, stem(freq, abs(c));
title("Representation parcimonieux");

