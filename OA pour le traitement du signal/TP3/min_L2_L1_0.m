% Fonction [ x, J, NZ ] = Min_L2_L1(y,W,lambda[,n_it_max,seuil]);
%
% Minimisation du critere 
%        J(x) = 1/2||y-W*x||_2^2 + lambda*||x||_1
% correspondant à la relaxation convexe par la norme L1 en approximation parcimonieuse
%
% Entrées :
%   y : vecteur colonne contenant les données
%   W : Matrice contenant les éléments du dictionnaires (colonnes de W)
%   lambda : paramètre de régularisation
% Entrées optionnelles
%   n_it_max : Nombre maximum d'itérations (à utiliser pour limiter le coût de calcul)
%       valeur par défaut : n_it_max = 1e6
%   seuil : seuil à partir du quel une amplitude est considérée nulle
%       valeur par défaut : seuil = 1e-5
%
% Sorties :
%   x : vecteur des paramètres minimisant le critère
%   J : vecteur donnant la valeur du critere a chaque iteration
%   NZ : vecteur donnant le nombre d'elements non nuls de x a chaque iteration
% 
% Attention, cette fonction est spécialisée pour l'analyse spectrale parcimonieuse
% pour lequel :
% - la matrice W'W est Toeplitz symetrique et W(:,k)'*W(:,k) = N
% - l'amplitude pour le terme x_k0 avec k0 indice du milieu de x correspondant a la
%   frequence nulle en analyse spectrale (valeur moyenne du signal) n'est pas pénalisée 

