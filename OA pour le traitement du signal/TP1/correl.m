function  [C, indC] = correl(x,y,pmax,option)
% function  C = correl(x,y,pmax,option)
%
% Calcul dans le domaine temporel des 2*p-1 (pour les indices -p+1 a p-1) 
% premiers coefficients d'intercorrelation :
%                  _ N-n		   
%	Cxy(n) =  C0(n) >	conj(x(k)) y(k+n)
%                  - k=0	
% avec suivant option : 
%	- 'biased'   (C0(n) = 1/N, par d�faut) 
% 	- 'unbiased' (C0(n) = 1/(N-n)) 
% 	- 'coeff'    (C0(n) = C(0)) 
% 	- 'unnorm'   (C0(n) = 1) 
%
% si p n'est pas pr�cis� : par d�faut p = (longueur de x)

%
% Autres formes d'appel :
% 				C = correl(x,pmax,option);
% 				C = correl(x,y,option);
% 				C = correl(x,y,pmax);
% 				C = correl(x,option);
% 				C = correl(x,pmax);
% 				C = correl(x,y);
% 				C = correl(x);

% V�rification des arguments
  N = length(x);
  x = reshape(x,N,1); auto = 0;
  if nargin==3					% Cas trois arguments
     if length(y)==1		% correl(x,pmax,option)
        option = pmax;
        pmax = y;
        y = x; auto = 1;
        %fprintf('correl(x,%d,%s)\n',pmax,option)
      elseif isstr(pmax) 	% correl(x,y,option)
        option = pmax;
        pmax = N; 
        %fprintf('correl(x,y,%s)\n',option)
     else						% correl(x,y,pmax)
        option = 'biased';
        %fprintf('correl(x,y,%d)\n',pmax)
     end
  elseif nargin==2			% Cas deux arguments
     if isstr(y)				% correl(x,option)
        option = y;
        y = x; auto = 1;
        pmax = N;
        %fprintf('correl(x,%s)\n',option)
     elseif length(y)==1	% correl(x,pmax)
        pmax = y;
        y = x; auto = 1;
        option = 'biased';
        %fprintf('correl(x,%d)\n',pmax)
     else						% correl(x,y)
        pmax = N;
        option = 'biased';
        %fprintf('correl(x,y)\n')
     end
  elseif nargin==1		% Cas un arguments correl(x)
     y = x; auto = 1;
     pmax = N;
     option = 'biased';
     %fprintf('correl(x)\n')
  else
     %fprintf('correl(x,y,%d,%s)\n',p,option)
  end
  y = reshape(y,N,1);

  if length(y)~=N
     help correl
     error('Les signaux x et y doivent etre de m�me longueur')
  end
  pmax = min(pmax,N);

% Calcul des coefficients pour un indice positif;
% avec une simple boucle
  C = zeros(1,2*pmax-1);
  for p = 0:pmax-1
      C(pmax+p) = x(1:N-p)'*y(p+1:N);
  end

% Calcul des coefficients pour un indice n�gatif;
  if auto		% cas de l'autocorr�lation,
     C(1:pmax-1) = fliplr(conj(C(pmax+1:2*pmax-1)));
  else
    for p = 1:pmax-1
        C(pmax-p) = x(p+1:N)'*y(1:N-p);
    end
  end

% Normalisation suivant option
  if (strcmp(option,'biased') || strcmp(option,'period'))
     C = C/N;
  elseif strcmp(option,'unbiased')
     C = C./(N+[(-pmax+1:-1) (0:-1:-pmax+1)]);
  elseif strcmp(option,'coeff')
     C = C/C(pmax);
  end

% Indices des coefficients de correlation
if nargin>1
   indC = [(-pmax+1:-1) (0:pmax-1)];
end
