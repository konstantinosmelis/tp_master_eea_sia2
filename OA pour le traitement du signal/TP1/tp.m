close all, clear, clc;

load('data_AR_2023.mat');

%% Estimation par resolution de l’equation de Yule-Walker
% Comprehension de l’algorithme de Levinson
Cx = [1/2, 1/4, -1/4, -1/2, -1/4, 1/4, 1/2, 1/4, -1/4, -1/2, -1/4, 1/4, 1/2, 1/4, -1/4];
[b, e2, L] = levin(Cx, 6);

figure, plot(0:(length(e2)-1), e2);
title("Variation de la variance au cours des iterations");

% Estimation de parametres
P = length(x_0);

% Spectre theorique
Fe = 1;
f = ((0:P-1) * (Fe / P)) - (Fe / 2);
spectreTheo = sigma2_0 ./ abs(fft(b_0, P)) .^ 2;
figure, plot(f, 10 * log10(spectreTheo));
title("Spectre theorique du signal x_0(t)");

% Reprensentation temporelle du signal
figure, plot(x_0);
title("Signal x_0(t)");

% Periodogramme et spectre theorique
periodogramme = (abs(fft(x_0, P)) .^ 2) / P;
figure, plot(f, 10 * log10(spectreTheo));
hold on, plot(f, 10 * log10(periodogramme)), hold off;
title("Spectre theorique et periodogramme de x_0(t)");
legend("Spectre theorique", "Periodogramme");

for N = [6, 12]
    % Estimation de l'autocorrelation
    [cNb, indexNb]= correl(x_0, P, 'unbiased');
    [cB, indexB]= correl(x_0, P, 'biased');

    figure, plot(indexNb, cNb);
    hold on, plot(indexB, cB), hold off;
    title("Estimations de l'autocorrelation de x_0(t)");
    legend("Estimation non biaisee", "Estimation biaisee");

    % Estimation des parametres avec l'algorithme de levinson
    cNb = cNb(256:end);
    cB = cB(256:end);

    [bNb, e2Nb, LNb] = levin(cNb, N);
    [bB, e2B, LB] = levin(cB, N);

    figure, plot(0:(length(e2Nb)-1), e2Nb);
    hold on, plot(0:(length(e2B)-1), e2B), hold off;
    title(sprintf("Variation de la variance au cours des itérations pour N=%d", N));
    legend("Variance non biaisee", "Variance biaisee");

    % Calcul des spectre obtenus a partir des parametres estimes
    spectreCoefNb = sigma2_0 ./ abs(fft(bNb, P)) .^ 2;
    spectreCoefB = sigma2_0 ./ abs(fft(bB, P)) .^ 2;

    figure, plot(f, 10 * log10(spectreTheo), 'm');
    hold on;
    plot(f, 10 * log10(periodogramme));
    plot(f, 10 * log10(spectreCoefNb));
    plot(f, 10 * log10(spectreCoefB));
    hold off;
    title("Spectre theorique et spectres estimes de x_0(t)");
    legend("Spectre theorique", "Periodogramme", "Modele non biaisee", "Modele biaisee");

    % Calcul et affichage des poles
    polesTheo = roots(b_0);
    polesB = roots(bB);
    polesNb = roots(bNb);

    figure, polar(angle(polesTheo), abs(polesTheo), '*');
    hold on;
    polar(angle(polesB), abs(polesB), 'o');
    polar(angle(polesNb), abs(polesNb), '+');
    hold off;
    title("Poles theoriques et estimes dans le plan complexe");
    legend("Poles theoriques", "Poles biaises", "Poles non biaises");
end

%% Estimation par minimisation de l’erreur de prediction
N = 6;

polesB = roots(LB(1:N+1, N+1));
polesNb = roots(LNb(1:N+1, N+1));

chi = x_0(1, N+1:end)';
X = toeplitz(x_0(1, N:end-1), flip(x_0(1, 1:N)));

% Estimation par moindres carrés les coefficients de prédiction linéaires.
coeff_est = inv((X' * X)) * (X' * chi);

% Estimation de la variance de l'erreur de prediction
err_prd = chi - X * coeff_est;
var_err_pred = var(err_prd);

% 4
[H, fthm] = freqz(1, [1; -1 * coeff_est], f, Fe);
spectreMC = abs(H) .^ 2;

figure, plot(f, 10 * log10(spectreTheo));
hold on, plot(f, 10 * log10(spectreMC));, hold off;
title("Spectre theorique et spectre estime par MC");
legend("Spectre theorique", "Spectre estime par MC");

% Comparaison avec les poles estimés precedemment et les poles theoriques
poles_est = roots([1; -1 * coeff_est]);

figure, polar(angle(polesTheo), abs(polesTheo), '*');
hold on;
polar(angle(polesB), abs(polesB), 'o');
polar(angle(polesNb), abs(polesNb), '+');
polar(angle(poles_est), abs(poles_est), '^');
hold off;
title("Poles theoriques et estimes dans le plan complexe");
legend("Poles theoriques", "Poles biaises", "Poles non biaises", "Poles MC");

% 6 prediction lineaire du signal ainsi que l'eereur de prediction
pred_lin_sig = filter([1; -coeff_est], 1, x_0);

figure;
subplot(211), plot(chi, '-r');
hold on, plot(pred_lin_sig(N+1:end), '-g'), hold off;
title("Signal et signal predit");
legend("Signal", "Signal Predit");
subplot(212), plot(err_prd(N+1:end));
title("Erreur de prédiction du signal");

% Autocorrelation de l'erreur de prediction
[autoCerrb,ind]= xcorr(err_prd, 'biased');
[autoCerrnb,ind]= xcorr(err_prd, 'unbiased');

figure, plot(autoCerrb);
title("Autocorrelation de l'erreur de prediction biaise");

figure, plot(autoCerrnb);
title("Autocorrelation de l'erreur de prediction non biaise");

