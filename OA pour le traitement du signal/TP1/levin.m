function [b, liste2, listeb] = levin(C,P);
% fonction [b, e2, L]= levin(C,P);
%
% Résolution pour les ordres 1 à P de l'équation normale (Yule-Walker) 
% d'un modèle Auto-régressif 
% système G*b= [e2, 0...0]^T avec G, matrice Toeplitz symétrique;
% et b = [1, -a1, -a2, ... -aP] 
%
% Entrée : 
%				C coefficients de corrélations (indices de 0 à P au moins)
%				P ordre (maximal) de l'AR
% Sortie
%				b  coefficients AR(P) [1, -a1, -a2, ... -aP]
%				e2 variance de l'erreurs de prédiction pour les AR d'ordre 0 à P
%				L	Matrice de stockage des AR d'ordre 1 à P

% Vecteur colonne
  C = C(:);

% Par résolution matricielle
%  ath = -toeplitz(C(1:p))\C(2:p+1);


% Stockage
  liste2 = zeros(1,P+1);
  listeb = zeros(P+1,P+1);
  listek = zeros(1,P+1);

% Initialisation
  p=0;
  b = 1;
  listeb (1,1) = b;
  e2 = C(1);
  liste2(1) = e2;

% Récurrence sur l'ordre
  for p =1:P
      d = C(2:p+1)'*flipud(b);
      k = d/e2;
      e2 = e2*(1-k^2);
      b = [b; 0] -k*[0; flipud(b)];
      liste2(p+1) = e2;
      listeb(1:p+1,p+1) = b;
  end
end
