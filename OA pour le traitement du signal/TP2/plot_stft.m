%% Script plot_stft
% Master 2 SIA2, Année universitaire 2023-2024

% Construction du signal
build_signal;

% Choix de la fenetre
L = 250;           % longueur de la fenetre
fen = ones(1, L);  % vecteur de la fenetre (de longueur L)
% fen = blackman(L) ;
% fen = hamming(L);

% Parametres de la STFT
NFFT = N;  % taille des fft (zero-padding si NFFT>L)
pas = 10;  % pas de décalage en temps des fenêtres (en nombre d'échantillons)
           % (période d'échantillonnage temporel de la STFT)

% NE PAS MODIFIER LA SUITE DU SCRIPT
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcul de la Transformee de Fourier a court terme
[X, tp, fp] = stft(x, NFFT, Fe, fen, pas);

% Affichage
figure(2); clf;
% Transformee de Fourier a court terme
subplot('position', [0.1 0.1 0.67 0.58]);
imagesc(tp, fp, abs(X)); axis xy;
title('Spectrogramme');
xlabel('Temps (sec)');
ylabel('Frequence (Hz)');
Xlim = axis; Xlim = Xlim(1:2);

% Periodogramme
subplot('position', [0.85 0.1 0.14 0.58]);
N2 = 8 * N;
freq = (0:N2-1) / N2 * Fe;
TFx = fft(x, N2);
plot(abs(TFx(1:end / 2)) .^ 2 / N, freq(1:end / 2));
xlabel('Puissance');
title('Spectre');

% Representation temporelle
subplot('position', [0.1 0.75 0.67 0.2]);
plot(t, x);
xlabel('Temps (sec)');
ylabel('Amplitude');
title('Representation temporelle du signal');
ax = axis; ax(1:2) = Xlim; axis(ax);

% Barre des couleurs
hand = subplot('position', [0.01 0.1 0.02 0.58]);
colorbar(hand);
