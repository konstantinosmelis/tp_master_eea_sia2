%% Script plot_cwt
% Master 2 SIA2, Année universitaire 2023-2024

% Construction du signal
build_signal;

% Parametres de la transformee en ondelettes continue 
pad = 1;        % si 1 complete le signal par des zeros, recommande pour:
                %   - attenuer les effets de bords 
                %   - calculer les fft sur des puissances de 2
% Les echelles sont definies d'apres la formule
% s(k) = s0*2^(k/voix)
s0 = .0002;     % Plus petite echelle
voix = 10;      % Nombre de voix par octave
oct = 7;        % Nombre d'octaves
dj = 1 / voix;  % pas de division d'une octave
j1 = oct / dj;  % Nombre total de voix

% Choix de l'ondelette
mother = 'Morlet';  % 'Morlet', 'Paul' ou 'DOG'
m = 6;              % parametre de l'ondelette

% NE PAS MODIFIER LA SUITE DU SCRIPT
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcul de la Transformee en ondelette
[wave, period, scale, coi] = wavelet(x, 1/Fe, pad, dj, s0, j1, mother, m);
power = (abs(wave)) .^ 2 ;      % Wavelet power spectrum

% Spectre global en ondelette
global_ws = sum(power') / N;  % Moyenne temporelle 

% Affichage
% Representation temporelle
figure(1); clf;
subplot('position', [0.1 0.75 0.67 0.2]);
plot(t,x);
xlabel('Temps (sec)');
ylabel('Amplitude');
title('Representation temporelle du signal');
axis tight;

% Transformee en ondelette
ax(1) = subplot('position', [0.1 0.1 0.67 0.58]);
imagesc(t, log2(scale), power);
xlabel('Temps (sec)');
ylabel('log_2 echelle');
title('Transformee en ondelette');
% Cone d'influence
hold on, plot(t, log2(coi), 'w'), hold off;
set(gcf, 'nextplot', 'add');
ax(2) = axes('position', get(ax(1), 'position'));
imagesc(t, log2(period), power);
set(ax(2), 'YAxisLocation', 'right');
Ylim = axis; Ylim = Ylim(3:4);
ylabel('log2 Periode (sec)');

% Spectre global en ondelette
subplot('position', [0.85 0.1 0.14 0.58]);
plot(global_ws, log2(period));
xlabel('Puissance');
title('Spectre');
set(gca, 'YLim', Ylim, 'YDir', 'reverse');
N2 = 8 * N;
freq = (0:N2-1) / N2 * Fe;
X = fft(x, N2);
hold on; warning off;
plot(abs(X(1:end/2)) .^ 2 / N, -log2(freq(1:end/2)), 'r')
hold off; warning on;
legend('Ondelette', 'Fourier');

% Barre des couleurs
hand = subplot('position', [0.01 0.1 0.02 0.58]);
colorbar(hand);
