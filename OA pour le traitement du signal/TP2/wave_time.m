%WAVE_TIME  1D Time Wavelet functions Morlet, Paul, or DOG
%
%  [DAUGHTER,TIME] = wave_time(MOTHER,T,SCALE,N,DT,PARAM);
%
%   Computes the wavelet function as a function of time.
%
% INPUTS:
%
%    MOTHER = a string, equal to 'MORLET' or 'PAUL' or 'DOG'
%    T      = a number, the Time position at which to calculate the wavelet
%    SCALE  = a number, the wavelet scale
%    N      = a number, the number of samples of the time representation
%    DT     = amount of time between each samples, i.e. the sampling time.
%    PARAM  = the nondimensional parameter for the wavelet function
%            For 'MORLET' this is k0 (wavenumber), e.g. 6.
%            For 'PAUL' this is m (order), e.g. 4.
%            For 'DOG' this is m (m-th derivative), e.g. 2 (Mexican hat).
%
% OUTPUTS:
%
%    DAUGHTER = a vector, the wavelet function
%    TIME     = a vector, the time corresponding to the wavelet

function [w, t] = wave_time(mother,b,s,N,dt,m);

% Fonctions telles que d�crites dans 
% http://www.clecom.co.uk/science/autosignal/help/
%      Continuous_Wavelet_Transfor.htm
mother = upper(mother);

n=((-N/2:N/2-1)*dt-b)/s;
if strcmp(mother,'MORLET')
   w = pi^-.25*exp( -n.^2/2 + i*m*n);
elseif strcmp(mother,'PAUL')
   w = (2*i)^m*prod(1:m)/sqrt(pi*prod(1:2*m))*(1-i*n).^(-m+1);
elseif strcmp(mother,'DOG')
 % Numerical approximation of the derivative...
   n = ((-N/2+1:N/2+m)*dt-b)/s;
   dergauss = exp( -(n).^2/2);
   for k=1:m
       dergauss = diff(dergauss);
   end
   w = (-1)^(m+1)/sqrt(gamma(m+.5)).*dergauss;
end
w = sqrt(dt/s)*w;
t = (-N/2:N/2-1)*dt;
