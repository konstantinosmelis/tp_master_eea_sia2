function startup()

% STARTUP.M  -  Startup file for the Time Frequency Toolbox.
%--------------------------------------------------------

global TFTB_HOME_PATH

if size(TFTB_HOME_PATH)~=[0,0]
	disp(' ')
	disp('The Tftb toolbox path is already installed.')
	answr=input('Do you want to add the new ones? (y/n) :','s');
	% Any string starting with a 'y' is considered to be 'YES'
	if (answr(1)~='y')
		disp('Leaving MATLABPATH unchanged')
		return
	end
end



% --------------------------------------------------------
% Home directory for the Time Frequency Toolbox.
% By default, it is the directory where 'startup.m' is located.

p = pwd;
TFTB_HOME_PATH = p;


% --------------------------------------------------------
% Other directories. If the Toolbox's structure is hold
% as it's served, they shouldn't be changed.
%
% But remember to change the syntax according to the machine. 
% (Default settings are for Unix)

MFILES_PATH = [TFTB_HOME_PATH,'/mfiles'];      % Matlab script and functions

% --------------------------------------------------------
% Append new path:

path (path,MFILES_PATH)
   
