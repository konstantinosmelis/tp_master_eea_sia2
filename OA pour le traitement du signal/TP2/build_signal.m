%% Script build_signal
% Master 2 SIA2, Année universitaire 2023-2024

% Paramètres des représentations temporelles et fréquentielles
Fe = 8000;             % Frequence d'echantillonnage du signal
N = 2048;              % Nombre d'echantillons du signal
t = (0:N-1) / Fe;      % Vecteur de temps
f = (0:N-1) / N * Fe;  % Vecteur de frequence

% Construction du signal
% Somme de deux kronecker
x = zeros(1, N);
x(N / 2) = 1;
x(floor(N / 3)) = 1;

% Somme de sinusoides
% s1 = sin(2 * pi * 500 * t);
% s1(1:N/2) = 0;
% s2 = cos(2 * pi * 1500 * t);
% s2(((N/2) + 1):N) = 0;
% x = s1 + s2;

% Signal personnalise
% x = sin(2 * pi * 500 * t) + exp(1j * 2 * pi * 1000 * t);

% Kronecker
% x = zeros(1, N);
% x(N / 2) = 1;

% Frequence pure
% x = exp(1j * 2 * pi * 1000 * t);

% Chirp
% x = chirp(t, 500, N/2, 1000, 'linear', 0, 'complex');
% x = exp(1j * 2 * pi * (500 + 1000 * t) .* t);
