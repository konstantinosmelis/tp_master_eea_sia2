function [X, time, freq] = stft(x,NFFT,Fe,win,pas);  

% STFT Calculate Short Time Fourier Transform from signal.
%    [X, Time, Freq] = STFT(x,NFFT,Fs,WINDOW,STEP) 
%    calculates the Short Time Fourier Transform for the signal in vector x
%    sampled with frequency Fs.  
%    STFT splits the signal into overlapping segments every STEP elements, 
%    windows each with the WINDOW vector and forms the columns of X with their 
%    zero-padded, length NFFT discrete Fourier transforms.  Thus each column of
%    X contains an estimate of the short-term, time-localized frequency content
%    of the signal x.  Time increases linearly across the
%    columns of X, from left to right.  Frequency increases linearly down
%    the rows, starting at 0.  
%
%    The result may be displayed using
%    imagesc(Time,Freq,abs(X)); axis xy

% H. Carfantan

% lx : longueur du signal
  x = x(:);
  lx = length(x);

% L : longueur des tranches
  if length(win)==1,
     L = win;
     win = ones(L,1);
  else
     win=win(:);
     L = length(win);
  end

% K = nombres de tranches
  K = floor((lx-L)/pas)+1;

% Calcul de la STFT
  N = ceil(NFFT/2);
  X = zeros(N,K);
  for k = 1 : K
      TFx = fft(x( (1:L) + pas*(k-1) ).*win,NFFT);
      X(:,k) = TFx(1:N);
  end
  time = (0:K-1)*pas/Fe;
  freq = (0:N-1)/NFFT*Fe;
