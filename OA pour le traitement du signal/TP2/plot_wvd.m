%% Script plot_wvd
% Master 2 SIA2, Année universitaire 2023-2024

% NE PAS MODIFIER LA SUITE DU SCRIPT
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Construction du signal
build_signal;

% Calcul distribution de Wigner-Ville
X = tfrwv(x(:));

% Affichage
figure(2); clf;
% Distribution de Wigner-Ville
subplot('position', [0.1 0.1 0.67 0.58]);
imagesc(t, f/2 - Fe/4, abs(fftshift(X, 1))); axis xy;
title('Distribution de Wigner-Ville');
xlabel('Temps (sec)');
ylabel('Frequence (Hz)');
Xlim = axis; Xlim = Xlim(1:2);
% Barre des couleurs
hand = colorbar;
set(hand, 'position', [0.02 0.1 0.02 0.58]);

% Periodogramme
subplot('position', [0.85 0.1 0.14 0.58]);
N2 = 8 * N;
freq = (0:N2-1) / N2 * Fe;
TFx = fft(x, N2);
plot(abs(fftshift(TFx)) .^ 2 / N, freq - Fe / 2);
xlabel('Puissance');
title('Spectre');

% Representation temporelle
subplot('position', [0.1 0.75 0.67 0.2]);
plot(t, real(x));
if ~isreal(x), hold on, plot(t, imag(x)), hold off; end
xlabel('Temps (sec)');
ylabel('Amplitude');
title('Representation temporelle du signal');
ax = axis; ax(1:2) = Xlim; axis(ax);
